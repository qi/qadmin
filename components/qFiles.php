<?php

/**
 * Class qFiles.
 * Provides methods to work with file system.
 * @author andcherv
 */
class qFiles extends CApplicationComponent
{
	/**
	 * Excluded directories.
	 * @author andcherv
	 * @var array
	 */
	protected $excludeDirs = array('.', '..');
	/**
	 * Excluded files.
	 * @author andcherv
	 * @var array
	 */
	protected $excludeFiles = array('.gitignore');

	/**
	 * Recursively removes files in the directory.
	 * @author andcherv
	 * @param string $path
	 * @return bool
	 */
	public function emptyDir($path)
	{
		$dirBefore = scandir($path);

		if (is_dir($path)) {
			$items = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::CHILD_FIRST);
			foreach ($items as $item) {
				if (is_dir($item->getPathname())) {
					if (!in_array($item->getBasename(), $this->excludeDirs)) {
						if (!rmdir($item->getPathname()))
							return false;
					}
				} else {
					if (!in_array($item->getFileName(), $this->excludeFiles)) {
						if (!unlink($item->getPathname()))
							return false;
					}
				}
			}
		}

		$dirAfter = scandir($path);

		return array_diff($dirBefore, $dirAfter);
	}

	/**
	 * Returns true if directory is empty.
	 * @author andcherv
	 * @param $path
	 * @return bool
	 */
	public function isEmptyDir($path)
	{
		if (is_readable($path))
			return count(scandir($path)) == $this->excludesCount;
		return false;
	}

	/**
	 * Returns count of items in the dorectory.
	 * @author andcherv
	 * @param $path
	 * @return bool|int
	 */
	public function count($path)
	{
		if (is_readable($path))
			return count(scandir($path)) > $this->excludesCount ? count(scandir($path)) - $this->excludesCount : 0;
		return false;
	}

	/**
	 * Returns count of excluded items.
	 * @author andcherv
	 * @return int
	 */
	protected function getExcludesCount()
	{
		return count($this->excludeDirs) + count($this->excludeFiles);
	}

	public function createDir($path)
	{
		if (!file_exists($path))
			return mkdir($path, 0777, true);
		return true;
	}
}