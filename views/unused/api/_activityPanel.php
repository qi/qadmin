<?php $this->beginWidget('qPanel', array(
	'id' => 'activity-panel',
	'title' => 'API Activity',
	'collapsible' => true,
)); ?>
<div class="panel-body">
	<div id="plot" style="height:100px"></div>
	<div class="help-block text-right">Last 2 min. with ~ 30 sec. latency</div>
</div>
<?php $this->endWidget(); ?>

<script>
	$(function () {
		var logData = [];
		var plotData = [];
		var portionLength = 20;
		var plotDataLength = 120;
		var plotUpdateTime = 1000;
		var logDataUpdateTime = portionLength/2 * 1000;
		var plot = $.plot("#plot", plotData, {
			series: {
				shadowSize: 0,
				lines: {fill: true}
			},
			colors: ["#0a929c"],
			xaxis: {show: false},
			yaxis: {min: 0,	max:1},
			grid: {
				borderWidth: 1,
				borderColor: "#ccc"
			}
		});
		var key;

		$(window).resize(function () {
			plot.resize();
			plot.draw();
		});

		updateLogData();
		updatePlot();

		function updateLogData() {
			if (!key || logData.length - key <= plotDataLength / 2) {
				$.get("/admin/api/ajaxGetActivityData", {length: portionLength}, function (response) {
					$.each(response.data, function (index, value) {
						if (index > key) {
							logData[index] = value;
						}
					});
				}, 'json');
			}
			setTimeout(updateLogData, logDataUpdateTime);
		}

		function updatePlotData() {
			if (plotData.length > 0)
				plotData = plotData.slice(1);
			while (plotData.length < plotDataLength) {
				key = getFirstKey(logData);
				if (key !== null) {
					plotData.push(logData[key]);
					delete logData[key];
				} else {
					plotData.push(0);
				}
			}
		}

		function updatePlot() {
			updatePlotData();
			var axes = plot.getAxes();
			axes.yaxis.options.max = Math.max.apply(Math, plotData);
			var normalizedPlotData = [];
			for (var i = 0; i < plotData.length; i++)
				normalizedPlotData.push([i, plotData[i]]);
			plot.setData([normalizedPlotData]);
			plot.setupGrid();
			plot.draw();
			setTimeout(updatePlot, plotUpdateTime);
		}

		function getFirstKey(data) {
			for (var prop in data)
				if (data.hasOwnProperty(prop)) {
					return prop;
				}
			return null;
		}

	});
</script>