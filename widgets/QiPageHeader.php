<?php

class QiPageHeader extends CWidget
{
	public $header = '';
	public $small = '';

	public function run()
	{
		echo CHtml::tag('div', array('id' => 'page-header', 'class' => 'page-header',
		), "<h1>$this->header&nbsp;<small>$this->small</small></h1>");
	}
}