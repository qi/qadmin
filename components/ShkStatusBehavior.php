<?php
class ShkStatusBehavior extends CBehavior
{
	/**
	 * Changes status.
	 * @author andcherv
	 * @param string $statusLabel Status label.
	 * @param bool $save Save model or just assign attribute.
	 * @return bool
	 * @throws CDbException
	 */
	public function changeStatus($statusLabel, $save = false)
	{
		if (!empty($statusLabel)) {
			$status = ShkSystemStatusEnum::model()->findByAttributes(array(
				'label' => $statusLabel,
			));
			if (!is_null($status) && !is_null($status->id)) {
				if ($this->owner->status_id == $status->id && $save === true) {
					$save = false;
				} else {
					$this->owner->status_id = $status->id;
				}
			} else {
				throw new CDbException('Status not found.');
			}
		}
		return $save === true ? $this->owner->save() : true;
	}
}