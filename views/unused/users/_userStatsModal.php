<?php
/**
 */
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php echo Yii::t('User', 'User\'s Statistics'); ?>
		<small><?php echo isset($user) ? $user->fullName : '&nbsp;' ?></small>
	</h4>
</div>
<div class="modal-body"></div>
