SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `qp_page_help`;
DROP TABLE IF EXISTS `shk_system_status_enum`;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `qp_page_help` (
  `id`         INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `language`   VARCHAR(2)       NOT NULL,
  `title`      VARCHAR(256)     NOT NULL,
  `module`     VARCHAR(100)     NULL,
  `controller` VARCHAR(100)     NOT NULL,
  `action`     VARCHAR(100)     NOT NULL,
  `content`    TEXT             NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `route` (`module`, `controller`, `action`, `language`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  AUTO_INCREMENT = 1;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `shk_system_status_enum` (
  `id`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label`       VARCHAR(100)     NOT NULL DEFAULT '',
  `title`       VARCHAR(100)     NOT NULL DEFAULT '',
  `description` TEXT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `label` (`label`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  COMMENT ='System statuses'
  AUTO_INCREMENT = 1;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;