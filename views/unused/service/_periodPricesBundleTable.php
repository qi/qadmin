<?php
/**
 * Prices of services bundle table.
 * @author andcherv
 * @var ShkServicePeriod $period
 * @var ShkServicePrice[] $prices
 * @var ShkSchoolBenefit $benefit
 */
echo CHtml::openTag('table', array(
	'id' => isset($tableId) ? $tableId : 'periodPricesTable',
	'class' => 'table table-hover table-condensed',
));
echo CHtml::openTag('tr');
echo CHtml::tag('th', array('class' => 'col-xs-3'), Yii::t('ShkService', 'Price in a bundle of...'));
for ($i = 1; $i <= count($period->services); $i++) {
	$colHeader = $i == 1 ? $i . ' ' . Yii::t('ShkService', 'service') : $i . ' ' . Yii::t('ShkService', 'services');
	echo CHtml::tag('th', array(), $colHeader);
}
echo CHtml::closeTag('tr');
foreach ($period->services as $activeService) {
	echo CHtml::openTag('tr');
	echo CHtml::tag('td', array(), $activeService->service->title);
	for ($bundle = 1; $bundle <= count($period->services); $bundle++) {
		$currentPrice = null;
		foreach ($prices as $price) {
			$priceBundle = null;
//			echo count($price->rules);
			foreach ($price->rules as $rule) {
				if ($rule->rule_id == ShkServicePriceRuleEnum::BUNDLE)
					$priceBundle = $rule->rule_value;
			}
			if ($price->service_id == $activeService->service_id && $priceBundle == $bundle)
				$currentPrice = $price;
		}
		echo CHtml::openTag('td');
		$priceId = "$activeService->service_id:$bundle";
		if (isset($benefit) && is_object($benefit)) {
			$priceId .= ":$benefit->id";
			$ruleId = ShkServicePriceRuleEnum::BENEFIT;
			echo CHtml::hiddenField("ShkServicePrice[$priceId][price_rules][$ruleId][rule_id]", $ruleId);
			echo CHtml::hiddenField("ShkServicePrice[$priceId][price_rules][$ruleId][rule_value]", $benefit->id);
		}
		$ruleId = ShkServicePriceRuleEnum::BUNDLE;
		echo CHtml::hiddenField("ShkServicePrice[$priceId][price_rules][$ruleId][rule_id]", $ruleId);
		echo CHtml::hiddenField("ShkServicePrice[$priceId][price_rules][$ruleId][rule_value]", $bundle);
		echo CHtml::hiddenField("ShkServicePrice[$priceId][period_id]", $period->id);
		echo CHtml::hiddenField("ShkServicePrice[$priceId][service_id]", $activeService->service_id);
		echo CHtml::textField("ShkServicePrice[$priceId][price]", isset($currentPrice) ? $currentPrice->price : '', array(
			'class' => 'form-control input-sm',
			'placeholder' => isset($currentPrice) ? $currentPrice->price : '',
		));
		echo CHtml::closeTag('td');
	}
	echo CHtml::closeTag('tr');
}
echo CHtml::closeTag('table');