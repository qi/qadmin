<?php

class QiSelect extends CWidget
{
	public $htmlOptions = array();
	public $title;
	public $header;
	public $default;
	public $items = [];

	public function init()
	{
		if (isset($this->htmlOptions['id']))
			$this->id = $this->htmlOptions['id'];
		if (!isset($this->title) && isset ($this->header))
			$this->title = $this->header;
	}

	public function run()
	{
		echo CHtml::openTag('div', array('id' => $this->id, 'class' => 'QiSelect'));
		echo CHtml::openTag('ul', array('class' => 'nav navbar-nav'));
		echo CHtml::openTag('li', array('class' => 'dropdown'));
		echo CHtml::link(CHtml::tag('span', array(), $this->title . '&nbsp;<b class="caret"></b>'), '#', array(
			'class' => 'dropdown-toggle bg-success', 'data-toggle' => 'dropdown',
		));
		echo CHtml::openTag('ul', array('class' => 'dropdown-menu'));
		if (isset($this->header))
			echo CHtml::tag('li', array('class' => 'dropdown-header'), $this->header);
		foreach ($this->items as $item) {
			echo CHtml::tag('li', [], $item);
		}
		if (isset($this->default))
			echo '<li class="divider"></li><li>' . $this->default . '</li>';
		echo CHtml::closeTag('ul');
		echo CHtml::closeTag('li');
		echo CHtml::closeTag('ul');
		echo CHtml::closeTag('div');
	}
}