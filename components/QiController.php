<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class QiController extends Controller
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = 'default';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();

	protected $modelName;
	protected $editFormId;

	/**
	 * Adds breadcrumbs.
	 * @param array $breadcrumbs
	 */
	public function addBreadcrumbs(array $breadcrumbs)
	{
		foreach ($breadcrumbs as $key => $value) {
			if (isset($value) && !is_null($value)) {
				$this->breadcrumbs[$key] = $value;
			} else {
				array_push($this->breadcrumbs, $key);
			}
		}
	}


	/**
	 * Returns axEditModal action link.
	 * TODO: create a behavior
	 * FIXME: value parse in gridview
	 * @param CActiveRecord $model
	 * @param string $action
	 * @param array $params
	 * @return string
	 */
	protected function getAxEditModalUrl(CActiveRecord $model, $action = 'axEditModal', array $params = [])
	{
//		return Yii::app()->createUrl($this->getUniqueId() . '/' . $action .'?' . get_class($model) . '[id]=$data->id');
		$result = [];
		array_walk($params, function(&$value, $key) use (&$result, $model) {
			$result[get_class($model) . '[' . $key . ']'] = $value;
		});
		return Yii::app()->createUrl($this->getUniqueId() . '/' . $action .'?' . http_build_query($result));
	}
}