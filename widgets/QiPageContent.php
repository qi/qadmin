<?php

class QiPageContent extends CWidget
{
	public function init()
	{
		echo CHtml::openTag('div', ['id' => 'page-content']);
	}

	public function run()
	{
		echo CHtml::closeTag('div');
	}
}