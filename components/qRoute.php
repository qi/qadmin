<?php

/**
 * Class qRoute
 * @author andcherv
 */
class qRoute extends CApplicationComponent
{
	public static function getModules()
	{
		$modules = array('');
		$appModules = Yii::app()->modules;
		if (isset($appModules) && !empty($appModules)) {
			foreach ($appModules as $key => $value) {
				array_push($modules, $key);
			}
		}
		return array_diff($modules, array('gii', 'debug', 'api'));
	}
}