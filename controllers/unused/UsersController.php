<?php

class UsersController extends DefaultController
{
	public $layout = 'default';

	public function actionIndex()
	{
		$this->pageTitle = Yii::t('qAdmin', 'Users');
		$this->addBreadcrumbs(array(
			Yii::t('qAdmin', 'Users'),
		));

		$usersModel = new UserAuth('search');
		$usersModel->unsetAttributes();
		if (isset($_GET['UserAuth']))
			$usersModel->attributes = $_GET['UserAuth'];

		if (isset($_GET['usersGridPageSize']))
			Yii::app()->user->setState('usersGridPageSize', $_GET['usersGridPageSize']);

		$this->render('index', array(
			'usersModel' => $usersModel,
		));
	}

	public function actionAjaxUserStatsModal($id)
	{
		if (Yii::app()->request->isAjaxRequest) {
			$this->renderPartial('_userStatsModal', array());
		}
		Yii::app()->end();
	}

	public function actionAjaxUserLocationModal($id)
	{
		if (Yii::app()->request->isAjaxRequest) {
			$this->renderPartial('_userLocationModal', array(
				'location' => UserLocation::model()->find(array(
						'with' => array('user.profile'),
						'condition' => 't.user_id=:id',
						'params' => array(':id' => $id),
						'order' => 't.change_time desc'
					)),
			));
		}
		Yii::app()->end();
	}

	public function actionAjaxUserEditModal($id)
	{
		$model = UserAuth::model()->findByPk($id, array(
			'with' => array('metadata',	'profile', 'activity'),
		));
		if (Yii::app()->request->isAjaxRequest) {
			$this->performAjaxValidation(array($model, $model->profile), 'user-form');
			$this->renderPartial('_userEditModal', array(
				'model' => $model,
			), false, true);
			Yii::app()->end();
		} else {
			if (isset($_POST['save']) || isset($_POST['create'])) {
				$model->attributes = $_POST['UserAuth'];
				$model->profile->attributes = $_POST['UserProfile'];
				$model->save();
				$model->profile->save();
			} elseif (isset($_POST['delete'])) {
				$model->metadata->delete();
			}
			$this->redirect('.');
		}

	}

	public function actionAjaxUpdateStats()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$totalCount = UserAuth::model()->with('metadata:active')->count();
			$onlineCount = UserAuth::model()->with(array(
				'metadata:active',
				'activity:online' => array(
					'joinType' => 'INNER JOIN'
				),
			))->count();
			$offlineCount = $totalCount - $onlineCount;
			echo CJSON::encode([
				'status' => 'success',
				'data' => [
					'total' => $totalCount,
					'online' => $onlineCount,
					'offline' => $offlineCount,
					'pctOnline' => $totalCount > 0 ? round($onlineCount * 100 / $totalCount, 1) : 0,
					'pctOffline' => $totalCount > 0 ? round($offlineCount * 100 / $totalCount, 1) : 0,
				],
			]);
		}
		Yii::app()->end();
	}
}