<?php
/**
 * @var CController $this
 * @var School $schoolModel
 */
?>

<?php $this->renderPartial('/layouts/_defaultPageHeader', array(
	'header' => Yii::t('School', 'Schools'),
	'small' => Yii::t('School', 'Statistics & administration'),
)); ?>

<div id="page-content" class="row">
	<!--<div class="col-xs-12">
		<div class="row">
			<div class="col-md-3">
				<?php /*$this->renderPartial('_countPanel'); */?>
			</div>
			<div class="col-md-3">
				<?php /*$this->renderPartial('_typePanel'); */?>
			</div>
			<div class="col-md-6">
				<?php /*$this->renderPartial('_countryPanel'); */?>
			</div>
		</div>
	</div>-->
	<div class="col-xs-12">
		<?php $this->renderPartial('_schoolsPanel', array(
			'schoolModel' => $schoolModel,
		)); ?>
	</div>
</div>