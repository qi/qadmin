<?php
/**
 * @var UserAuth $usersModel
 */
?>
<div id="logs-panel" class="panel panel-default">
	<div class="panel-heading text-right">
		<h6 class="pull-left">Users</h6>
		<?php echo CHtml::dropDownList('users-page-size', Yii::app()->user->getState('usersGridPageSize'), array(
			10 => 10, 30 => 30, 50 => 50
		), array(
			'class' => 'form-control input-sm',
			'style' => 'width: 70px; float: right;',
		)); ?>
	</div>
	<div class="panel-body table-responsive">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'users',
			'dataProvider' => $usersModel->search(),
			'filter' => $usersModel,
			'itemsCssClass' => 'table table-condensed table-hover text-center',
			'pager' => array(
				'header' => false,
//				'cssFile' => $this->assetsPath . '/css/shk.admin.pager.css'
			),
			'rowCssClassExpression' => function ($row, $data) {
					switch ($data->metadata->state) {
						case "disabled":
							$rowClass = "warning";
							break;
						case "deleted":
							$rowClass = "danger";
							break;
						default:
							$rowClass = "default";
					}
					return $rowClass;
				},
			'columns' => array(
				array(
					'type' => 'raw',
					'value' => 'CHtml::tag(\'i\', array(
						\'class\' => $data->status == \'online\' ? \'fa fa-circle text-success\' : \'fa fa-circle text-muted\',
						\'title\' => $data->status == \'online\' ? \'Online\' : \'Offline\',
						\'style\' => \'font-size: xx-small;\',
					))',
					'headerHtmlOptions' => array(
						'style' => 'width: 25px; min-width: 25px; max-width: 25px;',
					),
				),
				array(
					'name' => 'id',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-1',
					),
				),
				array(
					'name' => 'public_id',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-3',
					),
				),
				array(
					'name' => 'shk_id',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-1',
					),
				),

				array(
					'name' => 'name',
					'value' => '$data->fullName',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-2',
					),
				),
				array(
					'name' => 'email',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-2',
					),
				),
				array(
					'name' => 'role',
					'filter' => array(
						'admin' => 'admin',
						'moderator' => 'moderator',
						'student' => 'student',
					),
					'headerHtmlOptions' => array(
						'class' => 'col-xs-1',
					),
				),
				array(
					'name' => 'schoolName',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-2',
					),
				),
				array(
					'type' => 'raw',
					'value' => 'CHtml::link("<i class=\'fa fa-lg fa-bar-chart-o\'></i>", null, array(
						"data-toggle" => "modal",
						"data-target" => "#userStatsModal",
						"data-source" => "/admin/users/ajaxUserStatsModal?id=$data->id",
						"style" => "cursor:pointer",
						"title" => "' . Yii::t('qAdmin', 'Statistics') . '"
					))',
					'headerHtmlOptions' => array(
						'style' => 'width: 25px; min-width: 25px; max-width: 25px;',
					),
				),
				array(
					'type' => 'raw',
					'value' => 'CHtml::link("<i class=\'fa fa-lg fa-map-marker \'></i>", null, array(
						"data-toggle" => "modal",
						"data-target" => "#userLocationModal",
						"data-source" => "/admin/users/ajaxUserLocationModal?id=$data->id",
						"style" => "cursor:pointer",
						"title" => "' . Yii::t('User', 'Location') . '"
					))',
					'headerHtmlOptions' => array(
						'style' => 'width: 25px; min-width: 25px; max-width: 25px;',
					),
				),
				array(
					'type' => 'raw',
					'value' => 'CHtml::link("<i class=\'fa fa-lg fa-edit\'></i>", null, array(
						"data-toggle" => "modal",
						"data-target" => "#userEditModal",
						"data-source" => "/admin/users/ajaxUserEditModal?id=$data->id",
						"style" => "cursor:pointer",
						"title" => "' . Yii::t('qAdmin', 'Edit') . '"
					))',
					'headerHtmlOptions' => array(
						'style' => 'width: 25px; min-width: 25px; max-width: 25px;',
					),
				),
			),
		)); ?>
	</div>
</div>