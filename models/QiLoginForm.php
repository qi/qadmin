<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class QiLoginForm extends CFormModel
{
	public $email;
	public $password;
	public $rememberMe;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that email and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// email and password are required
			array('email, password', 'required'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe' => Yii::t('qadmin', 'Remember me next time'),
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute, $params)
	{
		if (!$this->hasErrors()) {
			if (!$this->getIdentity()->authenticate())
				$this->addError('password', 'Incorrect email or password.');
		}
	}

	/**
	 * Logs in the user using the given email and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		$this->getIdentity()->authenticate();
		if ($this->getIdentity()->errorCode === UserIdentity::ERROR_NONE) {
			Yii::app()->user->login($this->getIdentity(), $this->rememberMe ? 60 * 60 * 24 * 7 : 0);
			Yii::app()->user->setFlash(rand(), CJSON::encode([
				'type' => 'info', 'width' => '360px',
				'title' => Yii::t('qadmin', 'Welcome, {name}!', ['{name}' => Yii::app()->user->model->fullName]),
				'text' => Yii::t('qadmin', 'You can use the {help-icon} icon to view a help.', [
					'{help-icon}' => '<i class="fa fa-question-circle"></i>'
				]),
			]));
			return true;
		} else
			return false;
	}

	protected function getIdentity()
	{
		if ($this->_identity === null) {
			$module = Yii::app()->controller->module;
			if (class_exists($module->userIdentityClassName))
				$this->_identity = new $module->userIdentityClassName($this->email, $this->password);
			else $this->_identity = new CUserIdentity($this->email, $this->password);
		}
		return $this->_identity;
	}
}
