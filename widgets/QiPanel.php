<?php

Yii::import('zii.widgets.CPortlet');

/**
 * Class QiPanel
 * TODO: make predefined buttons
 * @author andcherv <andcherv@gmail.com>
 */
class QiPanel extends CPortlet
{
	/**
	 * @var array {@link CPortlet}
	 */
	public $htmlOptions = array('class' => 'panel panel-default');
	/**
	 * @var string {@link CPortlet}
	 */
	public $contentCssClass = 'collapse';
	/**
	 * @var string {@link CPortlet}
	 */
	public $decorationCssClass = 'panel-heading';
	/**
	 * @var bool whether the panel is collapsable or not.
	 */
	public $collapsible = true;
	public $collapsed = false;
	/**
	 * @var null|array decoration buttons array.
	 */
	public $buttons;
	/**
	 * @var null|string panel script.
	 */
	public $js;

	/**
	 * Registers scripts.
	 */
	public function init()
	{
		$this->htmlOptions['class'] = 'QiPanel ' . $this->htmlOptions['class'];
		if (!array_key_exists('name', $this->htmlOptions))
			$this->htmlOptions['name'] = $this->id;
		if ($this->collapsed !== true)
			$this->contentCssClass .= ' in';
		if (isset($this->js))
			Yii::app()->clientScript->registerScript("QiPanel_js_{$this->id}", $this->js, CClientScript::POS_READY);
		parent::init();
	}

	/**
	 * Renders panel heading if title is set.
	 */
	protected function renderDecoration()
	{
		if (isset($this->title)) {
			echo CHtml::openTag('div', array(
				'class' => $this->decorationCssClass,
			));
			if ($this->getIsCollapsable() === true) {
				echo CHtml::openTag('div', array(
					'class' => 'pull-left panel-toggle',
					'data-toggle' => "collapse",
					'data-target' => "#{$this->id} .collapse",
					'data-parent' => "#$this->id"
				));
				echo CHtml::tag('i', array(
					'class' => $this->collapsed !== true ? 'fa fa-lg fa-angle-up' : 'fa fa-lg fa-angle-down',
				), '');
				echo CHtml::closeTag('div');
			}
			echo CHtml::openTag('span', array(
				'class' => "{$this->titleCssClass}",
			));
			echo $this->title;
			echo CHtml::closeTag('h6');
			if (isset($this->buttons)) {
				echo CHtml::openTag('div', array(
					'class' => 'panel-buttons pull-right text-right',
					'style' => 'width: 50%', //@todo move to styles
				));
				foreach ($this->buttons as $button)
					echo $button;
				echo CHtml::closeTag('div');
			}
			echo CHtml::closeTag('div');
		}
	}

	/**
	 * Returns whether the panel is collapsable or not.
	 * @return bool
	 */
	protected function getIsCollapsable()
	{
		return isset($this->title) && $this->collapsible === true;
	}
}