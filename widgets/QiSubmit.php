<?php
/**
 * @author andcherv
 */
class QiSubmit extends CWidget
{
	public $confirm = true;
	public $htmlOptions = array();
	public $size = 'default';
	public $type = 'submit';

	public function init()
	{
		$this->htmlOptions['name'] = strtolower($this->type);

		switch (strtolower($this->type)) {
			case 'create':
				$this->type = 'btn-primary';
				if (!isset($this->htmlOptions['value']))
					$this->htmlOptions['value'] = Yii::t('qadmin', 'Create');
				break;
			case 'save':
				$this->type = 'btn-primary';
				if (!isset($this->htmlOptions['value']))
					$this->htmlOptions['value'] = Yii::t('qadmin', 'Save');
				break;
			case 'delete':
				$this->type = 'btn-danger';
				if (!isset($this->htmlOptions['value']))
					$this->htmlOptions['value'] = Yii::t('qadmin', 'Delete');
				break;
			case 'cancel':
				$this->type = 'btn-default';
				if (!isset($this->htmlOptions['value']))
					$this->htmlOptions['value'] = Yii::t('qadmin', 'Cancel');
				break;
			default:
				$this->type = 'btn-default';
				if (!isset($this->htmlOptions['value']))
					$this->htmlOptions['value'] = Yii::t('qadmin', 'Submit');
		}

		switch ($this->size) {
			case 'small':
				$this->size = 'btn-sm';
				break;
			case 'large':
				$this->size = 'btn-lg';
				break;
			default:
				$this->size = '';
		}

		if (!isset($this->htmlOptions['id']))
			$this->htmlOptions['id'] = __CLASS__ . '_' . $this->htmlOptions['name'];

		if (isset($this->htmlOptions['class']))
			$this->htmlOptions['class'] .= ' ' . __CLASS__;
		else $this->htmlOptions['class'] = __CLASS__;
		$this->htmlOptions['class'] .= " btn $this->type $this->size";

		if ($this->confirm == true) {
			if (!isset($this->htmlOptions['data-placement']))
				$this->htmlOptions['data-placement'] = 'auto';

			if (!isset($this->htmlOptions['title']))
				$this->htmlOptions['title'] = Yii::t('qadmin', 'Are you sure?');

			Yii::app()->clientScript->scriptMap['jquery.js'] = false;
			Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;

			Yii::app()->clientScript->registerScript(
				$this->htmlOptions['id'],
				$this->getScript(),
				CClientScript::POS_READY
			);
		}

		ksort($this->htmlOptions);
	}

	public function run()
	{
		if ($this->confirm == true)
			echo CHtml::button($this->htmlOptions['value'], $this->htmlOptions);
		else echo CHtml::submitButton($this->htmlOptions['value'], $this->htmlOptions);
	}

	protected function getScript()
	{
		$id = $this->htmlOptions['id'];

		$confirmButton = CHtml::submitButton(
			$this->htmlOptions['value'],
			CMap::mergeArray($this->htmlOptions, array(
				'id' => $id . '_confirm',
				'class' => "btn $this->type $this->size",
				'title' => $this->htmlOptions['value'],
			)));

		$cancelButton = CHtml::button(
			Yii::t('qadmin', 'Cancel'),
			CMap::mergeArray($this->htmlOptions, array(
				'id' => $id . '_cancel',
				'class' => "btn btn-default $this->size",
				'value' => Yii::t('qadmin', 'Cancel'),
				'title' => Yii::t('qadmin', 'Cancel'),
			)));

		$js = 'jQuery("#' . $id . '").popover({html: true,content: \'<div class="qSubmit-content">' . $cancelButton . $confirmButton . '</div>\'});
		jQuery(document).on("click", "#' . $id . '_cancel", function() {jQuery(this).closest(".popover").siblings("#' . $id . '").popover("hide");});
		jQuery(document).on("click", "#' . $id . '_confirm", function() {jQuery(this).closest(".popover").qP().loading().on();});';

		return $js;
	}
}