<?php
/**
 * Pages help management page.
 * @var QiPageHelp $helpModel
 */
?>

<?php $this->widget('qi.qadmin.widgets.QiPageHeader', array(
	'header' => Yii::t('settings', 'Page Help Management'),
	'small' => Yii::t('settings', 'Create and edit quick help modals content'),
)); ?>

<div id="page-content">
	<div class="row">
		<div class="col-md-12">
			<?php $this->renderPartial('help/_panelGrid', array('helpModel' => $helpModel)); ?>
		</div>
	</div>
</div>
