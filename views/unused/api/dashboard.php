<?php
/**
 * @var CController $this
 * @var qApiKey $keysModel
 * @var qApiSettings[] $settingsModels
 * @var qApiLog $logsModel
 * @var array $stats
 */
$this->widget('qModal', array(
	'htmlOptions' => array(
		'id' => 'settingsEditModal',
	),
));
$this->widget('qModal', array(
	'htmlOptions' => array(
		'id' => 'keyEditModal',
	),
));
$this->widget('qModal', array(
	'htmlOptions' => array(
		'id' => 'logDetailsModal',
		'width' => '1024px',
		'data-backdrop' => true,
	),
));
?>

<?php $this->renderPartial('/layouts/_defaultPageHeader', array(
	'header' => Yii::t('qPanel', 'API Dashboard'),
	'small' => Yii::t('qPanel', 'Statistics, logs & configuration'),
)); ?>

<div id="page-content" class="row">
	<div class="col-md-5">
		<?php $this->renderPartial('_statsPanel'); ?>
		<?php $this->renderPartial('_settingsPanel', array(
			'settingsModels' => $settingsModels,
		)); ?>
	</div>
	<div class="col-md-7">
		<?php $this->renderPartial('_activityPanel', array(
			'keysModel' => $keysModel,
		));?>
		<?php $this->renderPartial('_keysPanel', array(
			'keysModel' => $keysModel,
		));?>
	</div>
	<div class="col-xs-12">
		<?php $this->renderPartial('_logsPanel', array(
			'logsModel' => $logsModel,
		));?>
	</div>
</div>

<script>
	$(document).ready(function () {
		setInterval(function () {
			$.fn.yiiGridView.update("api-logs");
			$(document).qP().notifications().update();
		}, 1000 * 15);
		$("#logs-page-size").on("change", function () {
			$.fn.yiiGridView.update("api-logs", {
				data: {apiLogPageSize: $(this).val()}
			});
		});
	});
</script>