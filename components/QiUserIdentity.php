<?php

/**
 * Class QiUserIdentity
 */
class QiUserIdentity extends CUserIdentity
{
	/**
	 * @return bool
	 */
	public function authenticate()
	{
		Yii::app()->user->setState('role', 'user');
		$users = array(
			'admin@example.com' => array(
				'role' => 'admin',
				'email' => 'admin@example.com',
				'password' => 'admin',
			),
			'demo@example.com' => array(
				'role' => 'user',
				'email' => 'demo@example.com',
				'password' => 'demo',
			),
		);
		if (!isset($users[$this->username]))
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		elseif ($users[$this->username]['password'] !== $this->password)
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else {
			Yii::app()->user->setState('role', $users[$this->username]['role']);
			Yii::app()->user->setState('email', $users[$this->username]['email']);
			$this->errorCode = self::ERROR_NONE;
		}
		return !$this->errorCode;
	}

	public function getIsAdmin()
	{
		return Yii::app()->user->getState('role') == 'admin';
	}
}