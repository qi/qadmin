<style>
	.api-doc-section h1,
	.api-doc-section h2,
	.api-doc-section h3,
	.api-doc-section h4,
	.api-doc-section h5,
	.api-doc-section h6 {
		color: rgb(10, 146, 156);
		margin: 8px 10px 18px 10px;
	}

	.api-doc-section p, .api-doc-section pre, .api-doc-section ul {
		margin: 8px 10px 18px 10px;
	}

	.api-doc-section .page-header > h3 {
		font-weight: 700;
	}

	.api-doc-section {
		margin-bottom: 100px;
	}
	.api-doc-section:last-of-type {
		margin-bottom: 600px;
	}
</style>

<div id="resources" class="page-header anchor">
	<h2>Resources</h2>
</div>

<!-- UPLOADS -->
<section id="uploads" class="api-doc-section">

	<div class="page-header">
		<h3><i class="fa fa-exclamation-circle"></i> /uploads/ <small>Uploads</small></h3>
	</div>

	<!-- POST UPLOADS -->
	<h4>POST /uploads/ <small>Upload a file</small></h4>
	<p><b>Content-Type: multipart/form-data</b></p>
	<h5>Body</h5>
	<ul>
		<li><b>file</b></li>
		<li><b>type</b> ("user_photo")</li>
		<li><i>name</i></li>
		<li><i>description</i></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- GET UPLOADS -->
	<h4>GET /uploads/ <small>View uploads</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><i>public_id</i> (item id)</li>
	</ul>
	<p>or</p>
	<ul>
		<li><i>type</i> ("user_photo")</li>
		<li><i>query</i> (search string)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"pagination":{"total":1, "limit":30, "offset":0},
"data":[
	{
		"public_id":"5498c8058018440b320546fc1b3eac8c",
		"type":"user_photo",
		"name":"c4cff59322f36e3f6297442dcd61e14c",
		"description":"",
		"url":"http%3A%2F%2Fs-kid.dev%2Fuploads2ebf2205fa93e570dce52a4e454f9d74%2Fc4cff59322f36e3f6297442dcd61e14c.jpg",
		"mime":"image\/jpeg",
		"size":30778,
		"author":{
			"public_id":"2ebf2205fa93e570dce52a4e454f9d74"
		},
		"metadata":{
			"shk_id":"",
			"vk_id":"",
			"create_time":"2014-05-23T09:42:45+00:00",
			"update_time":"2014-05-23T09:42:45+00:00"
		}
	}
]
}</pre>

	<!-- DELETE UPLOADS -->
	<h4>DELETE /uploads/ <small>Delete an upload</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success"
}</pre>

</section>

<!-- MESSAGES -->
<section id="messages" class="api-doc-section anchor">

	<div class="page-header">
		<h3>/messages/ <small>Messages</small></h3>
	</div>

	<!-- POST MESSAGES -->
	<h4>POST /messages/ <small>Create a message</small></h4>
	<p>Default <u>status is "new"</u> and <u>sender_id defaults to a current user</u>.</p>
	<h5>Body</h5>
	<ul>
		<li><b>receiver_id</b></li>
		<li><b>content</b></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- GET MESSAGES -->
	<h4>GET /messages/ <small>View messages</small></h4>
	<p>Response contains only messages related to a current user ordered by creation date descending.</p>
	<h5>Params</h5>
	<ul>
		<li><i>public_id</i> (item id)</li>
	</ul>
	<p>or</p>
	<ul>
		<li><i>sender_id</i></li>
		<li><i>receiver_id</i></li>
		<li><i>interlocutor_id</i> (messages related to the user)</li>
		<li><i>status</i> ("new", "read")</li>
		<li><i>query</i> (message content search string)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"pagination":{"total":1, "limit":30, "offset":0},
"data":[
	{
		"public_id":"5498c8058018440b320546fc1b3eac8c",
		"sender":{
			"public_id":"e6a68d3a06b97d61170db601b41ab6e5"
		 },
		"receiver":{
			"public_id":"04289dc3f5fc8a82953a72187d05e114"
		},
		"content":"Test response",
		"status":"new",
		"metadata":{
			"shk_id":"",
			"vk_id":"",
			"create_time":"2014-05-23T09:42:45+00:00",
			"update_time":"2014-05-23T09:42:45+00:00"
		}
	}
]
}</pre>

	<!-- PUT, PATCH MESSAGES-->
	<h4>PUT, PATCH /messages/ <small>Update a message</small></h4>
	<p>Only related users are allowed.</p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Body</h5>
	<ul>
		<li><i>status:</i> ("new" or "read", updates only by a receiver)</li>
		<li><i>receiver_id:</i> (updates only by a sender)</li>
		<li><i>content:</i> (updates only by a sender)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- DELETE MESSAGES -->
	<h4>DELETE /messages/ <small>Delete a message</small></h4>
	<p>Only sender is allowed.</p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success"
}</pre>

</section>

<!-- SUBJECTS -->
<section id="subjects" class="api-doc-section anchor">

	<div class="page-header">
		<h3> /subjects/ <small>Subjects</small></h3>
	</div>

	<!-- POST SUBJECTS -->
	<h4>POST /subjects/ <small>Create a subject</small></h4>
	<p></p>
	<h5>Body</h5>
	<ul>
		<li><b>name</b></li>
		<li><b>community_id</b></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- GET SUBJECTS -->
	<h4>GET /subjects/ <small>View subjects</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><i>public_id</i> (item id)</li>
	</ul>
	<p>or</p>
	<ul>
		<li><i>school_id/community_id</i></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"pagination":{"total":1, "limit":30, "offset":0},
"data":[
	{
		"public_id":"5498c8058018440b320546fc1b3eac8c",
		"name":"Mathematics",
		"metadata":{
			"shk_id":"",
			"vk_id":"",
			"create_time":"2014-05-23T09:42:45+00:00",
			"update_time":"2014-05-23T09:42:45+00:00"
		}
	}
]
}</pre>

	<!-- PUT, PATCH SUBJECTS -->
	<h4>PUT, PATCH /subjects/ <small>Update a subject</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Body</h5>
	<ul>
		<li><i>name</i></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- DELETE SUBJECTS -->
	<h4>DELETE /subjects/ <small>Delete a subject</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success"
}</pre>

</section>

<!-- TEACHERS -->
<section id="teachers" class="api-doc-section anchor">

	<div class="page-header">
		<h3> /teachers/ <small>Teachers</small></h3>
	</div>

	<!-- POST TEACHERS -->
	<h4>POST /teachers/ <small>Create a teacher</small></h4>
	<p></p>
	<h5>Body</h5>
	<ul>
		<li><b>name</b></li>
		<li><b>community_id</b></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- GET TEACHERS -->
	<h4>GET /teachers/ <small>View teachers</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><i>public_id</i> (item id)</li>
	</ul>
	<p>or</p>
	<ul>
		<li><i>school_id/community_id</i></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"pagination":{"total":1, "limit":30, "offset":0},
"data":[
	{
		"public_id":"5498c8058018440b320546fc1b3eac8c",
		"name":"James Bond",
		"metadata":{
			"shk_id":"",
			"vk_id":"",
			"create_time":"2014-05-23T09:42:45+00:00",
			"update_time":"2014-05-23T09:42:45+00:00"
		}
	}
]
}</pre>

	<!-- PUT, PATCH TEACHERS -->
	<h4>PUT, PATCH /teachers/ <small>Update a teacher</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Body</h5>
	<ul>
		<li><i>name</i></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- DELETE TEACHERS -->
	<h4>DELETE /teachers/ <small>Delete a teacher</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success"
}</pre>

</section>

<!-- GRID -->
<section id="grid" class="api-doc-section anchor">

	<div class="page-header">
		<h3> /grid/ <small>Schedule grid items</small></h3>
	</div>

	<!-- POST GRID -->
	<h4>POST /grid/ <small>Create a grid item</small></h4>
	<p></p>
	<h5>Body</h5>
	<ul>
		<li><b>community_id</b></li>
		<li><b>start_time</b> (hh:mm:ss)</li>
		<li><b>end_time</b> (hh:mm:ss)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- GET GRID -->
	<h4>GET /grid/ <small>View grid items</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><i>public_id</i> (item id)</li>
	</ul>
	<p>or</p>
	<ul>
		<li><i>community_id</i></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"pagination":{"total":1, "limit":30, "offset":0},
"data":[
	{
		"public_id":"5498c8058018440b320546fc1b3eac8c",
		"community":{
			"public_id":"840ca51d290006917d0565437dd7b4df"
		},
		"start_time":"08:00:00",
		"end_time":"08:45:00",
		"metadata":{
			"shk_id":"",
			"vk_id":"",
			"create_time":"2014-05-23T09:42:45+00:00",
			"update_time":"2014-05-23T09:42:45+00:00"
		}
	}
]
}</pre>

	<!-- PUT, PATCH GRID -->
	<h4>PUT, PATCH /grid/ <small>Update a grid item</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Body</h5>
	<ul>
		<li><i>community_id</i></li>
		<li><i>start_time</i> (hh:mm:ss)</li>
		<li><i>end_time</i> (hh:mm:ss)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- DELETE GRID -->
	<h4>DELETE /grid/ <small>Delete a grid item</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success"
}</pre>

</section>

<!-- PERIODS -->
<section id="periods" class="api-doc-section anchor">

	<div class="page-header">
		<h3> /periods/ <small>Periods</small></h3>
	</div>

	<!-- POST PERIODS -->
	<h4>POST /periods/ <small>Create a period</small></h4>
	<p></p>
	<h5>Body</h5>
	<ul>
		<li><b>name</b></li>
		<li><b>type</b> ("year", "term")</li>
		<li><b>community_id</b></li>
		<li><b>start_time</b></li>
		<li><b>end_time</b></li>
		<li><i>parent_id</i> (parent period id)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- GET PERIODS -->
	<h4>GET /periods/ <small>View periods</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><i>public_id</i> (item id)</li>
	</ul>
	<p>or</p>
	<ul>
		<li><i>community_id</i></li>
		<li><i>parent_id</i> (parent period id)</li>
		<li><i>type</i> ("year", "term")</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"pagination":{"total":1, "limit":30, "offset":0},
"data":[
	{
		"public_id":"5498c8058018440b320546fc1b3eac8c",
		"name":"2nd semester",
		"type":"term",
		"start_time":"2014-01-01T00:00:00+00:00",
		"end_time":"2014-05-31T23:59:59+00:00",
		"parent":{
			"public_id":"7498c8058018440b320546fc1b3eac8c"
		},
		"metadata":{
			"shk_id":"",
			"vk_id":"",
			"create_time":"2014-05-23T09:42:45+00:00",
			"update_time":"2014-05-23T09:42:45+00:00"
		}
	}
]
}</pre>

	<!-- PUT, PATCH PERIODS -->
	<h4>PUT, PATCH /periods/ <small>Update a period</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Body</h5>
	<ul>
		<li><i>name</i></li>
		<li><i>type</i> ("year", "term")</li>
		<li><i>community_id</i></li>
		<li><i>start_time</i></li>
		<li><i>end_time</i></li>
		<li><i>parent_id</i> (parent period id)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- DELETE PERIODS -->
	<h4>DELETE /periods/ <small>Delete a period</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success"
}</pre>

</section>

<!-- SCHEDULES -->
<section id="schedules" class="api-doc-section anchor">

	<div class="page-header">
		<h3> /schedules/ <small>Schedules</small></h3>
	</div>

	<!-- POST SCHEDULES -->
	<h4>POST /schedules/ <small>Create a schedule</small></h4>
	<p></p>
	<h5>Body</h5>
	<ul>
		<li><b>community_id</b></li>
		<li><b>period_id</b></li>
		<li><i>days_count</i> (days count in a cycle, <u>defaults to 7</u>)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- GET SCHEDULES -->
	<h4>GET /schedules/ <small>View schedules</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><i>public_id</i> (item id)</li>
	</ul>
	<p>or</p>
	<ul>
		<li><i>school_id/community_id</i></li>
		<li><i>period_id</i></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"pagination":{"total":1, "limit":30, "offset":0},
"data":[
	{
		"public_id":"5498c8058018440b320546fc1b3eac8c",
		"community":{
			"public_id":"6498c8058018440b320546fc1b3eac8c"
		},
		"period":{
			"public_id":"4498c8058018440b320546fc1b3eac8c"
		},
		"days_count":"7",
		"metadata":{
			"shk_id":"",
			"vk_id":"",
			"create_time":"2014-05-23T09:42:45+00:00",
			"update_time":"2014-05-23T09:42:45+00:00"
		}
	}
]
}</pre>

	<!-- PUT, PATCH SCHEDULES -->
	<h4>PUT, PATCH /schedules/ <small>Update a schedule</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Body</h5>
	<ul>
		<li><i>community_id</i></li>
		<li><i>period_id</i></li>
		<li><i>days_count</i> (days count in a cycle, <u>defaults to 7</u>)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- DELETE SCHEDULES -->
	<h4>DELETE /schedules/ <small>Delete a schedule</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success"
}</pre>

</section>

<!-- LESSONS -->
<section id="lessons" class="api-doc-section anchor">

	<div class="page-header">
		<h3> /lessons/ <small>Lessons</small></h3>
	</div>

	<!-- POST LESSONS -->
	<h4>POST /lessons/ <small>Create a lesson</small></h4>
	<p></p>
	<h5>Body</h5>
	<ul>
		<li><b>schedule_id</b></li>
		<li><b>grid_id</b></li>
		<li><b>subject_id</b></li>
		<li><b>teacher_id</b></li>
		<li><b>day</b> (e.g. "1")</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- GET LESSONS -->
	<h4>GET /lessons/ <small>View lessons</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><i>public_id</i> (item id)</li>
	</ul>
	<p>or</p>
	<ul>
		<li><i>schedule_id</i></li>
		<li><i>grid_id</i></li>
		<li><i>subject_id</i></li>
		<li><i>teacher_id</i></li>
		<li><i>day</i></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"pagination":{"total":1, "limit":30, "offset":0},
"data":[
	{
		"public_id":"5498c8058018440b320546fc1b3eac8c",
		"schedule":{
			"public_id":"fre8c8058018440b320546fc1b3eac8c"
		},
		"grid":{
			"public_id":"iop8c8058018440b320546fc1b3eac8c"
		},
		"subject":{
			"public_id":"sdfc8058018440b320546fc1b3eac8c",
			"name":"History"
		},
		"teacher":{
			"public_id":"cvb8c8058018440b320546fc1b3eac8c",
			"name":"James Bond"
		},
		"day":"1",
		"metadata":{
			"shk_id":"",
			"vk_id":"",
			"create_time":"2014-05-23T09:42:45+00:00",
			"update_time":"2014-05-23T09:42:45+00:00"
		}
	}
]
}</pre>

	<!-- PUT, PATCH LESSONS -->
	<h4>PUT, PATCH /lessons/ <small>Update a lesson</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Body</h5>
	<ul>
		<li><i>schedule_id</i></li>
		<li><i>grid_id</i></li>
		<li><i>suiject_id</i></li>
		<li><i>teacher_id</i></li>
		<li><i>day</i> (e.g. "1")</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- DELETE LESSONS -->
	<h4>DELETE /lessons/ <small>Delete a lesson</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success"
}</pre>

</section>

<!-- HOMEWORKS -->
<section id="homeworks" class="api-doc-section anchor">

	<div class="page-header">
		<h3> /homeworks/ <small>Homeworks</small></h3>
	</div>

	<!-- POST HOMEWORKS -->
	<h4>POST /homeworks/ <small>Create a homework</small></h4>
	<p></p>
	<h5>Body</h5>
	<ul>
		<li><b>lesson_id</b></li>
		<li><b>content</b></li>
		<li><b>due_time</b></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- GET HOMEWORKS -->
	<h4>GET /homeworks/ <small>View homeworks</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><i>public_id</i> (item id)</li>
	</ul>
	<p>or</p>
	<ul>
		<li><i>lesson_id</i></li>
		<li><i>query</i></li>
		<li><i>from_time</i> (defaults to request time)</li>
		<li><i>due_time</i> (filters homeworks by its due_time attribute which is beetween from_time and specified due_time url param, note that the param should be url encoded, e.g. 2014-07-07T10%3A00%3A00%2B00%3A00)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"pagination":{"total":1, "limit":30, "offset":0},
"data":[
	{
		"public_id":"5498c8058018440b320546fc1b3eac8c",
		"lesson":{
			"public_id":"8498c8058018440b320546fc1b3eac8c"
		},
		"content":"Do some stuff",
		"due_time":"2014-07-01T09:00:00+00:00",
		"metadata":{
			"shk_id":"",
			"vk_id":"",
			"create_time":"2014-05-23T09:42:45+00:00",
			"update_time":"2014-05-23T09:42:45+00:00"
		}
	}
]
}</pre>

	<!-- PUT, PATCH HOMEWORKS -->
	<h4>PUT, PATCH /homeworks/ <small>Update a homework</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Body</h5>
	<ul>
		<li><i>lesson_id</i></li>
		<li><i>content</i></li>
		<li><i>due_time</i></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"data":{
	"public_id":"5498c8058018440b320546fc1b3eac8c"
}
}</pre>

	<!-- DELETE HOMEWORKS -->
	<h4>DELETE /homeworks/ <small>Delete a homework</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><b>public_id</b> (item id)</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success"
}</pre>

</section>

<!-- FILES -->
<section id="files" class="api-doc-section">

	<div class="page-header">
		<h3><i class="fa fa-exclamation-circle"></i> /files/ <small>Homework files</small></h3>
	</div>

	<!-- GET FILES -->
	<h4>GET /files/ <small>View homework files</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><i>public_id</i> (item id)</li>
	</ul>
	<p>or</p>
	<ul>
		<li><i>homework_id</i></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"pagination":{"total":1, "limit":30, "offset":0},
"data":[
	{
		"public_id":"5498c8058018440b320546fc1b3eac8c",
		"homework":[
			"public_id":"hg57d8058018440b320546fc1b3eac8c",
		],
		"name":"File",
		"url":"https:\/\/control.shkolnaya-karta.ru\/content\/hw\/85767\/001.jpg",
		"metadata":{
			"shk_id":"",
			"vk_id":"",
			"create_time":"2014-05-23T09:42:45+00:00",
			"update_time":"2014-05-23T09:42:45+00:00"
		}
	}
]
}</pre>

</section>

<!-- SCORES -->
<section id="scores" class="api-doc-section anchor">

	<div class="page-header">
		<h3> /scores/ <small>Scores</small></h3>
	</div>

	<!-- GET SCORES -->
	<h4>GET /scores/ <small>View scores</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><i>public_id</i> (item id)</li>
	</ul>
	<p>or</p>
	<ul>
		<li><i>subject_id</i></li>
		<li><i>lesson_id</i></li>
		<li><i>period_id</i></li>
		<li><i>type</i> ("lesson" or "period")</li>
		<li><i>query</i></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"pagination":{"total":1, "limit":30, "offset":0},
"data":[
	{
		"public_id":"5498c8058018440b320546fc1b3eac8c",
		"user":{
			"public_id":"56fgc8058018440b320546fc1b3eac8c"
		},
		"subject":{
			"public_id":"dfrtc8058018440b320546fc1b3eac8c"
		},
		"period":{
			"public_id":"1er1238058018440b320546fc1b3eac8c"
		},
		"value":"5",
		"description":"All stuff well done",
		"put_time":"2014-05-23T09:42:45+00:00",
		"metadata":{
			"shk_id":"",
			"vk_id":"",
			"create_time":"2014-05-23T09:42:45+00:00",
			"update_time":"2014-05-23T09:42:45+00:00"
		}
	},
	{
		"public_id":"5498c8058018440b320546fc1b3eac8c",
		"user":{
			"public_id":"56fgc8058018440b320546fc1b3eac8c"
		},
		"subject":{
			"public_id":"dfrtc8058018440b320546fc1b3eac8c"
		},
		"lesson":{
			"public_id":"1er1238058018440b320546fc1b3eac8c"
		},
		"value":"5",
		"description":"All stuff well done 2",
		"put_time":"2014-05-23T09:42:45+00:00",
		"metadata":{
			"shk_id":"",
			"vk_id":"",
			"create_time":"2014-05-23T09:42:45+00:00",
			"update_time":"2014-05-23T09:42:45+00:00"
		}
	}
]
}</pre>

</section>

<!-- LIVE -->
<section id="live" class="api-doc-section">

	<div class="page-header">
		<h3><i class="fa fa-exclamation-circle"></i> /live/ <small>Live Map</small></h3>
	</div>

	<!-- POST LIVE -->
	<h4>POST /live/ <small>Create a Live Map item</small></h4>
	<p></p>
	<h5>Body</h5>
	<ul>
		<li><b>latitude</b> (e.g. 55.75)</li>
		<li><b>longitude</b> (e.g. 37.6167)</li>
		<li><i>message</i> (e.g. "Hi, iPupils!")</li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success"
}</pre>

	<!-- GET LIVE -->
	<h4>GET /live/ <small>View a Live Map</small></h4>
	<p></p>
	<h5>Params</h5>
	<ul>
		<li><i>community_id</i></li>
	</ul>
	<h5>Response example</h5>
	<pre class="json">{
"status":"success",
"pagination":{"total":1, "limit":30, "offset":0},
"data":[
	{
		"user":{
			"public_id":"be6fdc0ecfc11a884eb2864e0bada3f2",
			"last_name":"Степанов",
			"first_name":"Степан",
			"photo_url":"http:\/\/s-kid.dev\/images\/icons\/default_user_male_128.png",
			"status":"online"
		},
		"latitude":"55.75",
		"longitude":"37.6167",
		"message":"Student10 Live Message",
		"change_time":"2014-07-08T13:03:21+00:00"
	}
]
}</pre>

</section>

<div id="codes" class="page-header anchor">
	<h2>Response codes</h2>
</div>

<section class="api-doc-section">
	<h4>200 Success <small>Generic success.</small></h4>
	<h4>201 Created <small>Resource item was successfully created.</small></h4>
	<h4>202 Accepted <small>Request accepted.</small></h4>
	<h4>401 Unauthorized <small>Typically indicates a problem with <a href="#auth">Authentication Headers</a>.</small></h4>
	<h4>403 Forbidden <small>Typically indicates a problem with user's authentication credentials (email, password).</small></h4>
	<h4>404 Not Found <small>Resource/item not found.</small></h4>
	<h4>405 Method Not Allowed <small>Method is not allowed for the resource.</small></h4>
	<p>e.g. you're trying to send POST request to a GET-only endpoint.</p>
	<h4>406 Not Acceptable <small>Server is not supporting any of acceptable by a client MIME types.</small></h4>
	<h4>409 Conflict <small>Validation error, duplicate, etc.</small></h4>
	<h4>415 Unsupported Media Type <small>Unsupported Content-Type of a body.</small></h4>
	<h4>500 Internal Server Error <small>Generic server error.</small></h4>
	<p>e.g. API has crashed for some reason.</p>
	<h4>501 Not Implemented <small>Method is not implemented.</small></h4>
	<p>e.g. you're trying to send PATCH request, which is not supported by the API in general.</p>
</section>