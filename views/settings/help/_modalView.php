<?php
/**
 * @author andcherv
 * @var ShkSystemPageHelp $pageHelp
 */
?>

<!-- Modal header -->
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"><?php echo isset($pageHelp->title) ? $pageHelp->title : '&nbsp;'; ?>
		<small><?php echo isset($pageHelp->url) ? $pageHelp->url : '&nbsp;'; ?></small>
	</h4>
</div>

<!-- Modal body -->
<div class="modal-body">
	<div class="row">
		<div class="col-xs-12">
			<?php echo isset($pageHelp->content) ? $pageHelp->content : '&nbsp;'; ?>
		</div>
	</div>
</div>