<?php

class QiPanelBody extends CWidget
{
	public function init()
	{
		echo CHtml::openTag('div', ['class' => 'panel-body']);
	}

	public function run()
	{
		echo CHtml::closeTag('div');
	}
}