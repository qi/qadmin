<?php

class CommunitiesController extends DefaultController
{
	public $layout = 'default';

	public function actionIndex()
	{
		$this->pageTitle = Yii::t('Community', 'Communitites');
		$this->addBreadcrumbs(array(
			Yii::t('Community', 'Communitites') => Yii::app()->createUrl('admin/communities'),
			Yii::t('Community', 'Communitites'),
		));

		$communityModel = new Community('search');
		$communityModel->unsetAttributes();
		if (isset($_GET[get_class($communityModel)]))
			$communityModel->attributes = $_GET[get_class($communityModel)];

		if (isset($_GET['communitiesGridPageSize']))
			Yii::app()->user->setState('communitiesGridPageSize', $_GET['communitiesGridPageSize']);

		$this->render('index', array(
			'communityModel' => $communityModel,
		));
	}

	public function actionAjaxUpdateStats()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$totalCount = Community::model()->with(array(
				'metadata:active', 'school.metadata:active',
			))->count();
			$publicCount = Community::model()->with(array(
				'metadata:active', 'school.metadata:active',
			))->count(array(
				'condition' => 'school.type=:public',
				'params' => array(':public' => School::TYPE_PUBLIC),
			));
			$connectedCount = Community::model()->with(array(
				'metadata:active', 'school.metadata:active',
			))->count(array(
				'condition' => 'school.type=:public',
				'params' => array(':public' => School::TYPE_CONNECTED),
			));
			echo CJSON::encode([
				'status' => 'success',
				'data' => [
					'total' => $totalCount,
					'public' => $publicCount,
					'connected' => $connectedCount,
					'pctPublic' => $totalCount > 0 ? round($publicCount * 100 / $totalCount, 1) : 0,
					'pctConnected' => $totalCount > 0 ? round($connectedCount * 100 / $totalCount, 1) : 0,
				],
			]);
		}
		Yii::app()->end();
	}
}