<?php

class DefaultController extends QiController
{
	public $assetsPath;
	public $assetsUrl;

	protected function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			Yii::app()->clientScript->registerMetaTag(Yii::app()->name, 'description');
			Yii::app()->clientScript->registerMetaTag(Yii::app()->name, 'keywords');
			if (isset($_GET['pageSize']))
				Yii::app()->user->setState('pageSize', $_GET['pageSize']);
			else
				Yii::app()->user->setState('pageSize', Yii::app()->params['defaultPageSize']);
		}
		return parent::beforeAction($action);
	}

	public function getViewPath()
	{
		return $this->module->basePath . '/views/default';
	}

	public function actionIndex()
	{
		if (isset($this->module->defaultRoute) && $this->module->defaultRoute != $this->module->id)
			$this->redirect(Yii::app()->createUrl($this->module->defaultRoute));
		$this->breadcrumbs = [Yii::t('default', 'Dashboard')];
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		$this->layout = 'empty';
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionLogin()
	{
		$this->layout = 'empty';
		$model = new QiLoginForm;
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if (isset($_POST['QiLoginForm'])) {
			$model->attributes = $_POST['QiLoginForm'];
			if ($model->validate() && $model->login())
				$this->redirect(Yii::app()->createUrl($this->module->defaultRoute));
		}
		$this->render('login', array('model' => $model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->createUrl($this->module->id . '/login'));
	}

	/**
	 * Returns JSON encoded user's flashes.
	 * @author andcherv
	 */
	public function actionAxGetFlashes()
	{
		$flashMessages = Yii::app()->user->getFlashes();
		echo CJSON::encode($flashMessages);
		Yii::app()->end();
	}
}