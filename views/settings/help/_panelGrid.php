<?php
/**
 * @var QiPageHelp $helpModel
 */
?>

<?php $this->beginWidget('qi.qadmin.widgets.QiPanel', array(
	'id' => 'help-panel',
	'title' => 'Page Help',
	'buttons' => array(
		CHtml::link('<i class="fa fa-bars"></i>', null, array(
			'class' => 'btn btn-default btn-sm pull-right',
		))
	),
)); ?>

<?php $this->widget('qi.qadmin.widgets.QiModal', array(
	'htmlOptions' => array(
		'id' => 'pageHelpEdit',
	),
)); ?>

<div class="panel-body table-responsive">
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'pageHelpGrid',
		'dataProvider' => $helpModel->search(),
		'filter' => $helpModel,
		'itemsCssClass' => 'table table-condensed table-hover text-center',
		'pager' => array(
			'header' => false,
			'cssFile' => 'qPanel-pager.css'
		),
		'columns' => array(
			array(
				'name' => 'language',
				'filter' => array(
					'en_us' => 'en_us',
					'ru_ru' => 'ru_ru',
				),
			),
			array(
				'name' => 'module',
				'filter' => array(
					'panel' => 'panel'
				),
				'headerHtmlOptions' => array(
					'class' => 'col-xs-2',
				),
			),
			array(
				'name' => 'controller',
				'headerHtmlOptions' => array(
					'class' => 'col-xs-2',
				),
			),
			array(
				'name' => 'action',
				'headerHtmlOptions' => array(
					'class' => 'col-xs-2',
				),
			),
			array(
				'name' => 'title',
				'headerHtmlOptions' => array(
					'class' => 'col-xs-2',
				),
			),
			array(
				'name' => 'content',
				'type' => 'raw',
				'headerHtmlOptions' => array(
					'class' => 'col-xs-4',
				),
				'htmlOptions' => array(
					'class' => 'text-left',
				),
			),
			array(
				'type' => 'raw',
				'value' => 'CHtml::link("<i class=\'fa fa-lg fa-cog\'></i>", null, array(
									"data-toggle" => "modal",
									"data-target" => "#pageHelpEdit",
									"data-source" => "/qadmin/settings/ajaxEditPageHelp?id=$data->id",
									"title" => "' . Yii::t('qPanel', 'Edit Page Help') . '"
								))',
			),
		)
	));    ?>
</div>
<?php $this->endWidget('help-panel'); ?>
