<?php
/**
 * @author andcherv
 * @var $years ShkSchoolAcademicYear[]
 * @var $school School
 * @var int $panelSchoolId
 */
?>

<!-- Page header -->
<?php $this->renderPartial('//layouts/_adminPageHeader', array(
	'header' => Yii::t('ShkSchool', 'Academic Years'),
	'small' => Yii::app()->shkPanel->filterString,
));?>

<!-- Buttons bar -->
<div id="buttons-bar" class="row">
	<?php !isset($panelSchoolId) ? $disabled = ' disabled' : $disabled = '';
	echo CHtml::link('<span class="glyphicon glyphicon-plus" style="margin-right:5px;"></span>' . Yii::t('admin', 'Add'), false, array(
			'class' => 'btn btn-sm btn-default' . $disabled,
			'data-toggle' => 'modal',
			'data-target' => '#academicYearParams',
			'data-source' => '/admin/school/year',
		)); ?>
</div>

<!-- Academic year parameters modal -->
<?php $this->widget('admin.widgets.qModal', array(
	'htmlOptions' => array(
		'id' => 'academicYearParams',
		'width' => '400px',
	),
)); ?>

<!--  Academic years table -->
<div id="table" class="row table-responsive">
	<?php if (isset($years)) {
		$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'academic-years',
			'afterAjaxUpdate' => 'shk.reloadDatePicker',
			'dataProvider' => $years->search(),
			'filter' => $years,
			'itemsCssClass' => 'table table-condensed table-hover text-center',
			'rowCssClassExpression' => function ($row, $data) {
					$rowClass = '';
					if (time() >= $data->start_time && time() <= $data->end_time) {
						$rowClass = 'success';
					}
					return $rowClass;
				},
			'pager' => array(
				'header' => false,
				'cssFile' => $this->assetsPath . '/css/shk.admin.pager.css'
			),
			'columns' => array(
				array(
					'name' => 'school_id',
					'filter' => false,
					'value' => 'CHtml::value($data->school, \'name\')',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-6'
					),
				),
				array(
					'name' => 'start_time',
					'filter' => '<div class="input-group"><span class="input-group-addon">></span>' .
						$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'model' => $years,
							'attribute' => 'start_time',
							'language' => 'ru',
							'htmlOptions' => array(
								'size' => '10',
								'class' => "datepicker form-control input-sm",
							),
							'defaultOptions' => array(
								'showOn' => 'focus',
								'showOtherMonths' => true,
								'selectOtherMonths' => true,
								'dateFormat' => 'dd.mm.yy',
							),
							'options' => array(
								'dateFormat' => 'dd.mm.yy',
							),
						), true) . '</div>',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-3',
					),
					'value' => 'date(\'d.m.Y\', $data->start_time)',
				),
				array(
					'name' => 'end_time',
					'filter' => '<div class="input-group">' .
						$this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'model' => $years,
							'attribute' => 'end_time',
							'language' => 'ru',
							'htmlOptions' => array(
								'size' => '10',
								'class' => "datepicker form-control input-sm",
							),
							'defaultOptions' => array(
								'showOn' => 'focus',
								'showOtherMonths' => true,
								'selectOtherMonths' => true,
								'dateFormat' => 'dd.mm.yy',
							),
							'options' => array(
								'dateFormat' => 'dd.mm.yy',
							),
						), true) . '<span class="input-group-addon"><</span></div>',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-3',
					),
					'value' => 'date(\'d.m.Y\', $data->end_time)',
				),
				array(
					'type' => 'raw',
					'value' => 'CHtml::link("<span class=\'glyphicon glyphicon-cog\'></span>", null,	array(
						"data-toggle" => "modal",
						"data-target" => "#academicYearParams",
						"data-source" => "/admin/school/year?id=$data->id",
						"title" => "' . Yii::t('ShkSchool', 'Edit Academic Year') . '"
					))',
				),
			),
		));
	} ?>
</div>

