<?php
/**
 * @author andcherv
 * @var ShkServiceDiscount $discountModel
 * @todo review afterAjaxUpdate
 */

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'discountsGridView',
	'dataProvider' => $discountModel->search(),
	'filter' => $discountModel,
	'itemsCssClass' => 'table table-condensed table-hover text-center',
	'rowCssClassExpression' => function ($row, $data) {
			if ($data->status == ShkServiceDiscount::STATUS_DISABLED) {
				$rowClass = 'warning';
			}
			return isset($rowClass) ? $rowClass : '';
		},
	'pager' => array(
		'header' => false,
		'cssFile' => false,
		'selectedPageCssClass' => 'active',
		'htmlOptions' => array(
			'class' => 'pagination'
		)
	),
	'afterAjaxUpdate' => 'function(){shk.admin.installPopover();}',
	'columns' => array(
		array(
			'type' => 'raw',
			'value' => 'CHtml::tag("span", array(
				"class" => "glyphicon glyphicon-info-sign",
				"data-content" => "$data->description",
				"data-toggle" => "popover",
			))',
		),
		array(
			'name' => 'period_id',
			'headerHtmlOptions' => array(
				'class' => 'col-xs-2',
			),
			'filter' => false,
			'value' => '$data->period->getRange("medium")'
		),
		array(
			'name' => 'title',
			'headerHtmlOptions' => array(
				'class' => 'col-xs-4',
			),
		),
		array(
			'name' => 'amount_type',
			'headerHtmlOptions' => array(
				'class' => 'col-xs-1',
			),
			'header' => Yii::t('ShkService', 'Amount'),
			'filter' => array(
				ShkServiceDiscount::AMOUNT_TYPE_VALUE => Yii::t('ShkService', '$'),
				ShkServiceDiscount::AMOUNT_TYPE_PERCENT => '%',
				ShkServiceDiscount::AMOUNT_TYPE_DAILY => '-',
			),
			'value' => '$data->amount_default . \' \' . $data->amountTypeLabel'
		),
		array(
			'name' => 'apply_type',
			'headerHtmlOptions' => array(
				'class' => 'col-xs-2',
			),
			'filter' => array(
				ShkServiceDiscount::APPLY_AUTO => Yii::t('ShkService', 'Automatically'),
				ShkServiceDiscount::APPLY_USER => Yii::t('ShkService', 'Manually'),
			),
			'value' => '$data->applyTypeLabel'
		),
		array(
			'name' => 'start_time',
			'headerHtmlOptions' => array(
				'class' => 'col-xs-2',
			),
			'filter' => false,
			'header' => Yii::t('ShkService', 'Applicable Range'),
			'value' => '$data->getRange("medium")',
		),
		array(
			'name' => 'status',
			'headerHtmlOptions' => array(
				'class' => 'col-xs-1',
			),
			'filter' => array(
				ShkServiceDiscount::STATUS_ACTIVE => Yii::t('ShkService', 'Active'),
				ShkServiceDiscount::STATUS_DISABLED => Yii::t('ShkService', 'Disabled'),
			),
			'value' => '$data->statusLabel',
		),
		array(
			'type' => 'raw',
			'value' => 'CHtml::link("<span class=\'glyphicon glyphicon-cog\'></span>", null, array(
				"data-toggle" => "modal",
				"data-target" => "#discountEditModal",
				"data-source" => "/admin/service/discountEditModal?id=$data->id",
				"title" => "' . Yii::t('ShkService', 'Edit a discount') . '"
			))',
		),
	)
));