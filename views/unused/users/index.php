<?php

/**
 * @var UserAuth $usersModel
 */

$this->widget('qModal', array(
	'htmlOptions' => array(
		'id' => 'userStatsModal',
		'width' => '768px',
		'data-backdrop' => true,
	),
));

$this->widget('qModal', array(
	'htmlOptions' => array(
		'id' => 'userLocationModal',
		'width' => '768px',
		'data-backdrop' => true,
	),
));

$this->widget('qModal', array(
	'htmlOptions' => array(
		'id' => 'userEditModal',
	),
));

$this->renderPartial('/layouts/_defaultPageHeader', array(
	'header' => Yii::t('qAdmin', 'Users'),
)); ?>

<div id="page-content" class="row">
	<div class="col-xs-12">
		<?php $this->renderPartial('_usersPanel', array(
			'usersModel' => $usersModel,
		)); ?>
	</div>
</div>

<script>
	$(document).ready(function () {
		$("#users-page-size").on("change", function () {
			$.fn.yiiGridView.update("users", {
				data: {usersGridPageSize: $(this).val()}
			});
		});
	});
</script>
