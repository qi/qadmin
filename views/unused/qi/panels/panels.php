<?php $this->renderPartial('/layouts/_defaultPageHeader', array(
	'header' => Yii::t('qAdmin', 'qPanel'),
	'small' => Yii::t('qAdmin', 'qPanel widget showcase'),
)); ?>

<div id="page-content" class="row">
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-12"><?php $this->renderPartial('panels/_default'); ?></div>
			<div class="col-md-12"><?php $this->renderPartial('panels/_header'); ?></div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-12"><?php $this->renderPartial('panels/_collapsible'); ?></div>
			<div class="col-md-12"><?php $this->renderPartial('panels/_collapsed'); ?></div>
		</div>
	</div>
</div>