<?php

/**
 * This is the model class for table "qi_cli_command".
 *
 * The followings are the available columns in table 'qi_cli_command':
 * @property integer $id
 * @property string $script
 * @property string $command
 * @property string $params
 *
 * The followings are the available model relations:
 * @property QiCliLaunch[] $cliLaunches
 */
class QiCliCommand extends CActiveRecord
{
	private $_paths = array();

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'qi_cli_command';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('command', 'required'),
			array('script, command, params', 'length', 'max' => 255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, script, command, params', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'launches' => array(self::HAS_MANY, 'QiCliLaunch', 'command_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'script' => 'Script',
			'command' => 'Command',
			'params' => 'Params',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('script', $this->script, true);
		$criteria->compare('command', $this->command, true);
		$criteria->compare('params', $this->params, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QiCliCommand the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function init()
	{
		$this->addPaths(array(
			Yii::getFrameworkPath() . DIRECTORY_SEPARATOR . 'cli' . DIRECTORY_SEPARATOR . 'commands',
			Yii::app()->basePath . '/commands',
		));
	}

	public function addPaths(array $paths)
	{
		return $this->_paths = array_merge($this->_paths, $paths);
	}

	public function run()
	{
		$launch = new QiCliLaunch();
		$launch->setAttributes(array(
			'command_id' => $this->id,
			'status' => QiCliLaunch::STATUS_PROCESSING,
			'start_time' => time(),
		));
		$launch->save(false);

		$runner = new CConsoleCommandRunner();
		foreach ($this->_paths as $path)
			$runner->addCommands($path);

		ob_start();
		$exitCode = $runner->run(array_merge(array($this->script, $this->command), explode(' ', $this->params)));
		$output = ob_get_clean();

		$launch->saveAttributes(array(
			'status' => $exitCode == 0 ? QiCliLaunch::STATUS_SUCCESS : QiCliLaunch::STATUS_FAILURE,
			'exit_code' => $exitCode,
			'output' => $output,
			'end_time' => time(),
		));

		return $exitCode;
	}
}
