<?php

class QiActiveForm extends CActiveForm
{
	/**
	 * @param CActiveRecord $model
	 * @param string $attribute
	 */
	public function qiBeginBlock($model, $attribute)
	{
		echo CHtml::tag('h5', [], $this->labelEx($model, $attribute));
		echo CHtml::tag('hr');
	}

	/**
	 * @param CActiveRecord $model
	 * @param string $attribute
	 * @param string $help
	 */
	public function qiEndBlock($model, $attribute, $help = '&nbsp;')
	{
		echo $this->error($model, $attribute);
		echo CHtml::label($help, get_class($model) . '_' . $attribute, [
			'class' => 'help-block'
		]);
	}

	/**
	 * @param CActiveRecord $model
	 * @param string $attribute
	 * @param array $htmlOptions
	 */
	public function qiTextFieldBlock($model, $attribute, $htmlOptions = array())
	{
		$this->qiBeginBlock($model, $attribute);
		echo $this->textField($model, $attribute, CMap::mergeArray([
			'class' => 'form-control',
			'placeholder' => $model->$attribute
		], $htmlOptions));
		$this->qiEndBlock($model, $attribute);
	}

	/**
	 * @param CActiveRecord $model
	 * @param string $attribute
	 * @param array $htmlOptions
	 */
	public function qiPasswordFieldBlock($model, $attribute, $htmlOptions = array())
	{
		$this->qiBeginBlock($model, $attribute);
		echo $this->passwordField($model, $attribute, CMap::mergeArray([
			'class' => 'form-control',
			'placeholder' => $model->$attribute
		], $htmlOptions));
		$this->qiEndBlock($model, $attribute);
	}

	/**
	 * @param CActiveRecord $model
	 * @param string $attribute
	 */
	public function qiCheckBoxBlock($model, $attribute)
	{
		$this->qiBeginBlock($model, $attribute);
		echo CHtml::openTag('div', ['class' => 'checkbox']);
		echo $this->checkBox($model, $attribute, ['style' => 'margin-left: 0px;']);
		echo '&nbsp;';
		echo CHtml::label($model->getAttributeLabel($attribute), get_class($model) . '_' . $attribute);
		echo CHtml::closeTag('div');
		$this->qiEndBlock($model, $attribute);
	}

	/**
	 * @param CActiveRecord $model
	 * @param string $attribute
	 * @param array $data
	 */
	public function qiCheckBoxListBlock($model, $attribute, $data)
	{
		$this->qiBeginBlock($model, $attribute);
		echo CHtml::openTag('div', ['class' => 'checkbox']);
		echo $this->checkBoxList($model, $attribute, $data, ['style' => 'margin-left: 0px;']);
		echo '&nbsp;';
//		echo CHtml::label($model->getAttributeLabel($attribute), get_class($model) . '_' . $attribute);
		echo CHtml::closeTag('div');
		$this->qiEndBlock($model, $attribute);
	}

	/**
	 * @param CActiveRecord $model
	 * @param string $attribute
	 * @param array $data
	 */
	public function qiDropDownBlock($model, $attribute, $data)
	{
		$this->qiBeginBlock($model, $attribute);
		echo $this->dropDownList($model, $attribute, $data, [
			'class' => 'form-control',
			'placeholder' => $model->$attribute,
		]);
		$this->qiEndBlock($model, $attribute);
	}

	/**
	 * @param CActiveRecord $model
	 * @param string $attribute
	 */
	public function qiTextAreaBlock($model, $attribute)
	{
		$this->qiBeginBlock($model, $attribute);
		echo $this->textArea($model, $attribute, [
			'class' => 'form-control',
			'rows' => 3
		]);
		$this->qiEndBlock($model, $attribute);
	}

	/**
	 * @param CActiveRecord $model
	 * @param string $attribute
	 */
	public function qiRedactorBlock($model, $attribute)
	{
		$this->qiBeginBlock($model, $attribute);
		$this->widget('application.vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', [
			'model' => $model, 'attribute' => 'note',
			'options' => [
				'lang' => 'en', 'minHeight' => '100',
				'placeholder' => CHtml::value($model, $attribute, ''),
			],
		]);
		$this->qiEndBlock($model, $attribute);
	}

	/**
	 * @param CActiveRecord $model
	 * @param string $attribute
	 * @param array $data
	 * @param array $options
	 */
	public function qiSelectizeBlock($model, $attribute, array $data, array $options = [])
	{
		$this->qiBeginBlock($model, $attribute);
		$this->widget('qi.qadmin.extensions.yii-selectize.YiiSelectize', [
			'model' => $model, 'attribute' => $attribute, 'data' => $data,
			'options' => CMap::mergeArray(['create' => false], $options),
			'useWithBootstrap' => true
		]);
		$this->qiEndBlock($model, $attribute);
	}
}
