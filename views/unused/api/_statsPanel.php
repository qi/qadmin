<style>
	.total {
		display: inline-block;
		overflow: hidden;
	}
</style>


<?php $this->beginWidget('qPanel', array(
	'id' => 'stats-panel',
)); ?>
<div class="panel-body">
	<div class="row">
		<div id="ios-stats" class="col-md-6 col-lg-6">
			<h5>iOS Requests</h5>
			<hr/>
			<h3>
				<i class="fa fa-apple"></i>
				<small>
					<span class="total">&nbsp;</span>
				</small>
			</h3>
			<?php $this->widget('admin.widgets.EasyPieChart', array(
				'id' => 'ios-success',
				'options' => array(
					'size' => 50,
					'barColor' => 'rgba(43, 155, 165, 1.0)',
					'scaleColor' => false,
					'lineCap' => 'square',
					'lineWidth' => 5,
				)
			));?>
			<?php $this->widget('admin.widgets.EasyPieChart', array(
				'id' => 'ios-fail',
				'options' => array(
					'size' => 50,
					'barColor' => 'rgba(255, 193, 64, 1.0)',
					'scaleColor' => false,
					'lineCap' => 'square',
					'lineWidth' => 5,
				)
			));?>
			<?php $this->widget('admin.widgets.EasyPieChart', array(
				'id' => 'ios-error',
				'options' => array(
					'size' => 50,
					'barColor' => 'rgba(255, 73, 64, 1.0)',
					'scaleColor' => false,
					'lineCap' => 'square',
					'lineWidth' => 5,
				)
			));?>
		</div>
		<div id="android-stats" class="col-md-6 col-lg-6">
			<h5>Android Requests</h5>
			<hr/>
			<h3>
				<i class="fa fa-android"></i>
				<small>
					<span class="total">&nbsp;</span>
				</small>
			</h3>
			<?php $this->widget('admin.widgets.EasyPieChart', array(
				'id' => 'android-success',
				'options' => array(
					'size' => 50,
					'barColor' => 'rgba(43, 155, 165, 1.0)',
					'scaleColor' => false,
					'lineCap' => 'square',
					'lineWidth' => 5,
				)
			));?>
			<?php $this->widget('admin.widgets.EasyPieChart', array(
				'id' => 'android-fail',
				'options' => array(
					'size' => 50,
					'barColor' => 'rgba(255, 193, 64, 1.0)',
					'scaleColor' => false,
					'lineCap' => 'square',
					'lineWidth' => 5,
				)
			));?>
			<?php $this->widget('admin.widgets.EasyPieChart', array(
				'id' => 'android-error',
				'options' => array(
					'size' => 50,
					'barColor' => 'rgba(255, 73, 64, 1.0)',
					'scaleColor' => false,
					'lineCap' => 'square',
					'lineWidth' => 5,
				)
			));?>
		</div>
		<div class="col-md-12 help-block text-right">Since 00:00:00 UTC</div>
	</div>
</div>
<?php $this->endWidget(); ?>

<script>
	$(document).ready(function () {
		var apiStatsPanel = $("#stats-panel");
		updateApiStats(apiStatsPanel);
		setInterval(function () {
			updateApiStats(apiStatsPanel);
		}, 1000 * 10);
	});
	function updateApiStats(panel) {
		var ios = panel.find("#ios-stats");
		var android = panel.find("#android-stats");
		$.get('/admin/api/ajaxUpdateStats', function (response) {
			if (ios.find(".total").html() != response["data"]["ios"]["requestsTotal"]) {
				ios.find(".total").slideToggle("fast", function () {
					$(this).html(response["data"]["ios"]["requestsTotal"]);
					$(this).slideToggle("fast");
				});
				ios.find("#ios-success .canvas").data('easyPieChart').update(response["data"]["ios"]["pctSuccess"]);
				ios.find("#ios-fail .canvas").data('easyPieChart').update(response["data"]["ios"]["pctFail"]);
				ios.find("#ios-error .canvas").data('easyPieChart').update(response["data"]["ios"]["pctError"]);
			}
			if (android.find(".total").html() != response["data"]["android"]["requestsTotal"]) {
				android.find(".total").slideToggle("fast", function () {
					$(this).html(response["data"]["android"]["requestsTotal"]);
					$(this).slideToggle("fast");
				});
				android.find("#android-success .canvas").data('easyPieChart').update(response["data"]["android"]["pctSuccess"]);
				android.find("#android-fail .canvas").data('easyPieChart').update(response["data"]["android"]["pctFail"]);
				android.find("#android-error .canvas").data('easyPieChart').update(response["data"]["android"]["pctError"]);
			}
		}, 'json');
	}
</script>