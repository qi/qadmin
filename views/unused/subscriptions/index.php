<?php
/**
 * @var UserCommunityRel $subscriptionsModel
 */

$this->widget('qModal', array(
	'htmlOptions' => array(
		'id' => 'userStatsModal',
		'width' => '768px',
		'data-backdrop' => true,
	),
));

$this->widget('qModal', array(
	'htmlOptions' => array(
		'id' => 'userEditModal',
	),
));

$this->renderPartial('/layouts/_defaultPageHeader', array(
	'header' => Yii::t('Community', 'Subscriptions'),
)); ?>

<div id="page-content" class="row">
	<div class="col-xs-12">
		<?php $this->renderPartial('_subscriptionsPanel', array(
			'subscriptionsModel' => $subscriptionsModel,
		)); ?>
	</div>
</div>

<script>
	$(document).ready(function () {
		$("#subscriptions-page-size").on("change", function () {
			$.fn.yiiGridView.update("subscriptions", {
				data: {subscriptionsGridPageSize: $(this).val()}
			});
		});
	});
</script>
