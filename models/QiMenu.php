<?php

/**
 * This is the model class for table "qi_menu".
 *
 * The followings are the available columns in table 'qi_menu':
 * @property integer $id
 * @property string $title
 *
 * The followings are the available model relations:
 * @property QiMenuAssignment[] $assignments
 * @property QiMenuItemChild[] $items
 */
class QiMenu extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'qi_menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('title', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'assignments' => array(self::HAS_MANY, 'QiMenuAssignment', 'menu_id'),
			'roleAssignments' => array(self::HAS_MANY, 'QiMenuAssignment', 'menu_id',
				'on' => 'roleAssignments.type=' . QiMenuAssignment::TYPE_ROLE),
			'userAssignments' => array(self::HAS_MANY, 'QiMenuAssignment', 'menu_id',
				'on' => 'userAssignments.type=' . QiMenuAssignment::TYPE_USER),
			'rootItems' => array(self::HAS_MANY, 'QiMenuItemChild', 'menu_id', 'condition' => 'rootItems.parent_name=\'root\''),
			'items' => array(self::HAS_MANY, 'QiMenuItem', array('child_name' => 'name'), 'through' => 'rootItems'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QiMenu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Returns user menu if 'user' or 'role' assigned menu object exists.
	 * @return null|QiMenu
	 */
	public function load()
	{
		return $this->find(array(
			'with' => array(
				'items.children' => [
					'joinType' => 'INNER JOIN',
					'on' => 't.id=childItems.menu_id'
				],
				'assignments' => array(
					'joinType' => 'inner join',
					'on' => '(assignments.type=:roleType and assignments.assignment=:role) or (assignments.type=:userType and assignments.assignment=:id)',
					'params' => array(
						':roleType' => QiMenuAssignment::TYPE_ROLE,
						':userType' => QiMenuAssignment::TYPE_USER,
						':role' => isset(Yii::app()->user->role) ? Yii::app()->user->role : 'user',
						':id' => Yii::app()->user->id
					),
					'order' => 'assignments.type desc',
				),
			),
		));
	}
}
