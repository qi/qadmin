<?php

/**
 * Shk app component for work with time and dates.
 * Add 'shkDate'=>array('class'=>'ShkDateTime'), to configuration components array.
 * Usage Yii::app()->shkDate->methodName()
 * @author andcherv
 */
class ShkDateTime extends CApplicationComponent
{
	public $timestampPattern = '!^\d+$!';

	/**
	 * Converts array of days from plan_map to suitable for convertWeekDaysToMonthDays.
	 * @author andcherv
	 * @param array $days
	 * @return array
	 */
	public function convertStudyPlanDaysToWeeks(array $days)
	{
		$weekDays = array();
		foreach ($days as $day) {
			$key = (int)(ceil($day / 7) - 1);
			if (!array_key_exists($key, $weekDays)) {
				array_push($weekDays, array());
			}
			$weekDay = (int)($day % 7);
			if ($weekDay == 0) {
				$weekDay = 7;
			}
			array_push($weekDays[$key], $weekDay);
		}
		return $weekDays;
	}

	/**
	 * Converts week days from study plan to array of month days.
	 * @author andcherv
	 * @param int|string $year
	 * @param int|string $month
	 * @param array $weeks Weeks with study days numbers from 1 to 7
	 * @param int $fw Study week number for first week of the month
	 * @return array
	 * @throws CException
	 */
	public function convertWeekDaysToMonthDays($year, $month, array $weeks, $fw)
	{
		if ($fw > count($weeks) || $fw < 1) {
			throw new CException("Wrong first study week value = $fw for weeks count = " . count($weeks) . ".");
		} else {
			// defining first and last day of month
			$startDate = strtotime($year . '-' . $month . '-1');
			$endDate = strtotime('+' . (date('t', $startDate) - 1) . ' days', $startDate);

			// initialising variables
			$currentDate = $startDate;
			$monthDays = array();
			$weekId = 0;
			$currentStudyWeekNumber = null;

			// itarating over month days
			while ($currentDate <= $endDate) {

				// changing study week number on monday if more then one
				if ($currentStudyWeekNumber == null) {
					$currentStudyWeekNumber = $fw;
				} elseif (count($weeks) > 1 && date('N', $currentDate) == 1) {
					if (count($weeks) > $currentStudyWeekNumber) {
						$currentStudyWeekNumber++;
					} else {
						$currentStudyWeekNumber = 1;
					}
				}

				// pushing new week array if $monthDays empty or monday
				if (empty($monthDays) || (date('N', $currentDate)) == 1) {
					array_push($monthDays, array());
					$weekId = max(array_keys($monthDays));
				}

				// pushing days in week array if needed
				if (in_array((date('N', $currentDate)), $weeks[$currentStudyWeekNumber - 1])) {
					array_push($monthDays[$weekId], (date('d', $currentDate)));
				}

				// changing current day to next
				$currentDate = strtotime('+1 day', $currentDate);
			}
		}

		return $monthDays;
	}

	public function getFirstWeekNumber($year, $month)
	{
		$startDate = strtotime($year . '-' . $month . '-1');
		$weekNumber = date('W', $startDate);
		return $weekNumber;
	}

	public function getMonth($time = true, $date = null)
	{
		$monthStart = null;
		$monthEnd = null;
		if ($date === null) {
			$date = date('Y-m-d H:i:s');
		}
		if (is_string($date)) {
			$monthStart = date('Y-m-1', strtotime($date));
			$monthEnd = date('Y-m-d', strtotime('+1 month ' . $monthStart));
			$monthStart = strtotime($monthStart . ' midnight');
			$monthEnd = strtotime($monthEnd . ' midnight') - 1;
			if ($time === true) {
				$monthStart = date('Y-m-d H:i:s', $monthStart);
				$monthEnd = date('Y-m-d H:i:s', $monthEnd);
			} else {
				$monthStart = date('Y-m-d', $monthStart);
				$monthEnd = date('Y-m-d', $monthEnd);
			}
		} else {
			throw new CException('Argument must be a string date or result of date("Y-m-d H:i:s").');
		}
		return array('monthStart' => $monthStart, 'monthEnd' => $monthEnd); //@todo rename to start and end
	}

	/**
	 * Returns number of days in selected month.
	 * @author andcherv
	 * @param int|string $year 2013
	 * @param int|string $month 5
	 * @return int Days count in selected month
	 */
	public function getMonthDaysCount($year, $month)
	{
		$startDate = strtotime($year . '-' . $month . '-1');
		$monthDaysCount = intval((date('t', $startDate)), 10);
		return $monthDaysCount;
	}

	public function getMonthEnd($time = true, $date = null)
	{
		$month = self::getMonth($time, $date);
		return isset($month['monthEnd']) ? $month['monthEnd'] : null;
	}

	public function getMonthStart($time = true, $date = null)
	{
		$month = self::getMonth($time, $date);
		return isset($month['monthStart']) ? $month['monthStart'] : null;
	}

	/**
	 * Returns count of days from result of convertWeekDaysToMonthDays method.
	 * @author andcherv
	 * @param array $monthDays
	 * @return int
	 */
	public function getMonthWorkDaysCount(array $monthDays)
	{
		$workDaysCount = 0;
		foreach ($monthDays as $weekDays) {
			$workDaysCount += count($weekDays);
		}
		return $workDaysCount;
	}

	public function getWeek($time = true, $date = null)
	{
		$weekStart = null;
		$weekEnd = null;
		if ($date === null) {
			$date = date('Y-m-d H:i:s');
		}
		if (is_string($date)) {
			$weekStart = date('Y-m-d', strtotime('monday this week ' . $date));
			$weekStart = strtotime($weekStart . ' midnight');
			$weekEnd = date('Y-m-d', strtotime('monday next week ' . $date));
			$weekEnd = strtotime($weekEnd . ' midnight') - 1;
			if ($time === true) {
				$weekStart = date('Y-m-d H:i:s', $weekStart);
				$weekEnd = date('Y-m-d H:i:s', $weekEnd);
			} else {
				$weekStart = date('Y-m-d', $weekStart);
				$weekEnd = date('Y-m-d', $weekEnd);
			}
		} else {
			throw new CException('Argument must be a string date or result of date("Y-m-d H:i:s").');
		}
		return array('weekStart' => $weekStart, 'weekEnd' => $weekEnd); //@todo rename to start and end
	}

	public function getWeekEnd($time = true, $date = null)
	{
		$week = self::getWeek($time, $date);
		return isset($week['weekEnd']) ? $week['weekEnd'] : null;
	}

	public function getWeekStart($time = true, $date = null)
	{
		$week = self::getWeek($time, $date);
		return isset($week['weekStart']) ? $week['weekStart'] : null;
	}

	/**
	 * Return true if date string or timestamp is in defined range.
	 * @author andcherv
	 * @param $startTime
	 * @param $endTime
	 * @param $checkTime
	 * @return bool
	 */
	public function isInRange($startTime, $endTime, $checkTime)
	{
		if (!preg_match($this->timestampPattern, $startTime))
			$startTime = strtotime($startTime);
		if (!preg_match($this->timestampPattern, $endTime))
			$startTime = strtotime($endTime);
		if (!preg_match($this->timestampPattern, $checkTime))
			$startTime = strtotime($checkTime);
		if ($startTime <= $checkTime && $endTime >= $checkTime)
			return true;
		return false;
	}

	/**
	 * Returns true if date string or timestamp is today.
	 * @author andcherv
	 * @param int|string $time
	 * @return bool
	 */
	public function isToday($time)
	{
		$today = strtotime('today midnight');
		$tomorrow = strtotime('tomorrow midnight');
		if (!preg_match($this->timestampPattern, $time))
			$time = strtotime($time);
		$time = (int)$time;;
		if ($time >= $today && $time < $tomorrow)
			return true;
		return false;
	}

	/**
	 * Returns days count between two dates.
	 * @author andcherv
	 * @param int|string $start Start date. Formatted string or timestamp.
	 * @param int|string $end End date. Formatted string or timestamp.
	 * @param bool $fullDaysOnly
	 * @return int
	 */
	public function getDaysCount($start, $end, $fullDaysOnly = true)
	{
		if (!preg_match($this->timestampPattern, $start))
			$start = strtotime($start);
		if (!preg_match($this->timestampPattern, $end))
			$end = strtotime($end);
		if ($fullDaysOnly === false)
			$end += (60 * 60 * 24) - (($end - $start) % (60 * 60 * 24));
		return intval(($end - $start) / (60 * 60 * 24));
	}
}