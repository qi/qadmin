<?php
/**
 * @author andcherv
 * @var CActiveForm $form
 * @var qApiSettings[] $settings
 */
?>
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'settings-form',
	'htmlOptions' => array(
		'class' => 'form-horizontal',
	),
)); ?>

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title"><?php echo Yii::t('qAdmin', 'API Settings'); ?></h4>
	</div>

	<div class="modal-body">
		<?php foreach ($settings as $type => $group) { ?>
			<h4><?php echo ucfirst($type); ?></h4>
			<hr/>
			<?php foreach ($group as $setting) { ?>
				<div class="form-group">
					<label for="" class="col-xs-7 control-label">
						<?php echo $setting->label . ' (' . $setting->attribute . '):'; ?>
					</label>

					<div class="col-xs-2">
						<?php echo CHtml::textField("qApiSettings[{$setting->attribute}][value]", $setting->value, array(
							'class' => 'form-control input-sm',
							'readonly' => true,
						)); ?>
					</div>
					<div class="btn-group col-xs-3" data-toggle="buttons">
						<?php
						echo CHtml::tag('label', array(
								'class' => $setting->isActive ? 'btn btn-success btn-sm active' : 'btn btn-default btn-sm',
							), CHtml::radioButton("qApiSettings[{$setting->attribute}][state]", $setting->isActive, array(
								'value' => qApiSettings::STATE_ACTIVE,
							)) . Yii::t('qAdmin', 'On'));
						echo CHtml::tag('label', array(
								'class' => $setting->isDisabled ? 'btn btn-warning btn-sm active' : 'btn btn-default btn-sm',
							), CHtml::radioButton("qApiSettings[{$setting->attribute}][state]", $setting->isDisabled, array(
								'value' => qApiSettings::STATE_DISABLED,
							)) . Yii::t('qAdmin', 'Off'));
						?>
					</div>
				</div>
			<?php } ?>
		<?php } ?>
	</div>

	<div class="modal-footer">
		<?php $this->widget('qSubmit', array(
			'type' => 'cancel',
			'confirm' => false,
			'htmlOptions' => array(
				'class' => 'pull-left',
				'data-dismiss' => 'modal',
			),
		)); ?>
		<?php $this->widget('qSubmit', array(
			'type' => 'save',
		));    ?>
	</div>

<?php $this->endWidget(); ?>