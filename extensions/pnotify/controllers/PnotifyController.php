<?php

Yii::import('qi.qadmin.extensions.pnotify.PNotify');

class PnotifyController extends CController
{
	public $defaultAction = 'flashes';

	public function actionFlashes()
	{
		echo CJSON::encode($this->getFlashes());
		Yii::app()->end();
	}

	protected function  getFlashes()
	{
		$flashes = [];
		$user = Yii::app()->user;
		foreach ($user->flashes as $flash) {
			if (!is_array($flash))
				$flash = json_decode($flash);
			array_push($flashes, $flash);
		}
		return $flashes;
	}
}