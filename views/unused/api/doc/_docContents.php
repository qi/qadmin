<style>
	#api-doc-affix {
		max-width: 180px;
		top: 20px;
		margin-top: 40px;
	}

	#api-doc-affix, #api-doc-affix a {
		color: #333;
		font-weight: 300;
	}



	#api-doc-affix .nav > li > a {
		font-size: 0.9em;
		padding: 2px 12px;
	}

	#api-doc-affix .nav > .active > ul {
		display: block;
	}

	#api-doc-affix .nav > .active > a, #api-doc-affix .nav > .active:hover > a, #api-doc-affix .nav > .active:focus > a {
		padding-left: 11px;
		font-weight: 700;
		color: rgb(10, 146, 156);
		background-color: transparent;
		border-left: 2px solid rgb(10, 146, 156);
	}

	#api-doc-affix .nav > li > a:hover, #api-doc-affix .nav > li > a:focus {
		padding-left: 11px;
		color: rgb(10, 146, 156);
		text-decoration: none;
		background-color: transparent;
		border-left: 1px solid rgb(10, 146, 156);
	}



	#api-doc-affix .nav .nav {
		display: none;
	}

	#api-doc-affix .nav .nav > li > a {
		font-size: 0.9em;
		padding: 2px 30px;
	}

	#api-doc-affix .nav .nav > li > a:hover {
		padding-left: 29px;
	}

	#api-doc-affix .nav .nav > a, #api-doc-affix .nav .nav > a:hover{
		font-weight: 500;
	}

	#api-doc-affix .nav .nav > .active > a, #api-doc-affix .nav .nav > .active:hover > a, #api-doc-affix .nav .nav > .active:focus > a {
		font-weight: 500;
		padding-left: 28px;
	}
</style>
<div id="api-doc-affix" data-spy="affix" data-offset-top="150">
	<ul class="nav">
		<li>
			<a href="#auth"><del>Authentication</del></a>
		</li>
		<li>
			<a href="#headers"><del>Headers</del></a>
		</li>
		<li>
			<a href="#resources">Resources</a>
			<ul class="nav">
				<li><a href="#methods"><del>Methods</del></a></li>
				<li><a href="#common"><del>Common</del></a></li>
				<li><a href="#auth"><del>/auth/</del></a></li>
				<li><a href="#password"><del>/password/</del></a></li>
				<li><a href="#uploads">/uploads/</a></li>
				<li><a href="#users"><del>/users/</del></a></li>
				<li><a href="#countries"><del>/countries/</del></a></li>
				<li><a href="#cities"><del>/cities/</del></a></li>
				<li><a href="#schools"><del>/schools/</del></a></li>
				<li><a href="#communities"><del>/communities/</del></a></li>
				<li><a href="#subscriptions"><del>/subscriptions/</del></a></li>
				<li><a href="#news"><del>/news/</del></a></li>
				<li><a href="#messages">/messages/</a></li>
				<li><a href="#subjects">/subjects/</a></li>
				<li><a href="#teachers">/teachers/</a></li>
				<li><a href="#grid">/grid/</a></li>
				<li><a href="#periods">/periods/</a></li>
				<li><a href="#schedules">/schedules/</a></li>
				<li><a href="#lessons">/lessons/</a></li>
				<li><a href="#homeworks">/homeworks/</a></li>
				<li><a href="#files">/files/</a></li>
				<li><a href="#scores">/scores/</a></li>
				<li><a href="#live">/live/</a></li>
			</ul>
		</li>
		<li>
			<a href="#codes">Response codes</a>
		</li>
	</ul>
	<div id="api-doc-markup" class="well" style="margin: 30px; text-align: center; width: 100px;">
		<u>default</u><br/>
		<b>required</b><br/>
		<i>optional</i><br/>
		<i class="fa fa-exclamation-circle"></i> update<br/>
	</div>
</div>

<script>
	$(document).ready(function () {

		// set affix link active on scroll reaches the section
		var closest = null;
		var offsets = $.map($("#api-doc-panel").find(".anchor"), function (section) {
			return {
				id: $(section).prop("id"),
				offset: $(section).offset().top - 72
			};
		});
		$(document).scroll(function () {
			var windowScroll = $(this).scrollTop();
			$.each(offsets, function () {
				if (closest == null || Math.abs(this.offset - windowScroll) < Math.abs(closest.offset - windowScroll)) {
					closest = this;
				}
			});
			$.each($("#api-doc-affix").find("a"), function () {
				$(this).parent("li").removeClass("active");
				if ($(this.hash).selector == "#" + closest.id) {
					$(this).parent("li").addClass("active");
					if ($(this).parent("li").parent("ul").parent("li").length > 0) {
						$(this).parent("li").parent("ul").parent("li").addClass("active");
					}
				}
			});
		});

		// scroll to the section on affix link click
		$("#api-doc-affix").on("click", "a", function () {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - 72
					}, 500);
					return false; // prevents link default action
				}
			}
			return true;
		});

	});
</script>