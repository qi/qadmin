<?php
$this->widget('qi.qadmin.widgets.QiPageHeader', array(
	'header' => Yii::t('qAdmin', 'Maintenance'),
)); ?>

<div id="page-content" class="row">
	<div class="col-xs-3">
		<?php $this->beginWidget('qi.qadmin.widgets.QiPanel', array(
			'id' => 'assets',
			'title' => 'Assets',
			'js' => 'updateAssetsCount();
				function updateAssetsCount() {
					value = $(".q-dash-panel-value");
					$.get("/admin/system/ajaxAssetsGetCount", function (response) {
						if (value.html() != response["data"]["count"]) {
							value.slideToggle("fast", function () {
								$(this).html(response["data"]["count"]);
								$(this).slideToggle("fast");
							});
						}
					}, "json");
				}',
			'buttons' => array(
				CHtml::htmlButton('<i class="fa fa-trash-o"></i>', array(
					'id' => 'clean-assets-button',
					'class' => 'btn btn-default btn-sm pull-right',
					'title' => Yii::t('qAdmin', 'Clean Assets'),
					'data-loading-text' => "...",
					'ajax' => array(
						'url' => Yii::app()->createUrl('dashboard/settings/ajaxAssetsClean'),
						'beforeSend' => 'function() {
							$("#clean-assets-button").button("loading");
						}',
						'complete' => 'function(){
							$(document).qP().notifications().update();
							$("#clean-assets-button").button("reset");
							updateAssetsCount();
						}'
					),
				)),
			),
		)); ?>
		<div class="panel-body">
			<h1>
				<i class="fa fa-folder-open"></i>
				<small>
					<span class="q-dash-panel-value">&nbsp;</span>
					<span class="q-dash-panel-title"><?php echo Yii::t('qAdmin', 'Assets folders'); ?></span>
				</small>
			</h1>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>