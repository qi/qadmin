<?php
/**
 * @var Community $communityModel
 */
?>
<div id="communities-panel" class="panel panel-default">
	<div class="panel-heading text-right">
		<h6 class="pull-left"><?php echo Yii::t('Community', 'Communitites'); ?></h6>
		<?php echo CHtml::dropDownList('communities-page-size', Yii::app()->user->getState('communitiesGridPageSize'), array(
			10 => 10, 30 => 30, 50 => 50
		), array(
			'class' => 'form-control input-sm',
			'style' => 'width: 70px; float: right;',
		)); ?>
	</div>
	<div class="panel-body table-responsive">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'communities',
			'dataProvider' => $communityModel->search(),
			'filter' => $communityModel,
			'itemsCssClass' => 'table table-condensed table-hover text-center',
			'pager' => array(
				'header' => false,
//				'cssFile' => $this->assetsPath . '/css/shk.admin.pager.css'
			),
			'rowCssClassExpression' => function ($row, $data) {
					switch ($data->metadata->state) {
						case "disabled":
							$rowClass = "warning";
							break;
						case "deleted":
							$rowClass = "danger";
							break;
						default:
							$rowClass = "default";
					}
					return $rowClass;
				},
			'columns' => array(
				array(
					'name' => 'id',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-1',
					),
				),
				array(
					'name' => 'name',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-3',
					),
				),
				array(
					'name' => 'type',
					'filter' => array(
						'personal' => 'personal',
						'group' => 'group',
					),
					'headerHtmlOptions' => array(
						'class' => 'col-xs-1',
					),
				),
				array(
					'name' => 'schoolName',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-3',
					),
				),
				array(
					'name' => 'public_id',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-3',
					),
				),
				array(
					'name' => 'shk_id',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-1',
					),
				),
				array(
					'type' => 'raw',
					'value' => 'CHtml::link("<i class=\'fa fa-lg fa-bar-chart-o\'></i>", null, array(
						"data-toggle" => "modal",
						"data-target" => "#userStatsModal",
						"data-source" => "/admin/users/ajaxUserStatsModal?id=$data->id",
						"style" => "cursor:pointer",
						"title" => "' . Yii::t('qAdmin', 'Statistics') . '"
					))',
					'headerHtmlOptions' => array(
						'style' => 'width: 25px; min-width: 25px; max-width: 25px;',
					),
				),
				array(
					'type' => 'raw',
					'value' => 'CHtml::link("<i class=\'fa fa-lg fa-edit\'></i>", null, array(
						"data-toggle" => "modal",
						"data-target" => "#userEditModal",
						"data-source" => "/admin/users/ajaxUserEditModal?id=$data->id",
						"style" => "cursor:pointer",
						"title" => "' . Yii::t('qAdmin', 'Edit') . '"
					))',
					'headerHtmlOptions' => array(
						'style' => 'width: 25px; min-width: 25px; max-width: 25px;',
					),
				),
			),
		)); ?>
	</div>
</div>