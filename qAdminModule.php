<?php

/**
 * Class qAdminModule
 *
 * @property string $logoUrl
 * @property string $faviconUrl
 *
 * @todo Create Morris.js extension
 */
class qAdminModule extends CWebModule
{
	public $defaultRoute;
	public $themeOptions = array(
		'assetsUrl' => null,
		'defaultLogoUrl' => '/img/icons8.com/automatic_light/PNG/128/automatic-128.png',
		'defaultFaviconUrl' => '/img/icons8.com/automatic_dark/PNG/32/automatic-32.png',
		'customLogoUrl' => null,
		'customFaviconUrl' => null,
	);
	public $userIdentityClassName = 'QiUserIdentity';
	/**
	 * Admin panel settings for current session.
	 * @var array
	 */
	public $forceLocalAssets = false;
	public $css;

	private $_selectorsList;

	public function init()
	{
		$this->setImport(array(
			"{$this->id}.models.*",
			"{$this->id}.components.*",
			"{$this->id}.controllers.*",
			"{$this->id}.widgets.*",
		));
		Yii::app()->setComponents(array(
			'errorHandler' => array(
				'errorAction' => "/{$this->id}/default/error",
			),
			'panel' => array(
				'class' => 'ShkAdminPanel',
			),
			'qFiles' => array(
				'class' => 'qFiles',
			),
			'highlightCode' => array(
				'class' => "{$this->id}.extensions.yii-highlight.HighlightJs",
				'theme' => 'github',
			),
		));

		$this->controllerMap = CMap::mergeArray(array(
			'default' => "{$this->id}.controllers.DefaultController",
			'settings' => "{$this->id}.controllers.SettingsController",
		), $this->controllerMap);

		$this->layoutPath = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'layouts';

		if (!isset($this->defaultRoute))
			$this->defaultRoute = $this->id;

		Yii::app()->clientScript->registerCoreScript('jquery');
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			if (Yii::app()->user->isGuest && !in_array($action->id, array('error', 'login'))) {
				Yii::app()->controller->redirect(Yii::app()->createUrl($this->id . '/login'));
			}
			$this->registerAssets();
			return true;
		} else return false;
	}

	/**
	 * Returns logo URL.
	 * @return string
	 */
	public function getLogoUrl()
	{
		if ($this->themeOptions['customLogoUrl'] === null)
			return $this->themeOptions['assetsUrl'] . $this->themeOptions['defaultLogoUrl'];
		return $this->themeOptions['customLogoUrl'];
	}

	/**
	 * Returns favicon URL.
	 * @return string
	 */
	public function getFaviconUrl()
	{
		if ($this->themeOptions['customFaviconUrl'] === null)
			return $this->themeOptions['assetsUrl'] . $this->themeOptions['defaultFaviconUrl'];
		return $this->themeOptions['customFaviconUrl'];
	}

	/**
	 * Returns user image URL.
	 * @return string
	 */
	public function getUserImageUrl()
	{
		return $this->themeOptions['assetsUrl'] . '/img/icons8.com/user_male/PNG/100/user_male-100.png';
	}

	/**
	 * Returns global selectors list.
	 * @todo change to array?
	 * @return CList
	 */
	public function getSelectorsList()
	{
		if (!isset($this->_selectorsList))
			$this->_selectorsList = new CList();
		return $this->_selectorsList;
	}

	public function registerAssets()
	{
//		$this->registerBootstrap();
		$this->registerOpenSans();
		$this->registerFontAwesome();
		$this->registerAnimateCss();
		$this->registerToggleSwitch();
		$this->registerFlot();
		$this->registerQAdmin();
	}

	protected function registerQAdmin()
	{
		$path = Yii::getPathOfAlias($this->id . '.assets');
		$url = Yii::app()->getAssetManager()->publish($path, false, -1, true);
		Yii::app()->clientScript->registerCssFile($url . '/css/qAdmin.css');
//		Yii::app()->clientScript->registerCssFile($url . '/css/ipupils-bootstrap-theme.css');
		Yii::app()->clientScript->registerCssFile($url . '/css/qAdmin-pager.css');
		Yii::app()->clientScript->registerCssFile($url . '/css/shk.admin.css');
		if (isset($this->css))
			Yii::app()->clientScript->registerCssFile(Yii::app()->createAbsoluteUrl($this->css));
		Yii::app()->clientScript->registerScriptFile($url . '/js/qAdmin.js');
		Yii::app()->clientScript->registerScriptFile($url . '/js/qPanelWidget.js');
		$this->themeOptions['assetsUrl'] = $url;
	}

	protected function registerBootstrap()
	{
		if ($this->forceLocalAssets === true) {
			$path = Yii::getPathOfAlias($this->id . '.vendors.bootstrap');
			$url = Yii::app()->getAssetManager()->publish($path, false, -1, true);
			Yii::app()->clientScript->registerCssFile($url . '/css/bootstrap.min.css');
			Yii::app()->clientScript->registerScriptFile($url . '/js/bootstrap.min.js');
		} else {
			Yii::app()->clientScript->registerCssFile('//maxcdn.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css');
			Yii::app()->clientScript->registerScriptFile('//maxcdn.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js');
		}
	}

	protected function registerOpenSans()
	{
		if ($this->forceLocalAssets === true) {
			$path = Yii::getPathOfAlias($this->id . '.vendors.open-sans');
			$url = Yii::app()->getAssetManager()->publish($path, false, -1, true);
			Yii::app()->clientScript->registerCssFile($url . '/OpenSans.css');
		} else {
			Yii::app()->clientScript->registerCssFile('//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic');
		}
	}

	protected function registerFontAwesome()
	{
		Yii::app()->clientScript->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css');
	}

	protected function registerToggleSwitch()
	{
		$path = Yii::getPathOfAlias($this->id . '.vendors.toggle-switch');
		$url = Yii::app()->getAssetManager()->publish($path, false, -1, true);
		Yii::app()->clientScript->registerCssFile($url . '/toggle-switch.css');
	}

	protected function registerFlot()
	{
		$path = Yii::getPathOfAlias($this->id . '.vendors.flot');
		$url = Yii::app()->getAssetManager()->publish($path, false, -1, true);
		Yii::app()->clientScript->registerScriptFile($url . '/jquery.flot.min.js');
	}

	protected function registerAnimateCss()
	{
		$path = Yii::getPathOfAlias($this->id . '.vendors.animate-css');
		$url = Yii::app()->getAssetManager()->publish($path, false, -1, true);
		Yii::app()->clientScript->registerCssFile($url . '/animate.min.css');
	}
}
