<?php

/**
 * Class QiController
 * @todo switch module default layout to minimal
 */
class QiaController extends DefaultController
{
	public $layout = 'default';
	public $defaultAction = 'theme';

	public function init()
	{
		Yii::app()->highlightCode->addHighlighter();
	}

	public function actionTheme()
	{
		$this->pageTitle = Yii::t('qAdmin', 'Theme');
		$this->addBreadcrumbs(array(
			Yii::t('qAdmin', 'qAdmin') => Yii::app()->createUrl('admin/qi'),
			Yii::t('qApi', 'Theme'),
		));
		$this->render('theme');
	}

	public function actionPanels()
	{
		$this->pageTitle = Yii::t('qAdmin', 'Panels');
		$this->addBreadcrumbs(array(
			Yii::t('qAdmin', 'qAdmin') => Yii::app()->createUrl('admin/qi'),
			Yii::t('qApi', 'Panels'),
		));
		$this->render('panels/panels');
	}

	public function actionWidgets()
	{
		$this->pageTitle = Yii::t('qAdmin', 'Widgets');
		$this->addBreadcrumbs(array(
			Yii::t('qAdmin', 'qAdmin') => Yii::app()->createUrl('admin/qi'),
			Yii::t('qApi', 'Widgets'),
		));
		$this->render('widgets');
	}

	public function actionCharts()
	{
		$this->pageTitle = Yii::t('qAdmin', 'Charts');
		$this->addBreadcrumbs(array(
			Yii::t('qAdmin', 'qAdmin') => Yii::app()->createUrl('admin/qi'),
			Yii::t('qApi', 'Charts'),
		));
		$this->render('charts');
	}

	public function actionAjaxGetFlotData($length)
	{
		if (Yii::app()->request->isAjaxRequest) {
			$data = array();
			for ($i =0; $i < $length; $i++ ) {
				array_push($data, rand(0, 10));
			}
			echo CJSON::encode(array(
				'status' => 'success',
				'data' => $data,
			));
			Yii::app()->end();
		}
	}
}