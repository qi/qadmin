<?php
/**
 * School benefits management page.
 * @author andcherv
 * @var $benefitModel
 */
?>

<!-- Page Header -->
<?php $this->renderPartial('//layouts/_adminPageHeader', array(
	'header' => Yii::t('ShkSchool', 'Benefits'),
	'small' => Yii::app()->shkPanel->filterString,
)); ?>

<!-- Buttons Bar -->
<div id="btnBar" class="row">
	<?php
	echo CHtml::link('<span class="glyphicon glyphicon-plus"></span>' . Yii::t('qPanel', 'Add'), null, array(
		'class' => 'btn btn-sm btn-default',
		'disabled' => !isset(Yii::app()->shkPanel->school),
		'data-toggle' => 'modal',
		'data-target' => '#benefitEditModal',
		'data-source' => '/admin/school/benefitEditModal',
		'title' => Yii::t('ShkSchool', 'Add a benefit'),
	));
	?>
</div>

<!-- Benefit Edit Modal -->
<?php $this->widget('qPanel.widgets.qModal', array(
	'htmlOptions' => array(
		'id' => 'benefitEditModal',
		'width' => '768px',
	),
)); ?>

<!-- Benefit Recipients Modal -->
<?php $this->widget('qPanel.widgets.qModal', array(
	'htmlOptions' => array(
		'id' => 'benefitRecipientsEditModal',
	),
)); ?>

<!-- Benefits Table -->
<div id="table" class="row table-responsive">
	<?php if (isset($benefitModel)) {
		$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'benefitsGrid',
			'dataProvider' => $benefitModel->search(),
			'filter' => $benefitModel,
			'itemsCssClass' => 'table table-condensed table-hover text-center',
			'rowCssClassExpression' => function ($row, $data) {
					if ($data->status == ShkSchoolBenefit::STATUS_DISABLED) {
						$rowClass = 'warning';
					}
					return isset($rowClass) ? $rowClass : '';
				},
			'pager' => array(
				'header' => false,
				'cssFile' => false,
				'selectedPageCssClass' => 'active',
				'htmlOptions' => array(
					'class' => 'pagination'
				)
			),
			'columns' => array(
				array(
					'name' => 'schoolName',
					'header' => Yii::t('ShkSchool', 'School'),
					'filter' => false,
					'headerHtmlOptions' => array('class' => 'col-xs-3'),
				),
				array(
					'name' => 'title',
					'headerHtmlOptions' => array('class' => 'col-xs-3'),
				),
				array(
					'name' => 'description',
					'headerHtmlOptions' => array('class' => 'col-xs-3'),
				),
				array(
					'name' => 'status',
					'value' => '$data->statusLabel',
					'filter' => array(
						1 => Yii::t('ShkSchool', 'Active'),
						0 => Yii::t('ShkSchool', 'Disabled'),
					),
					'headerHtmlOptions' => array(
						'class' => 'col-xs-2',
					),
				),
				array(
					'name' => 'recipientsCount',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-1',
					),
				),
				array(
					'type' => 'raw',
					'value' => 'CHtml::link("<span class=\'glyphicon glyphicon-list-alt\'></span>", null, array(
						"data-toggle" => "modal",
						"data-target" => "#benefitRecipientsEditModal",
						"data-source" => "/admin/school/benefitRecipientsEditModal?id=$data->id",
						"title" => "' . Yii::t('ShkSchool', 'Edit a benefit recipients list') . '"
					))',
				),
				array(
					'type' => 'raw',
					'value' => 'CHtml::link("<span class=\'glyphicon glyphicon-cog\'></span>", null, array(
						"data-toggle" => "modal",
						"data-target" => "#benefitEditModal",
						"data-source" => "/admin/school/benefitEditModal?id=$data->id",
						"title" => "' . Yii::t('ShkSchool', 'Edit a benefit') . '"
					))',
				),
			)
		));
	} ?>
</div>