<?php
/**
 * @author andcherv
 * @var ShkServicePeriod $period
 * @var ShkSchoolBenefit[] $benefits
 * @var string $formAction
 */
?>

<?php echo CHtml::beginForm($this->action->id, 'post'); ?>

<!-- Modal header -->
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"><?php echo Yii::t('ShkService', 'Period Prices') ?>
		<small><?php echo $period->range; ?></small>
	</h4>
</div>

<!-- Modal body -->
<div class="modal-body">
	<?php if (isset($period->services) && !empty($period->services)) { //@todo do not render benefit tab if no active ?>
		<ul class="nav nav-tabs">
			<li class="active"><a href="#default" data-toggle="tab"><?php echo Yii::t('ShkService', 'General Prices'); ?></a></li>
			<li><a href="#benefits" data-toggle="tab"><?php echo Yii::t('ShkService', 'Benefits'); ?></a></li>
		</ul>
		<div id="tabContent" class="tab-content">
			<div class="tab-pane active" id="default">
				<?php
				echo CHtml::openTag('div', array('class' => 'row'));
				echo CHtml::openTag('div', array('class' => 'col-xs-12'));
				$prices = ShkServicePrice::model()->period($period->id)->addRules(array(
					'bundle' => array(),
				))->findAll();
				$this->renderPartial('/service/_periodPricesBundleTable', array(
					'period' => $period,
					'prices' => $prices,
				));
				echo CHtml::closeTag('div');
				echo CHtml::closeTag('div');
				?>
			</div>
			<div class="tab-pane" id="benefits">
				<?php
				foreach ($benefits as $benefit) {
					echo CHtml::tag('h4', array(), $benefit->title);
					echo CHtml::tag('hr');
					echo CHtml::openTag('div', array('class' => 'row'));
					echo CHtml::openTag('div', array('class' => 'col-xs-12'));
					$prices = ShkServicePrice::model()->period($period->id)->addRules(array(
						'bundle' => array(),
						'benefit' => array($benefit->id),
					))->findAll();
					$this->renderPartial('/service/_periodPricesBundleTable', array(
						'period' => $period,
						'prices' => $prices,
						'benefit' => $benefit,
					));
					echo CHtml::closeTag('div');
					echo CHtml::closeTag('div');
				}
				?>
			</div>
		</div>
	<?php } else echo CHtml::tag('div', array('class' => 'waterMark'), Yii::t('ShkService', 'No active services.')); ?>
</div>

<div class="modal-footer">
	<?php
	$this->widget('admin.widgets.qSubmit', array(
		'type' => 'cancel',
		'confirm' => false,
		'htmlOptions' => array(
			'class' => 'pull-left',
			'data-dismiss' => 'modal',
		),
	));
	echo CHtml::button('Обнулить', array(
		'id' => 'setZeroPrices',
		'class' => 'btn btn-sm btn-warning qSubmit',
	));
	$this->widget('admin.widgets.qSubmit', array(
		'type' => 'save',
		'htmlOptions' => array(
			'id' => 'paymentPeriodPricesSave',
		),
	));
	?>

</div>

<?php echo CHtml::endForm(); ?>

<script>
	$(document).ready(function () {
		$("#setZeroPrices").on("click", function () {
			$("#tabContent").find(".active").find("input[type=text]").each(function () {
				$(this).val('0.00')
			});
		});
	});
</script>