<?php
/** Database manipulation command.
 * @author andcherv
 */
class ShkDatabaseCommand extends CConsoleCommand
{
	/** Use test database.
	 * @author andcherv
	 * @var bool
	 */
	public $test = false;

	/** Database connection.
	 * @author andcherv
	 * @var CDbConnection
	 */
	protected $connection = null;

	/** Creates DB tables and populates them with data.
	 * @author andcherv
	 * @param bool $fk Create constraints, default true
	 * @param bool $data Populate with data, default true
	 * @param null|string $module Module name. All modules if null.
	 */
	public function actionCreate($module = null, $fk = true, $data = true)
	{
		if ($this->connection = $this->setConnection($this->test)) {
			//$modulesNames = Yii::app()->shkProject->getModulesNames();
			$modulesNames = array('admin', 'ShkSchool', 'ShkService');
			if ($module !== null && in_array($module, $modulesNames)) {
				$this->createModuleDatabase($module, $this->connection, $fk, $data);
			} else {
				foreach ($modulesNames as $moduleName) {
					$this->createModuleDatabase($moduleName, $this->connection, $fk, $data);
				}
			}
		} else {
			echo "Database connection error" . PHP_EOL;
		}
	}

	/**
	 * Fixes wrong school id for users cards
	 * @author andcherv
	 */
	public function actionFixCardSchools()
	{
		if ($this->connection = $this->setConnection($this->test)) {
			$cards = Card::model()->with('member')->findAll(array(
				'condition' => 't.id_school != member.id_school',
			));
			$updatedCardsCount = 0;
			foreach ($cards as $card) {
				$card->id_school = $card->member->id_school;
				if ($card->save())
				{
					$updatedCardsCount++;
				}
			}
			echo "$updatedCardsCount cards was updated" . PHP_EOL;
		} else {
			echo "Database connection error" . PHP_EOL;
		}
	}

	/** Sets database connection according to arguments.
	 * @author andcherv
	 * @param $test
	 * @return CDbConnection
	 */
	protected function setConnection($test)
	{
		if ($test == true) {
			Yii::app()->db->active = false;
			Yii::app()->db->connectionString = 'mysql:host=localhost;dbname=skarta_test';
			Yii::app()->db->active = true;
		}
		return Yii::app()->db;
	}

	/** Executes module's sql files with given arguments.
	 * @author andcherv
	 * @param string $moduleName
	 * @param CDbConnection $connection
	 * @param bool $fk
	 * @param bool $data
	 */
	protected function createModuleDatabase($moduleName, CDbConnection $connection, $fk, $data)
	{
		$modulePath = Yii::app()->basePath . '/modules/' . $moduleName;
		$fileTables = $modulePath . '/data/tables.sql';
		$fileConstraints = $modulePath . '/data/constraints.sql';
		$fileData = $modulePath . '/data/data.sql';
		if (file_exists($fileTables)) {
			$sql = file_get_contents($fileTables);
			if ($this->executeSql($sql, $connection)) {
				echo $moduleName . ' tables created.' . PHP_EOL;
			} else {
				echo $moduleName . ' tables error.' . PHP_EOL;
			}
		} else {
			//echo $moduleName . ' tables file not found.' . PHP_EOL;
		}
		if ($fk) {
			if (file_exists($fileConstraints)) {
				$sql = file_get_contents($fileConstraints);
				if ($this->executeSql($sql, $connection)) {
					echo $moduleName . ' constraints created.' . PHP_EOL;
				} else {
					echo $moduleName . ' constraints error.' . PHP_EOL;
				}
			} else {
				//echo $moduleName . ' constraints file not found.' . PHP_EOL;
			}
		}
		if ($data) {
			if (file_exists($fileData)) {
				$sql = file_get_contents($fileData);
				if ($this->executeSql($sql, $connection)) {
					echo $moduleName . ' data created.' . PHP_EOL;
				} else {
					echo $moduleName . ' data error.' . PHP_EOL;
				}
			} else {
				//echo $moduleName . ' data file not found.' . PHP_EOL;
			}
		}
	}

	/** Executes SQL statement for CDbConnection.
	 * @author andcherv
	 * @param string $sql
	 * @param CDbConnection $connection
	 * @return bool
	 */
	protected function executeSql($sql, CDbConnection $connection)
	{
		$executed = false;
		$transaction = $connection->beginTransaction();
		try {
			$connection->createCommand($sql)->execute();
			$transaction->commit();
			$executed = true;
		} catch (Exception $e) {
			$transaction->rollback();
			$executed = false;
			CVarDumper::dump($e->getMessage());
		}
		return $executed;
	}
}