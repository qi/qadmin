<?php
/**
 * @author andcherv <andcherv@gmail.com>
 * @var Controller $this
 * @var string $content
 */
?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta charset="UTF-8"/>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->module->faviconUrl; ?>"/>
	<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"/>
	<title><?php echo $this->pageTitle; ?></title>
</head>

<body>

<header class="navbar navbar-default navbar-fixed-top">
	<div id="header-logo" class="bg-primary pull-left">
		<a href="<?php echo Yii::app()->createUrl($this->module->defaultRoute); ?>" class="center-block">
			<img class="navbar-logo pull-left" src="<?php echo $this->module->logoUrl; ?>"/>
			<span id="brandLong"
			      class="navbar-brand hidden-xs hidden-sm hidden-md"><?php echo Yii::app()->name; ?></span>
			<span id="brandShort"
			      class="navbar-brand visible-xs visible-sm visible-md"><?php echo Yii::app()->name; ?></span>
		</a>
	</div>
	<?php foreach ($this->module->selectorsList as $selector) {
		echo $selector;
	} ?>
	<ul id="headerMenu" class="nav navbar-nav navbar-right navbar-text">
		<li>
			<div id="user-block">
				<div class="row text-right">
					<div class="col-xs-12">
						<!--<a href="#" title="Messages" class="navbar-link">
							<span class="glyphicon glyphicon-envelope"></span>
						</a>
						<a href="#" title="Settings" class="navbar-link">
							<span class="glyphicon glyphicon-cog"></span>
						</a>-->
						<a href="<?php echo Yii::app()->createUrl($this->module->id . '/logout') ?>" title="Sign Out" class="navbar-link">
							<span class="glyphicon glyphicon-off"></span>
						</a>
					</div>
					<div class="col-xs-12">
						<a href="#" class="navbar-link">
							<?php echo isset(Yii::app()->user->email) ? Yii::app()->user->email : Yii::app()->user->id; ?>
						</a>
					</div>
				</div>
			</div>
		</li>
		<li>
			<a href="#" class="thumbnail">
				<img id="user-image" class="animated tada" src="<?php echo $this->module->userImageUrl; ?>" alt=""/>
			</a>
		</li>
	</ul>
</header>

<section id="wrapper">
	<section id="container">
		<div id="breadcrumbs">
			<?php if (isset($this->breadcrumbs)) {
				$this->widget('zii.widgets.CBreadcrumbs', array(
					'homeLink' => CHtml::link('<i class="fa fa-lg fa-home"></i>', Yii::app()->homeUrl),
					'links' => $this->breadcrumbs,
				));
			} ?>
			<div class="page-help">
				<?php
				if (QiPageHelp::helpExists()) {
					$this->widget('qi.qadmin.widgets.QiModal', array(
						'htmlOptions' => array(
							'id' => 'help-modal',
							'data-backdrop' => true,
						),
					));
					echo CHtml::link(CHtml::tag('i', array(
						'class' => 'fa fa-lg fa-question-circle pull-right',
					), ''), null, array(
						'id' => 'helpLink',
						'data-toggle' => 'modal',
						'data-target' => '#help-modal',
						'data-source' => Yii::app()->createUrl($this->module->id . '/settings/axViewHelp', array(
								'module' => isset($this->module->id) ? $this->module->id : null,
								'controller' => isset($this->id) ? $this->id : null,
								'action' => isset($this->action->id) ? $this->action->id : null,
							)),
					));
				} ?>
			</div>
		</div>
		<div id="content">
			<?php echo $content; ?>
		</div>
	</section>
</section>

<aside id="qap-aside">
	<div id="qap-aside-menu">
		<?php
		$menu = QiMenu::model()->load();
		if (isset($menu)) {
			foreach ($menu->items as $item) {
				echo CHtml::openTag('div', array('class' => 'panel'));
				echo CHtml::link(CHtml::tag('div', array('class' => $item->isActive ? 'panel-heading active' : 'panel-heading'),  Yii::t('qadmin', $item->title)),
					isset($item->children) && !empty($item->children) ? '#' . $item->name : Yii::app()->createUrl($item->route), array(
						'data-toggle' => isset($item->children) && !empty($item->children) ? 'collapse' : null,
						'data-parent' => isset($item->children) && !empty($item->children) ? '#qap-aside-menu' : null,
					));
				$collapse = 'collapse';
				foreach ($item->children as $child) if ($child->isActive) $collapse = 'in';
				echo CHtml::openTag('div', array('id' => $item->name, 'class' => 'panel-body ' . $collapse));
				foreach ($item->children as $child) {
					echo CHtml::link(CHtml::tag('div', array(
						'class' => $child->isActive ? 'menu-item active' : 'menu-item',
					), Yii::t('qadmin', $child->title)), Yii::app()->createUrl($child->route));
				}
				echo CHtml::closeTag(('div'));
				echo CHtml::closeTag(('div'));
			}
		}
		?>
	</div>
	<footer>
		<?php echo 'Qi ' . gmdate('Y'); ?>
	</footer>
</aside>

<?php $this->widget('qi.qadmin.extensions.pnotify.PNotify', [
	'flashes' => ['route' => Yii::app()->baseUrl . '/dashboard/pnotify'],
]); ?>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script>
	$(document).ready(function () {


		$('#user-block').addClass('animated');
		asdToggle();
		$("#user-image").on("click", function () {
			$(this).addClass("tada");
			asdToggle();
			return false;
		}).on("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
			$(this).removeClass("tada");
		});
	});

	function asdToggle() {
		var asd = $("#user-block");
		if (!asd.hasClass("bounceInRight") && !asd.hasClass("bounceOutRight")) {
			asd.addClass("bounceInRight");
		} else if (asd.hasClass("bounceOutRight")) {
			asd.removeClass("bounceOutRight");
			asd.addClass("bounceInRight");
		} else if (asd.hasClass("bounceInRight")) {
			asd.removeClass("bounceInRight");
			asd.addClass("bounceOutRight");
		}

	}
</script>

</body>

</html>