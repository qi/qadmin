$(document).ready(function () {
    var page = $('#page-content');
    page.on("show.bs.collapse", ".QiPanel", function () {
        $(this).find(".panel-toggle i").removeClass("fa-angle-down").addClass("fa-angle-up");
    });
    page.on("hide.bs.collapse", ".QiPanel", function () {
        $(this).find(".panel-toggle i").removeClass("fa-angle-up").addClass("fa-angle-down");
    });
});
