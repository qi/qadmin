<?php
/**
 * @author andcherv
 * @var ShkServicePeriod $period
 * @var ShkServicePrice[] $prices
 * @var string $formAction
 */
$activeServicesCount = count($period->activeServices);
$priceRuleId = ShkServicePriceRuleEnum::BUNDLE_OF;
?>

<?php echo CHtml::beginForm($formAction, 'post'); ?>
<?php echo CHtml::hiddenField('period_id', $period->id); ?>

<!-- Modal header -->
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"><?php echo Yii::t('ShkService', 'Period Prices') ?>
		<small><?php echo $period->range; ?></small>
	</h4>
</div>

<!-- Modal body -->
<div class="modal-body">

	<ul class="nav nav-tabs">
		<li class="active"><a href="#home" data-toggle="tab">Home</a></li>
		<li><a href="#profile" data-toggle="tab">Profile</a></li>
		<li><a href="#messages" data-toggle="tab">Messages</a></li>
		<li><a href="#settings" data-toggle="tab">Settings</a></li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="home">

			<div class="row">
				<div class="col-xs-12">
					<?php if (!isset($period->activeServices) || empty($period->activeServices)) {
						echo CHtml::tag('div', array('class' => 'waterMark'), Yii::t('admin', 'No active services.'));
					} else {
						echo CHtml::openTag('table', array(
							'id' => 'periodBundlePrices',
							'class' => 'table table-hover table-condensed',
						));
						?>
						<tr>
							<th class="col-xs-3"><?php echo Yii::t('admin', 'Price in a bundle of...'); ?></th>
							<?php for ($i = 1; $i <= count($period->activeServices); $i++) { ?>
								<th><?php echo $i == 1 ? $i . ' ' . Yii::t('admin', 'service') : $i . ' ' . Yii::t('admin', 'services'); ?></th>
							<?php } ?>
						</tr>
						<?php foreach ($period->activeServices as $activeService) {
							echo CHtml::openTag('tr');
							echo CHtml::tag('td', array(), $activeService->service->title);
							for ($i = 1; $i <= $activeServicesCount; $i++) {
								$priceId = $activeService->id . ':' . $i;
								$currentPrice = null;
								foreach ($prices as $price) {
									$rules = $price->rules;
									if ($price->service_id == $activeService->service_id && current($rules)->rule_value == $i) {
										$currentPrice = $price;
									}
								}
								echo CHtml::openTag('td');
								echo CHtml::hiddenField('ShkServicePrice[' . $priceId . '][period_id]', $period->id);
								echo CHtml::hiddenField('ShkServicePrice[' . $priceId . '][service_id]', $activeService->service_id);
								echo CHtml::textField('ShkServicePrice[' . $priceId . '][price]', isset($currentPrice) ? $currentPrice->price : '', array(
										'class' => 'form-control input-sm',
										'placeholder' => isset($currentPrice) ? $currentPrice->price : '',
									));
								echo CHtml::hiddenField('ShkServicePrice[' . $priceId . '][price_rules][0][rule_id]', $priceRuleId);
								echo CHtml::hiddenField('ShkServicePrice[' . $priceId . '][price_rules][0][rule_value]', $i);
								echo CHtml::closeTag('td');
							}
							echo CHtml::closeTag('tr');
						}
						echo CHtml::closeTag('table');
					} ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="profile">
			<div class="row">
				<div class="col-xs-12">
					<?php if (!isset($period->activeServices) || empty($period->activeServices)) {
						echo CHtml::tag('div', array('class' => 'waterMark'), Yii::t('admin', 'No active services.'));
					} else {
						echo CHtml::openTag('table', array(
							'id' => 'periodBundlePrices2',
							'class' => 'table table-hover table-condensed',
						));
						?>
						<tr>
							<th class="col-xs-3"><?php echo Yii::t('admin', 'Price in a bundle of...'); ?></th>
							<?php for ($i = 1; $i <= count($period->activeServices); $i++) { ?>
								<th><?php echo $i == 1 ? $i . ' ' . Yii::t('admin', 'service') : $i . ' ' . Yii::t('admin', 'services'); ?></th>
							<?php } ?>
						</tr>
						<?php foreach ($period->activeServices as $activeService) {
							echo CHtml::openTag('tr');
							echo CHtml::tag('td', array(), $activeService->service->title);
							for ($i = 1; $i <= $activeServicesCount; $i++) {
								$priceId = $activeService->id . ':' . $i;
								$currentPrice = null;
								foreach ($prices as $price) {
									$rules = $price->rules;
									if ($price->service_id == $activeService->service_id && current($rules)->rule_value == $i) {
										$currentPrice = $price;
									}
								}
								echo CHtml::openTag('td');
								echo CHtml::hiddenField('ShkServicePrice[' . $priceId . '][period_id]', $period->id);
								echo CHtml::hiddenField('ShkServicePrice[' . $priceId . '][service_id]', $activeService->service_id);
								echo CHtml::textField('ShkServicePrice[' . $priceId . '][price]', isset($currentPrice) ? $currentPrice->price : '', array(
										'class' => 'form-control input-sm',
										'placeholder' => isset($currentPrice) ? $currentPrice->price : '',
									));
								echo CHtml::hiddenField('ShkServicePrice[' . $priceId . '][price_rules][0][rule_id]', $priceRuleId);
								echo CHtml::hiddenField('ShkServicePrice[' . $priceId . '][price_rules][0][rule_value]', $i);
								echo CHtml::closeTag('td');
							}
							echo CHtml::closeTag('tr');
						}
						echo CHtml::closeTag('table');
					} ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="messages">&nbsp;</div>
		<div class="tab-pane" id="settings">&nbsp;</div>
	</div>
</div>

<div class="modal-footer">
	<?php echo CHtml::button('Обнулить', array(
		'id' => 'setZeroPrices',
		'class' => 'btn btn-sm btn-warning qSubmit'
	)); ?>
	<?php $this->widget('admin.widgets.qSubmit', array(
		'type' => 'save',
		'htmlOptions' => array(
			'id' => 'paymentPeriodPricesSave',
		)
	)); ?>
	<?php $this->widget('admin.widgets.qSubmit', array(
		'type' => 'cancel',
		'confirm' => false,
		'htmlOptions' => array(
			'class' => 'pull-left',
			'data-dismiss' => 'modal',
		),
	)); ?>
</div>

<?php echo CHtml::endForm(); ?>

<script>
	jQuery(document).ready(function () {
		jQuery("#setZeroPrices").on("click", function () {
			jQuery("#periodBundlePrices").find("input[type=text]").each(function () {
				jQuery(this).val('0.00')
			});
		});

	});
</script>