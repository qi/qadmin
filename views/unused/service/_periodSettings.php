<?php
/**
 * @var ShkServicePeriod $period
 */
?>
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'period-settings',
	'enableClientValidation' => false,
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
		'validateOnType' => true,
		'inputContainer' => '.inputContainer',
		'afterValidate' => 'js:function(form, data, hasErrors){
			if (hasErrors){
				$(form).find(".popover").qP().loading().off();
				$(form).find("input[name=create], input[name=save]").popover("toggle");
				return false;
			} else {
				return true;
			}
		}'
	),
	'focus' => array($period, 'description'),
)); ?>
<?php echo CHtml::activeHiddenField($period, 'id'); ?>
<?php echo CHtml::activeHiddenField($period, 'school_id'); ?>

	<!-- Modal header -->
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title"><?php echo isset($period->school) ? $period->school->name : null; ?>
			<small>
				<?php echo isset($period->update_time) ? 'Последнее обновление: ' . date('d.m.Y H:i:s', $period->update_time) : Yii::t('ShkService', 'New payment period'); ?>
			</small>
		</h4>
	</div>

<?php if (isset($period->school) && isset($services)) { ?>

	<!-- Modal body -->
	<div class="modal-body">

	<!-- Period start and end date -->
	<div class="row">

		<!-- Period start -->
		<div id="settings-period-start" class="col-xs-6 inputContainer">
			<?php
			echo CHtml::tag('h4', array(), $form->labelEx($period, 'start_time'));
			echo CHtml::tag('hr');
			echo CHtml::openTag('div', array('class' => 'input-group'));
			echo CHtml::activeTextField($period, 'start_time', array(
				'class' => 'form-control input-sm',
				'readonly' => true,
				'value' => isset($period) ? date('d.m.Y', $period->start_time) : null,
				'placeholder' => isset($period) ? date('d.m.Y', $period->start_time) : null
			));
			echo CHtml::label('', 'ShkServicePeriod_start_time', array(
				'class' => 'btn input-group-addon glyphicon glyphicon-calendar',
			));
			echo CHtml::closeTag('div');
			echo CHtml::label(Yii::t('ShkService', 'Start date of the period'),
				'ShkServicePeriod_start_time', array(
					'class' => 'help-block',
				));
			echo $form->error($period, 'start_time');
			?>
		</div>

		<!-- Period end -->
		<div id="settings-period-end" class="col-xs-6 inputContainer">
			<?php
			echo CHtml::tag('h4', array(), $form->labelEx($period, 'end_time'));
			echo CHtml::tag('hr');
			echo CHtml::openTag('div', array('class' => 'input-group'));
			$this->widget('qPanel.extensions.datetimepicker.DateTimePicker', array(
				'label' => false,
				'model' => $period,
				'attribute' => 'end_time',
				'options' => array(
					'timepicker' => false,
					'format' => 'd.m.Y',
					'formatDate' => 'd.m.Y',
					'minDate' => isset($minTime) ? date('d.m.Y', $minTime) : false,
					'maxDate' => isset($maxTime) ? date('d.m.Y', $maxTime) : false,
				),
				'htmlOptions' => array(
					'id' => 'ShkServicePeriod_end_time',
					'name' => 'ShkServicePeriod[end_time]',
					'class' => 'form-control input-sm',
					'value' => isset($period) ? date('d.m.Y', $period->end_time) : null,
					'placeholder' => isset($period) ? date('d.m.Y', $period->end_time) : null
				),
			));
			echo CHtml::label('', 'ShkServicePeriod_end_time', array(
				'class' => 'btn input-group-addon glyphicon glyphicon-calendar',
			));
			echo CHtml::closeTag('div');
			echo CHtml::label(Yii::t('ShkService', 'End date of the period'),
				'ShkServicePeriod_end_time', array(
					'class' => 'help-block',
				));
			echo $form->error($period, 'end_time');
			?>
		</div>

	</div>

	<!-- Strategy and active services buttons -->
	<div class="row">
		<div id="settings-payment" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
			<h4><?php echo Yii::t('admin', 'Payment strategy'); ?></h4>
			<hr>
			<div class="btn-group" data-toggle="buttons">
				<?php if (isset($period->settings) && $period->settings->strategy_of_payment == 1) {
					$checked = true;
				} else {
					$checked = false;
				} ?>
				<label class="btn btn-sm btn-margin <?php echo $checked ? 'active btn-warning' : 'btn-default'; ?>">
					<?php
					echo CHtml::radioButton('ShkServicePeriodSettings[strategy_of_payment]', $checked, array(
						'value' => ShkServicePeriodSettings::STRATEGY_PRE,
					));
					echo Yii::t('admin', ShkServicePeriodSettings::getStrategyLabel(ShkServicePeriodSettings::STRATEGY_PRE));
					?>
				</label>
				<?php if (isset($period->settings) && $period->settings->strategy_of_payment == 2) {
					$checked = true;
				} else {
					$checked = false;
				} ?>
				<label class="btn btn-sm btn-margin <?php echo $checked ? 'active btn-warning' : 'btn-default'; ?>">
					<?php
					echo CHtml::radioButton('ShkServicePeriodSettings[strategy_of_payment]', $checked, array(
						'value' => ShkServicePeriodSettings::STRATEGY_POST
					));
					echo Yii::t('admin', ShkServicePeriodSettings::getStrategyLabel(ShkServicePeriodSettings::STRATEGY_POST));
					?>
				</label>
			</div>
			<span class="help-block">Оплата за текущий или предыдущий период.</span>
		</div>
		<div id="settings-services" class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
			<h4><?php echo Yii::t('admin', 'Active services'); ?></h4>
			<hr>
			<div class="btn-group" data-toggle="buttons">
				<?php foreach ($services as $service) {
					if (isset($period->services) && !empty($period->services)) {
						$active = false;
						foreach ($period->activeServices as $serviceActive) {
							if ($serviceActive->status == ShkServicePeriodActive::ACTIVE && $service->id == $serviceActive->service_id) {
								$active = true;
							}
						}
					} else {
						$active = true;
					}
					?>
					<label class="btn btn-sm btn-margin <?php echo $active == true ? 'btn-success active' : 'btn-default'; ?>">
						<?php
						//echo CHtml::hiddenField("ShkServicePeriodActive[$service->id][period_id]", $period->id);
						echo CHtml::hiddenField("ShkServicePeriodActive[$service->id][service_id]", $service->id);
						echo CHtml::checkBox("ShkServicePeriodActive[$service->id][status]", $active, array(
							'uncheckValue' => '0',
						)); ?>
						<span><?php echo $service->title; ?></span>
					</label>
				<?php } ?>
			</div>
			<span class="help-block">Активные в данном периоде услуги.</span>
		</div>
	</div>

	<!-- Approval, defarment and acceptance fields -->
	<div class="row">

		<!-- Deferment of payment -->
		<div id="settings-deferment" class="col-xs-4 inputContainer">
			<?php
			echo CHtml::tag('h4', array(), $form->labelEx($period->settings, 'deferment_of_payment'));
			echo CHtml::tag('hr');
			echo CHtml::openTag('div', array('class' => 'input-group'));
			$this->widget('qPanel.extensions.datetimepicker.DateTimePicker', array(
				'label' => false,
				'model' => $period->settings,
				'attribute' => 'deferment_of_payment',
				'options' => array(
					'timepicker' => false,
					'format' => 'd.m.Y',
				),
				'htmlOptions' => array(
					'id' => 'ShkServicePeriodSettings_deferment_of_payment',
					'name' => 'ShkServicePeriodSettings[deferment_of_payment]',
					'class' => 'form-control input-sm',
					'value' => isset($period) ? date('d.m.Y', $period->settings->deferment_of_payment) : null,
					'placeholder' => isset($period) ? date('d.m.Y', $period->settings->deferment_of_payment) : null
				),
			));
			echo CHtml::label('', 'ShkServicePeriodSettings_deferment_of_payment', array(
				'class' => 'btn input-group-addon glyphicon glyphicon-calendar',
			));
			echo CHtml::closeTag('div');
			echo CHtml::label(Yii::t('ShkService', 'Срок отсрочки платежа. В этот день произойдет попытка снятия средств
				и все выставлненные счета станут оплаченными или просроченными.'), //@todo translation
				'ShkServicePeriodSettings_deferment_of_payment', array(
					'class' => 'help-block',
				));
			echo $form->error($period->settings, 'deferment_of_payment'); ?>
		</div>

		<!-- Acceptance of bill -->
		<div id="settings-acceptance" class="col-xs-4 inputContainer">
			<?php
			echo CHtml::tag('h4', array(), $form->labelEx($period->settings, 'acceptance_of_bill'));
			echo CHtml::tag('hr');
			echo CHtml::openTag('div', array('class' => 'input-group'));
			$this->widget('qPanel.extensions.datetimepicker.DateTimePicker', array(
				'label' => false,
				'model' => $period->settings,
				'attribute' => 'acceptance_of_bill',
				'options' => array(
					'timepicker' => false,
					'format' => 'd.m.Y',
				),
				'htmlOptions' => array(
					'id' => 'ShkServicePeriodSettings_acceptance_of_bill',
					'name' => 'ShkServicePeriodSettings[acceptance_of_bill]',
					'class' => 'form-control input-sm',
					'value' => isset($period) ? date('d.m.Y', $period->settings->acceptance_of_bill) : null,
					'placeholder' => isset($period) ? date('d.m.Y', $period->settings->acceptance_of_bill) : null
				),
			));
			echo CHtml::label('', 'ShkServicePeriodSettings_acceptance_of_bill', array(
				'class' => 'btn input-group-addon glyphicon glyphicon-calendar',
			));
			echo CHtml::closeTag('div');
			echo CHtml::label(Yii::t('ShkService', 'Срок принятия счета. Новые счета могут быть созданы до конца этого дня.'), //@todo translation
				'ShkServicePeriodSettings_acceptance_of_bill', array(
					'class' => 'help-block',
				));
			echo $form->error($period->settings, 'acceptance_of_bill'); ?>
		</div>

		<!-- Approval of bill -->
		<div id="settings-approval" class="col-xs-4 inputContainer">
			<?php echo CHtml::tag('h4', array(), $form->labelEx($period->settings, 'approval_of_bill'));
			echo CHtml::tag('hr');
			echo CHtml::openTag('div', array('class' => 'input-group'));
			echo $form->textField($period->settings, 'approval_of_bill', array(
				'class' => 'form-control input-sm',
				'placeholder' => $period->settings ? $period->settings->approval_of_bill : null,
				'value' => $period->settings ? $period->settings->approval_of_bill : null,
			));
			echo CHtml::label('', 'ShkServicePeriodSettings_approval_of_bill', array(
				'class' => 'btn input-group-addon glyphicon glyphicon-pencil',
			));
			echo CHtml::closeTag('div');
			echo CHtml::label(Yii::t('ShkService', 'Количество дней на утверждение счета,
			по истечении которых с момента создания счета его нельзя будет редактировать.'), //@todo translation
				'ShkServicePeriodSettings_approval_of_bill', array(
					'class' => 'help-block',
				));
			echo $form->error($period->settings, 'approval_of_bill'); ?>
		</div>

	</div>

	<!-- Comment field -->
	<div class="row">
		<div id="period-description" class="col-xs-12 inputContainer">
			<?php
			echo CHtml::tag('h4', array(), $form->labelEx($period, 'description'));
			echo CHtml::tag('hr');
			echo $form->textArea($period, 'description', array('class' => 'form-control'));
			echo $form->error($period, 'description');
			?>
		</div>
	</div>

	</div>

<?php } ?>

	<!-- Modal footer -->
	<div class="modal-footer">
		<?php
		$this->widget('admin.widgets.qSubmit', array(
			'type' => 'cancel',
			'confirm' => false,
			'htmlOptions' => array(
				'class' => 'pull-left',
				'data-dismiss' => 'modal',
			),
		));
		if ($period->isDeletable && Yii::app()->user->isAdmin) {
			$this->widget('admin.widgets.qSubmit', array(
				'type' => 'delete',
				'htmlOptions' => array(
					'id' => 'paymentPeriodDelete',
				),
			));
		}
		$this->widget('admin.widgets.qSubmit', array(
			'type' => $period->isNewRecord ? 'create' : 'save',
			'htmlOptions' => array(
				'id' => 'paymentPeriodSettingsSave',
			),
		));
		?>
	</div>

<?php $this->endWidget(); ?>