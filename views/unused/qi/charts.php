<?php $this->renderPartial('/layouts/_defaultPageHeader', array(
	'header' => Yii::t('qAdmin', 'Charts'),
	'small' => Yii::t('qAdmin', ''),
)); ?>

<div id="page-content" class="row">
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-12">
				<?php $this->beginWidget('qPanel', array(
					'id' => 'flot-chart-moving',
					'title' => 'Flot Moving chart',
					'collapsible' => true,
				)); ?>
				<div class="panel-body">
					<div id="flot" style="height:200px"></div>
				</div>
				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>

	<script>
		$(function () {

			var speed = 100;
			var plotDataLength = 60 * 5;
			var stack = [];
			var plotData = [];

			updateStack();
			updatePlotData();

			var plot = $.plot("#flot", plotData, {
				series: {
					shadowSize: 0,
					lines: {
						fill: true
					}
				},
				colors: ["#0a929c"],
				xaxis: {show: false}
			});

			updatePlot();

			function updateStack() {
				if (stack.length <= plotDataLength * 2) {
					$.get("/admin/qi/ajaxGetFlotData", {length: plotDataLength}, function (response) {
						stack = stack.concat(response.data);
					}, 'json');
				}
				setTimeout(updateStack, plotDataLength * speed);
			}

			function updatePlotData() {
				if (plotData.length > 0)
					plotData = plotData.slice(1);
				while (plotData.length < plotDataLength) {
					if (stack.length > 0) {
						plotData.push(stack[0]);
						stack = stack.slice(1);
					} else {
						plotData.push(0);
					}
				}
			}

			function updatePlot() {
				updatePlotData();
				var axes = plot.getAxes();
				axes.yaxis.options.max = Math.max.apply(Math, plotData);
				var normalizedPlotData = [];
				for (var i = 0; i < plotData.length; i++)
					normalizedPlotData.push([i, plotData[i]]);
				plot.setData([normalizedPlotData]);
				plot.setupGrid();
				plot.draw();
				setTimeout(updatePlot, speed);
			}
		});
	</script>