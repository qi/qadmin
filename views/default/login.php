<?php
/**
 * qPanel login form.
 * @author andcherv
 * @var Controller $this
 * @var LoginForm $model
 */
?>
	<div id="page-header" class="center-block text-center">
		<div class="page-header">
			<a href="<?php echo Yii::app()->homeUrl; ?>">
				<?php if (isset($this->module->logoUrl))
					echo CHtml::image($this->module->logoUrl, Yii::app()->name, array('style' => 'width: 148px; height: 148px;'));
				echo CHtml::tag('h1', array('style' => 'margin-top: -24px;'), Yii::app()->name); ?>
			</a>
		</div>
	</div>
<?php $this->beginWidget('qi.qadmin.widgets.QiPanel', array(
	'id' => 'login-panel',
	'title' => Yii::t('qadmin', 'Sign in'),
	'collapsible' => false,
	'htmlOptions' => array(
		'class' => 'panel panel-default center-block',
		'style' => 'width: 420px; margin-top: 40px;'
	),
)); ?>
	<div class="panel-body">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'login-form',
			'focus' => array($model, 'email'),
			'enableClientValidation' => true,
			'enableAjaxValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'validateOnChange' => false,
				'validateOnType' => false,
				'errorCssClass' => 'error has-error',
				'successCssClass' => 'success has-success',
			),
			'htmlOptions' => array(
				'class' => 'form-horizontal',
			),
			'errorMessageCssClass' => 'help-block',
		)); ?>
		<div class="form-group">
			<div class="col-xs-12">
				<?php echo $form->textField($model, 'email', array(
					'class' => 'form-control',
					'placeholder' => $model->getAttributeLabel('email'),
				)); ?>
				<?php echo $form->error($model, 'email'); ?>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<?php echo $form->passwordField($model, 'password', array(
					'class' => 'form-control',
					'placeholder' => $model->getAttributeLabel('password'),
				)); ?>
				<?php echo $form->error($model, 'password'); ?>
			</div>
		</div>
		<span class="help-block text-right sr-only">
			<?php echo CHtml::link(Yii::t('qadmin', 'Forgot your password?'), '/restore') ?>
		</span>

		<div class="form-group">
			<div class="col-xs-8">
				<div class="checkbox">
					<?php echo $form->checkBox($model, 'rememberMe', array('style' => 'margin-left: 0;')); ?>
					<?php echo $form->label($model, 'rememberMe'); ?>
					<?php echo $form->error($model, 'rememberMe'); ?>
				</div>
			</div>
			<div class="col-xs-4">
				<?php echo CHtml::submitButton(Yii::t('qadmin', 'Sign in'), array(
					'class' => 'btn btn-primary pull-right'
				)); ?>
			</div>
		</div>
		<?php $this->endWidget('login-form'); ?>
	</div>
<?php $this->endWidget('login-panel'); ?>