(function ($) {
    $.fn.qP = function () {

        // NOTIFICATIONS
        //this.notifications = function () {
        //    this.update = function () {
        //        $.ajax({
        //            type: 'GET',
        //            dataType: 'json',
        //            url: '/admin/default/ajaxCheckFlashes/'
        //        }).fail(function () {
        //                console.log('Notifications update error');
        //            }).success(function (response) {
        //                var timeout = 0;
        //                $.each(response, function (index, flash) {
        //                    timeout = timeout + 500;
        //                    flash = $.parseJSON(flash);
        //                    setTimeout(function () {
        //                        $.pnotify({
        //                            title: flash['title'] ? flash['title'] : null,
        //                            text: flash['text'] ? flash['text'] : null,
        //                            type: flash['type'] ? flash['type'] : null,
        //                            icon: flash['icon'] ? flash['icon'] : true,
        //                            hide: flash['hide'] != "false"
        //                        });
        //                    }, timeout);
        //                });
        //                console.log('Notifications updated');
        //            });
        //    };
        //    return this;
        //};

        // LOADING
        this.loading = function (positionFix) {
            this.positionFix = positionFix && positionFix == true ? true : false;
            this.loadingPosition = null;
            this.on = function () {
                if (this.positionFix == true) {
                    this.loadingPosition = $(this).css("position");
                    $(this).css("position", "relative");
                }
                var div = $("<div class='loading'>&nbsp</div>");
                $(div).css("position", "absolute");
                $(div).css("z-index", parseInt($(this).css("z-index")) + 1000);
                $(div).css("width", "100%");
                $(div).css("height", "100%");
                $(div).css("top", "0");
                $(div).css("left", "0");
                $(this).append(div);
            };
            this.off = function () {
                if (this.positionFix == true) {
                    $(this).css("position", this.loadingPosition);
                }
                $(this).find(".loading").each(function () {
                    this.remove();
                });
            };
            return this;
        };

        // MODAL
        this.modal = function() {
            // displays modal with ajax loaded data (GET only)
            this.load = function (modalId, url) {
                $(modalId).find(".modal-content").html("&nbsp;").addClass('loading');
                $.get(url, function (data) {
                    //setTimeout(function () {
                        $(modalId).find(".modal-content").removeClass('loading').html(data);
                    //}, 100);
                });
            };
            this.afterValidate = function(form, data, hasErrors) {
                if (hasErrors){
                    $(form).find(".popover").qP().loading().off();
                    $(form).find("input[name=create], input[name=save]").popover("toggle");
                    return false;
                } else {
                    return true;
                }
            };
            return this;
        };

        return this;
    };
})(jQuery);