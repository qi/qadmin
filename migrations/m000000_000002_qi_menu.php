<?php

class m000000_000002_qi_menu extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('qi_menu', array(
			'id' => 'pk',
			'title' => 'string not null',
		));
		$this->createTable('qi_menu_assignment', array(
			'menu_id' => 'integer not null',
			'type' => 'integer not null',
			'assignment' => 'string not null',
			'primary key (`menu_id`, `type`, `assignment`)',
			'foreign key (`menu_id`) references `qi_menu` (`id`) on delete cascade on update cascade',
		));
		$this->createTable('qi_menu_item', array(
			'name' => 'varchar(100) not null',
			'title' => 'string not null',
			'route' => 'string not null',
			'description' => 'text',
			'primary key (`name`)',
		));
		$this->createTable('qi_menu_item_child', array(
			'menu_id' => 'integer not null',
			'parent_name' => 'varchar(100) not null',
			'child_name' => 'varchar(100) not null',
			'primary key (`menu_id`, `parent_name`, `child_name`)',
			'foreign key (`menu_id`) references `qi_menu` (`id`) on delete cascade on update cascade',
			'foreign key (`parent_name`) references `qi_menu_item` (`name`) on delete cascade on update cascade',
			'foreign key (`child_name`) references `qi_menu_item` (`name`) on delete cascade on update cascade',
		));

		$this->insert('qi_menu', array(
			'id' => 1,
			'title' => 'Default Menu',
		));
		$this->insert('qi_menu_assignment', array(
			'menu_id' => 1,
			'type' => 1,
			'assignment' => 'admin',
		));
		$this->insert('qi_menu_item', array(
			'name' => 'root',
			'title' => 'Root Menu Item',
			'route' => '',
		));
		$this->insert('qi_menu_item', array(
			'name' => 'dashboard',
			'title' => 'Dashboard',
			'route' => 'dashboard',
		));
		$this->insert('qi_menu_item', array(
			'name' => 'settings',
			'title' => 'Settings',
			'route' => 'dashboard/settings',
		));
		$this->insert('qi_menu_item', array(
			'name' => 'settings-maintenance',
			'title' => 'System Maintenance',
			'route' => 'dashboard/settings/maintenance',
		));
		$this->insert('qi_menu_item', array(
			'name' => 'settings-help',
			'title' => 'Page Help Management',
			'route' => 'dashboard/settings/help',
		));
		$this->insert('qi_menu_item_child', array(
			'menu_id' => '1',
			'parent_name' => 'root',
			'child_name' => 'dashboard',
		));
		$this->insert('qi_menu_item_child', array(
			'menu_id' => '1',
			'parent_name' => 'root',
			'child_name' => 'settings',
		));
		$this->insert('qi_menu_item_child', array(
			'menu_id' => '1',
			'parent_name' => 'settings',
			'child_name' => 'settings-maintenance',
		));
		$this->insert('qi_menu_item_child', array(
			'menu_id' => '1',
			'parent_name' => 'settings',
			'child_name' => 'settings-help',
		));
	}

	public function safeDown()
	{
		$this->dropTable('qi_menu_item_child');
		$this->dropTable('qi_menu_item');
		$this->dropTable('qi_menu_assignment');
		$this->dropTable('qi_menu');
	}
}