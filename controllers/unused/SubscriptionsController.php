<?php

class SubscriptionsController extends DefaultController
{
	public $layout = 'default';

	public function actionIndex()
	{
		$this->pageTitle = Yii::t('Community', 'Subscriptions');
		$this->addBreadcrumbs(array(
			Yii::t('Community', 'Communitites') => Yii::app()->createUrl('admin/communities'),
			Yii::t('Community', 'Subscriptions'),
		));

		$subscriptionsModel = new UserCommunityRel('search');
		$subscriptionsModel->unsetAttributes();
		if (isset($_GET[get_class($subscriptionsModel)]))
			$subscriptionsModel->attributes = $_GET[get_class($subscriptionsModel)];

		if (isset($_GET['subscriptionsGridPageSize']))
			Yii::app()->user->setState('subscriptionsGridPageSize', $_GET['subscriptionsGridPageSize']);

		$this->render('index', array(
			'subscriptionsModel' => $subscriptionsModel,
		));
	}

	public function actionAjaxUpdateStats()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$totalCount = UserCommunityRel::model()->with(array(
				'metadata:active',
				'community' => array(
					'with' => array(
						'metadata:active',
						'school.metadata:active'
					),
				),
			))->count();
			$publicCount = UserCommunityRel::model()->with(array(
				'metadata:active',
				'community' => array(
					'with' => array(
						'metadata:active',
						'school.metadata:active'
					),
				),
			))->count(array(
					'condition' => 'school.type=:public',
					'params' => array(':public' => School::TYPE_PUBLIC),
				));
			$connectedCount = UserCommunityRel::model()->with(array(
				'metadata:active',
				'community' => array(
					'with' => array(
						'metadata:active',
						'school.metadata:active'
					),
				),
			))->count(array(
					'condition' => 'school.type=:public',
					'params' => array(':public' => School::TYPE_CONNECTED),
				));
			echo CJSON::encode([
				'status' => 'success',
				'data' => [
					'total' => $totalCount,
					'public' => $publicCount,
					'connected' => $connectedCount,
					'pctPublic' => $totalCount > 0 ? round($publicCount * 100 / $totalCount, 1) : 0,
					'pctConnected' => $totalCount > 0 ? round($connectedCount * 100 / $totalCount, 1) : 0,
				],
			]);
		}
		Yii::app()->end();
	}
}