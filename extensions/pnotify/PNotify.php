<?php

/**
 * Class PNotify
 * @todo phpdoc and readme.md
 */
class PNotify extends CWidget
{
	public $htmlOptions = [];
	public $options = [];
	public $baseScriptUrl;
	public $flashes = false;
	private $_flashesDefaults = [
		'route' => null,
		'updateInterval' => 180000,
		'showTimeout' => 500,
	];

	public function init()
	{
		if ($this->flashes !== false && is_array($this->flashes))
			$this->flashes = CMap::mergeArray($this->_flashesDefaults, $this->flashes);
		$cs = Yii::app()->clientScript;
		$path = Yii::getPathOfAlias('qi.qadmin.extensions.pnotify');
		if ($this->baseScriptUrl === null)
			$this->baseScriptUrl = Yii::app()->getAssetManager()->publish($path . DIRECTORY_SEPARATOR . 'assets');
		$cs->registerCssFile($this->baseScriptUrl . DIRECTORY_SEPARATOR . 'pnotify.custom.min.css');
		$cs->registerScriptFile($this->baseScriptUrl . DIRECTORY_SEPARATOR . 'pnotify.custom.min.js', CClientScript::POS_END);
		$cs->registerScript($this->id, $this->getScript(), CClientScript::POS_READY);
	}

	protected function getScript()
	{
		if ($this->flashes === false && !empty($this->options))
			$script = 'new PNotify(' . CJavaScript::encode($this->options) . ');';
		else
			$script = 'updateFlashes();setInterval(function(){updateFlashes();},' . $this->flashes['updateInterval'] . ');function updateFlashes(){jQuery.get("' . $this->flashes['route'] . '",function(response){var timeout=0;jQuery.each(response,function(index,flash){timeout=timeout+' . $this->flashes['showTimeout'] . ';setTimeout(function(){new PNotify(flash);},timeout);});},"json");}';
		return $script;
	}
}