<?php
/**
 * @author andcherv
 * @var PageHelp $helpModel
 */
?>
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'pageHelpForm',
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
		'validateOnType' => true,
		'inputContainer' => '.inputContainer',
		'afterValidate' => 'js:function(form, data, hasErrors){
			if (hasErrors){
				$(form).find(".popover").qP().loading().off();
				$(form).find("input[name=create], input[name=save]").popover("toggle");
				return false;
			} else {
				return true;
			}
		}'
	),
	'focus' => array($helpModel, 'title'),
)); ?>
<?php echo CHtml::activeHiddenField($helpModel, 'id'); ?>

	<!-- Modal header -->
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title"><?php echo isset($helpModel->title) ? $helpModel->title : Yii::t('qPanel', 'New Page Help'); ?>
			<small><?php echo isset($helpModel->url) ? $helpModel->url : '&nbsp;'; ?></small>
		</h4>
	</div>

	<!-- Modal body -->
	<div class="modal-body">
		<div class="row">
			<div class="col-xs-8 inputContainer">
				<?php echo CHtml::tag('h4', array(), $form->labelEx($helpModel, 'title'));
				echo CHtml::tag('hr');
				echo $form->textField($helpModel, 'title', array(
					'class' => 'form-control input-sm',
					'placeholder' => isset($helpModel->title) ? $helpModel->title : '',
				));
				echo CHtml::label(Yii::t('qPanel', 'Title'), 'PageHelp_title', array(
					'class' => 'help-block',
				));
				echo $form->error($helpModel, 'title'); ?>
			</div>
			<div class="col-xs-4 inputContainer">
				<?php echo CHtml::tag('h4', array(), $form->labelEx($helpModel, 'language'));
				echo CHtml::tag('hr');
				echo $form->textField($helpModel, 'language', array(
					'class' => 'form-control input-sm',
					'placeholder' => isset($helpModel->language) ? $helpModel->language : '',
				));
				echo CHtml::label(Yii::t('qPanel', 'Language'), 'PageHelp_language', array(
					'class' => 'help-block',
				));
				echo $form->error($helpModel, 'language');    ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4 inputContainer">
				<?php echo CHtml::tag('h4', array(), $form->labelEx($helpModel, 'module'));
				echo CHtml::tag('hr');
				echo $form->textField($helpModel, 'module', array(
					'class' => 'form-control input-sm',
					'placeholder' => isset($helpModel->module) ? $helpModel->module : '',
				));
				echo CHtml::label(Yii::t('qPanel', 'Module ID'), 'PageHelp_module', array(
					'class' => 'help-block',
				));
				echo $form->error($helpModel, 'module'); ?>
			</div>
			<div class="col-xs-4 inputContainer">
				<?php echo CHtml::tag('h4', array(), $form->labelEx($helpModel, 'controller'));
				echo CHtml::tag('hr');
				echo $form->textField($helpModel, 'controller', array(
					'class' => 'form-control input-sm',
					'placeholder' => isset($helpModel->controller) ? $helpModel->controller : '',
				));
				echo CHtml::label(Yii::t('qPanel', 'Controller ID'), 'PageHelp_controller', array(
					'class' => 'help-block',
				));
				echo $form->error($helpModel, 'controller'); ?>
			</div>
			<div class="col-xs-4 inputContainer">
				<?php echo CHtml::tag('h4', array(), $form->labelEx($helpModel, 'action'));
				echo CHtml::tag('hr');
				echo $form->textField($helpModel, 'action', array(
					'class' => 'form-control input-sm',
					'placeholder' => isset($helpModel->action) ? $helpModel->action : '',
				));
				echo CHtml::label(Yii::t('qPanel', 'Action ID'), 'PageHelp_action', array(
					'class' => 'help-block',
				));
				echo $form->error($helpModel, 'action'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 inputContainer">
				<?php echo CHtml::tag('h4', array(), $form->labelEx($helpModel, 'content'));
				echo CHtml::tag('hr');
				$this->widget('application.vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', array(
					'model' => $helpModel,
					'attribute' => 'content',
					'options' => array(
//						'lang' => Yii::app()->language ? Yii::app()->language : 'en',
						'lang' => 'en',
						'placeholder' => isset($helpModel->content) ? $helpModel->content : $helpModel->getAttributeLabel('content'),
						'autoresize' => false,
						'minHeight' => '200',
					),
				));
				echo CHtml::label('', 'PageHelp_content', array(
					'class' => 'help-block',
				));
				echo $form->error($helpModel, 'content'); ?>
			</div>
		</div>
	</div>

	<!-- Modal footer -->
	<div class="modal-footer">
		<?php
		$this->widget('qi.qadmin.widgets.QiSubmit', array(
			'type' => 'cancel',
			'confirm' => false,
			'htmlOptions' => array(
				'class' => 'pull-left',
				'data-dismiss' => 'modal',
			),
		));
		if (!$helpModel->isNewRecord) {
			$this->widget('qi.qadmin.widgets.QiSubmit', array(
				'type' => 'delete',
			));
		}
		$this->widget('qi.qadmin.widgets.QiSubmit', array(
			'type' => $helpModel->isNewRecord ? 'create' : 'save',
		));
		?>
	</div>

<?php $this->endWidget(); ?>