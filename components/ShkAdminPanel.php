<?php

/**
 * Class ShkAdminPanel
 * @author andcherv
 */
class ShkAdminPanel extends CApplicationComponent
{
	/**
	 * School ID session varible name.
	 * @author andcherv
	 */
	const SESSION_SCHOOL = 'shk_admin_panel_school_id';

	/**
	 * Academic year ID session varible name.
	 * @author andcherv
	 */
	const SESSION_YEAR = 'shk_admin_panel_academic_year_id';

	/**
	 * Payment period ID session varible name.
	 * @author andcherv
	 */
	const SESSION_PERIOD = 'shk_admin_panel_payment_period_id';

	/**
	 * Enable/disable global school filter.
	 * @author andcherv
	 * @var bool
	 */
	public $schoolSelectActive = false;

	/**
	 * Enable/disable global year filter.
	 * @author andcherv
	 * @var bool
	 */
	public $yearSelectActive = false;

	/**
	 * Enable/disable global period filter.
	 * @author andcherv
	 * @var bool
	 */
	public $periodSelectActive = false;

	/**
	 * School model.
	 * @author andcherv
	 * @var
	 */
	protected $_schoolModel;

	/**
	 * Academic year model.
	 * @author andcherv
	 * @var
	 */
	protected $_yearModel;

	/**
	 * Payment period model.
	 * @author andcherv
	 * @var
	 */
	protected $_periodModel;

	/**
	 * Quick page help.
	 * @author andcherv
	 * @var
	 */
	private $_pageHelp;

	/**
	 * Sets or unsets school ID session variable.
	 * @author andcherv
	 * @param int|null $id
	 */
	public function setSchool($id)
	{
		Yii::app()->user->setState(self::SESSION_SCHOOL, $id, null);
		Yii::app()->user->setState(self::SESSION_YEAR, null, null);
		Yii::app()->user->setState(self::SESSION_PERIOD, null, null);
	}

	/**
	 * Sets or unsets academic year ID session variable.
	 * @author andcherv
	 * @param int|null $id
	 */
	public function setYear($id)
	{
		Yii::app()->user->setState(self::SESSION_YEAR, $id, null);
		Yii::app()->user->setState(self::SESSION_PERIOD, null, null);
	}

	/**
	 * Sets or unsets payment period ID session variable.
	 * @author andcherv
	 * @param int|null $id
	 */
	public function setPeriod($id)
	{
		Yii::app()->user->setState(self::SESSION_PERIOD, $id, null);
	}

	/**
	 * Returns school ID from session or its model (id and name by default).
	 * @author andcherv
	 * @param bool $getModel
	 * @param null $criteria
	 * @param bool $force
	 * @return int|CActiveRecord|null
	 */
	public function getSchool($getModel = false, $criteria = null, $force = false)
	{
		return $this->getPanelAttribute(self::SESSION_SCHOOL, array(
			'modelName' => $getModel === true ? 'School' : null,
			'defaultSelect' => 'id, name',
			'criteria' => $criteria,
		), $force === false ? '_schoolModel' : null);
	}

	/**
	 * Returns academic year ID from session or its model (id, school_id, start_time and end_time by default).
	 * @author andcherv
	 * @param bool $getModel
	 * @param null $criteria
	 * @param bool $force
	 * @return int|CActiveRecord|null
	 */
	public function getYear($getModel = false, $criteria = null, $force = false)
	{
		return $this->getPanelAttribute(self::SESSION_YEAR, array(
			'modelName' => $getModel === true ? 'ShkSchoolAcademicYear' : null,
			'defaultSelect' => 'id, school_id, start_time, end_time',
			'criteria' => $criteria,
		), $force === false ? '_yearModel' : null);
	}

	/**
	 * Returns payment period ID from session or its model (id, school_id, start_time and end_time by default).
	 * @author andcherv
	 * @param bool $getModel
	 * @param null $criteria
	 * @param bool $force
	 * @return int|CActiveRecord|null
	 */
	public function getPeriod($getModel = false, $criteria = null, $force = false)
	{
		return $this->getPanelAttribute(self::SESSION_PERIOD, array(
			'modelName' => $getModel === true ? 'ShkServicePeriod' : null,
			'defaultSelect' => 'id, school_id, start_time, end_time',
			'criteria' => $criteria,
		), $force === false ? '_periodModel' : null);
	}

	/**
	 * Sets predefined flash notification by label.
	 * @author andcherv
	 * @param string $label Flash label
	 * @param null|string $message Custom message
	 */
	public function setFlash($label, $message = null)
	{
		$flash = self::flashes($label, $message);
		if (isset($flash)) {
			Yii::app()->user->setFlash($label, CJSON::encode($flash));
		}
	}

	/**
	 * Returns quick help string for current page.
	 * @author andcherv
	 * @param array $route Page route (module, controller, action)
	 * @return null
	 * @todo remove?
	 */
	public function getPageHelp($route = array())
	{
		if (empty($route) || !is_array($route)) {
			$route = array(
				'module' => isset(Yii::app()->controller->module->id) ? Yii::app()->controller->module->id : null,
				'controller' => isset(Yii::app()->controller->id) ? Yii::app()->controller->id : null,
				'action' => isset(Yii::app()->controller->action->id) ? Yii::app()->controller->action->id : null,
			);
		}

		if (isset($route['module']) && isset($route['controller']) && isset($route['action'])) {
			if (!isset($this->_pageHelp) || !is_object($this->_pageHelp) ||
				$this->_pageHelp->module != $route['module'] ||
				$this->_pageHelp->controller != $route['controller'] ||
				$this->_pageHelp->action != $route['action']
			) {
				$this->_pageHelp = PageHelp::model()->find(array(
					'condition' => 'module=:module AND controller=:controller AND action=:action AND language=:language',
					'params' => array(
						':module' => $route['module'],
						':controller' => $route['controller'],
						':action' => $route['action'],
						':language' => Yii::app()->language ? Yii::app()->language : 'en',
					)));
			}
		}
		return isset($this->_pageHelp->content) ? $this->_pageHelp->content : null;
	}

	public function setFilters(array $filters)
	{
		if (empty($filters)) {
			$this->schoolSelectActive = false;
			$this->yearSelectActive = false;
			$this->periodSelectActive = false;
		} else {
			if (in_array('school', $filters))
				$this->schoolSelectActive = true;
			if (in_array('year', $filters))
				$this->yearSelectActive = true;
			if (in_array('period', $filters))
				$this->periodSelectActive = true;
		}
	}

	/**
	 * Returns global filters configuration string.
	 * @author andcherv
	 * @return string
	 */
	public function getFilterString()
	{
		$filterString = array();
		if ($this->schoolSelectActive === true) {
			if ($panelSchool = Yii::app()->shkPanel->getSchool(true)) {
				array_push($filterString, $panelSchool->name);
			} else {
				array_push($filterString, Yii::t('admin', 'All schools'));
			}
		}
		if ($this->yearSelectActive === true) {

			if ($panelYear = Yii::app()->shkPanel->getYear(true)) {
				array_push($filterString, $panelYear->range);
			} else {
				array_push($filterString, Yii::t('admin', 'All years'));
			}
		}
		if ($this->periodSelectActive === true) {

			if ($panelPeriod = Yii::app()->shkPanel->getPeriod(true)) {
				array_push($filterString, $panelPeriod->range);
			} else {
				array_push($filterString, Yii::t('admin', 'All periods'));
			}
		}
		return implode(', ', $filterString);
	}

	/**
	 * Returns admin panel attributes from session by its name.
	 * @author andcherv
	 * @param string $sessionVarName
	 * @param array $attributes ['modelName', 'defaultSelect', 'criteria']. If modelName not set, only session variable value (ID) will be returned.
	 * @param bool|string $varName if set, $this->$varName variable will be used for storing model
	 * @return mixed
	 */
	protected function getPanelAttribute($sessionVarName, array $attributes, $varName = null)
	{
		$id = Yii::app()->user->getState($sessionVarName);
		if ($attributes['modelName'] !== null && $id !== null) {
			if ($varName !== null && isset($this->$varName) && $this->$varName->id == $id) {
			} else {
				if ($attributes['criteria'] === null)
					$attributes['criteria'] = new CDbCriteria();
				$attributes['criteria']->mergeWith(array(
					'condition' => "id=$id",
					'select' => $attributes['defaultSelect'],
				));
				$this->$varName = $attributes['modelName']::model()->find($attributes['criteria']);
			}
			return $this->$varName;
		}
		return $id;
	}

	/**
	 * Returns predefined flash notification by label.
	 * @author andcherv
	 * @param string $label Flash label
	 * @param null|string $message Custom message
	 * @return null|array Pines Notify attributes http://pinesframework.org/pnotify/
	 */
	protected function flashes($label, $message)
	{
		$flashes = array(
			'shk-admin-select-school' => array(
				'title' => Yii::t('admin', 'Выберите школу'),
				'text' => Yii::t('admin', 'Для продолжения необходимо выбрать школу в выпадающем списке главной панели.'),
				'hide' => 'false',
			),

			'shk-admin-save-success' => array(
				'title' => Yii::t('admin', 'Saved'),
				'text' => Yii::t('admin', 'Saved successfully'),
				'type' => 'success',
			),

			'shk-admin-delete-success' => array(
				'title' => Yii::t('admin', 'Deleted'),
				'text' => Yii::t('admin', 'Deleted successfully'),
				'type' => 'success',
			),

			'shk-admin-year-change-success' => array(
				'title' => Yii::t('admin', 'Current academic year'),
				'text' => $message,
				'type' => 'info',
			),
			'shk-admin-year-change-fail' => array(
				'title' => Yii::t('admin', 'Error'),
				'text' => Yii::t('admin', 'Year change error.'),
				'type' => 'error',
			),
			'shk-admin-period-change-success' => array(
				'title' => Yii::t('admin', 'Current payment period'),
				'text' => $message,
				'type' => 'info',
			),
			'shk-admin-period-change-fail' => array(
				'title' => Yii::t('admin', 'Error'),
				'text' => Yii::t('admin', 'Period change error.'),
				'type' => 'error',
			),
			'shk-admin-school-change-success' => array(
				'title' => Yii::t('admin', 'Current school'),
				'text' => $message,
				'type' => 'info',
			),
			'shk-admin-school-change-fail' => array(
				'title' => Yii::t('admin', 'Error'),
				'text' => Yii::t('admin', 'School change error.'),
				'type' => 'error',
			),

			'shk-system-assets-clean-success' => array(
				'title' => Yii::t('admin', 'Assets cleaned successfully'),
				'text' => $message,
				'type' => 'success',
			),
			'shk-system-assets-clean-fail' => array(
				'title' => Yii::t('admin', 'Error'),
				'text' => Yii::t('admin', 'Assets clean error.'),
				'type' => 'error',
			),

			'shk-service-bill-pay-success' => array(
				'title' => Yii::t('ShkService', 'Bill paid'),
				'text' => Yii::t('ShkService', 'Bill was successfully paid.'),
				'type' => 'success',
			),

			'shk-service-bill-pay-error' => array(
				'title' => Yii::t('ShkService', 'Bill payment error'),
				'text' => $message,
				'type' => 'error',
			),

			'shk-service-billing-disabled' => array(
				'title' => Yii::t('ShkService', 'Billing 2.0 is disabled'),
				'text' => Yii::t('ShkService', 'Billing 2.0 for this school is disabled. To continue, you need to activate this mode in the <a href="/admin/service/settings">settings</a>.'),
				'type' => 'warning',
				'width' => '440px',
				'hide' => false,
			),

			'qPanel-save-success' => array(
				'title' => Yii::t('admin', 'Saved'),
				'text' => Yii::t('admin', 'Saved successfully'),
				'type' => 'success',
			),

			'qPanel-delete-success' => array(
				'title' => Yii::t('admin', 'Deleted'),
				'text' => Yii::t('admin', 'Deleted successfully'),
				'type' => 'success',
			),

			// Common errors
			'shk-admin-transaction-fail' => array(
				'title' => Yii::t('admin', 'Transaction Failed'),
				'text' => $message,
				'type' => 'error',
			),
			'shk-admin-model-error' => array(
				'title' => Yii::t('admin', 'Validation Error'),
				'text' => $message,
				'type' => 'error',
			),

			'qPanel-transaction-fail' => array(
				'title' => Yii::t('qPanel', 'Transaction Failed'),
				'text' => isset($message) ? $message : Yii::t('qPanel', 'Transaction Failed'),
				'type' => 'error',
			),
			'qPanel-validation-error' => array(
				'title' => Yii::t('qPanel', 'Validation Error'),
				'text' => isset($message) ? $message : Yii::t('qPanel', 'Validation Error'),
				'type' => 'error',
			),

			// Panel notifications

			'qPanel-select-period' => array(
				'title' => Yii::t('ShkService', 'Select a period'),
				'text' => Yii::t('ShkService', 'Select a payment period to add a new discount.'),
				'type' => 'info',
			),
		);

		return isset($flashes[$label]) ? $flashes[$label] : null;
	}

	public function checkSchoolBillingMode()
	{
		$school = $this->getSchool(true);
		if (isset($school) && isset($school->serviceSettings) && $school->serviceSettings->isBillingActive) {
			return true;
		} elseif (isset($school)) {
			$this->setFlash('shk-service-billing-disabled');
		}
		return false;
	}

}