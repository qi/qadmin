<?php
/**
 * @var CController $this
 * @var CliCommand $commandModel
 * @var CliLaunch $launchModel
 */
?>

<?php $this->renderPartial('/layouts/_defaultPageHeader', array(
	'header' => Yii::t('qAdmin', 'Commands'),
)); ?>

<div id="page-content" class="row">
	<div class="col-xs-12">
		<div class="row">
			<div class="col-md-4">
				<!--				--><?php //$this->renderPartial('_statsPanel', array()); ?>
				<?php $this->renderPartial('_commandsPanel', array('commandModel' => $commandModel)); ?>
			</div>
			<div class="col-md-8">
				<?php $this->renderPartial('_launchesPanel', array('launchModel' => $launchModel)); ?>
			</div>
		</div>
	</div>
</div>