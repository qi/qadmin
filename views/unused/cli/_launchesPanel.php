<?php
/**
 * @var CController $this
 * @var CliLaunch $launchModel
 */
?>

<?php $this->beginWidget('admin.widgets.qPanel', [
	'id' => 'launches-panel',
	'title' => 'Launches History',
//	'buttons' => [ //@todo refresh button
//		CHtml::dropDownList('page-size', Yii::app()->user->getState('pageSize'), [
//			10 => 10, 30 => 30, 50 => 50, 100 => 100,
//		], [
//			'class' => 'form-control input-sm pull-right',
//			'style' => 'width: 70px; float: right;',
//		])
//	],
//	'js' => '$("#page-size").on("change",function(){$.fn.yiiGridView.update("schools-grid",{data:{pageSize:$(this).val()}});});'
]); ?>
	<div class="panel-body table-responsive">
		<?php $this->widget('qModal', ['id' => 'detailsModal']); ?>
		<?php $this->widget('zii.widgets.grid.CGridView', [
			'id' => 'launches-grid',
			'dataProvider' => $launchModel->search(),
			'filter' => $launchModel,
			'itemsCssClass' => 'table table-condensed table-hover text-center',
			'pager' => ['header' => false],
			'rowCssClassExpression' => function ($row, $data) {
					switch ($data->status) {
						case CliLaunch::STATUS_SUCCESS:
							$rowClass = "success";
							break;
						case CliLaunch::STATUS_PROCESSING:
							$rowClass = "warning";
							break;
						case CliLaunch::STATUS_FAILURE:
							$rowClass = "danger";
							break;
						default:
							$rowClass = "default";
					}
					return $rowClass;
				},
			/*'columns' => [
				['headerHtmlOptions' => ['class' => 'grid-icon'], 'type' => 'raw',
					'value' => 'CHtml::tag(\'i\', [\'class\' => \'fa fa-lg fa-info-circle\'],\'\')'],
				['headerHtmlOptions' => ['class' => 'col-md-1'], 'type' => 'raw', 'name' => 'type',
					'filter' => ['connected' => 'connected', 'public' => 'public'],
					'value' => 'CHtml::tag(\'i\',[\'class\'=>\'fa fa-tag \'.$data->type],\'&nbsp;\').$data->type'],
				['headerHtmlOptions' => ['class' => 'col-md-1'], 'name' => 'id'],
				['headerHtmlOptions' => ['class' => 'col-md-3'], 'name' => 'public_id'],
				['headerHtmlOptions' => ['class' => 'col-md-1'], 'name' => 'shk_id'],
				['headerHtmlOptions' => ['class' => 'col-md-1'], 'name' => 'vk_id'],
				['headerHtmlOptions' => ['class' => 'col-md-3'], 'name' => 'name'],
				['headerHtmlOptions' => ['class' => 'col-md-2'], 'name' => 'city_id'],
				['headerHtmlOptions' => ['class' => 'grid-icon'], 'type' => 'raw',
					'value' => 'CHtml::link(
						CHtml::tag(\'i\', [\'class\' => \'fa fa-lg fa-edit\']), null, [
							\'data-toggle\' => \'modal\', \'data-target\' => \'#editModal\',
							\'data-source\' => \'/admin/schools/ajaxEditModal?id=\'.$data->id,
							\'title\' => \'' . Yii::t('qAdmin', 'Edit') . '\'
						])',
				],
			],*/
		]); ?>
	</div>
<?php $this->endWidget(); ?>