<?php
/**
 * @var qApiLog $log
 * @return string
 */

function getLabelClass($log)
{
	$labelClass = '';
	if (isset($log->response_status)) {
		switch ($log->response_status) {
			case 'success':
				$labelClass = 'bg-success';
				break;
			case 'fail':
				$labelClass = 'bg-warning';
				break;
			case 'error':
				$labelClass = 'bg-danger';
				break;
		}
	}
	return $labelClass;
}

?>

<div class="modal-header <?php echo getLabelClass($log); ?>">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php echo Yii::t('qPanel', 'Request Details'); ?>
		<small>
			<?php echo Yii::app()->dateFormatter->formatDateTime($log->request_time, 'full', 'full'); ?>
		</small>
	</h4>
</div>

<div class="modal-body">

	<div style="overflow: auto">
		<h4><?php echo Yii::t('qApi', 'Client'); ?>: </h4>
		<hr>
		<code>Remote: <?php echo $log->client_addr; ?></code><br/>
		<code>API Key: <?php echo $log->client_key; ?></code><br/>
		<code>API Token: <?php echo $log->client_token; ?></code><br/>
		<span class="help-block">&nbsp;</span>
	</div>

	<div style="overflow: auto">
		<h4><?php echo Yii::t('qApi', 'Request'); ?>: </h4>
		<hr/>
		<code><?php echo $log->request_type . ' ' . urldecode($log->request_uri); ?></code>
		<br/><br/>
		<?php foreach (unserialize($log->request_headers) as $index => $value) {
			echo '<code>' . trim($index) . ': ' . trim($value) . '</code></br>';
		} ?>
		<br/>
		<?php if ($log->request_body != null) { ?>
			<pre><?php echo Yii::app()->system->complexDecode($log->request_body); ?></pre>
		<?php } ?>
		<span class="help-block">&nbsp;</span>
	</div>

	<div style="overflow: auto">
		<h4><?php echo Yii::t('qApi', 'Response'); ?>: </h4>
		<hr>
		<code><?php echo $log->response_status . ' ' . $log->response_code; ?></code>
		<br/><br/>
		<?php foreach (unserialize($log->response_headers) as $index => $value) {
			echo '<code>' . trim($index) . ': ' . trim($value) . '</code></br>';
		} ?>
		<br/>
		<?php if ($log->response_body != null) { ?>
			<pre><?php echo Yii::app()->system->complexDecode($log->response_body); ?></pre>
		<?php } ?>
		<span class="help-block">&nbsp;</span>
	</div>

</div>