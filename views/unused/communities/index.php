<?php
/**
 * @var Community $communityModel
 */

$this->widget('qModal', array(
	'htmlOptions' => array(
		'id' => 'userStatsModal',
		'width' => '768px',
		'data-backdrop' => true,
	),
));

$this->widget('qModal', array(
	'htmlOptions' => array(
		'id' => 'userEditModal',
	),
));

$this->renderPartial('/layouts/_defaultPageHeader', array(
	'header' => Yii::t('Community', 'Communities'),
)); ?>

<div id="page-content" class="row">
	<div class="col-xs-12">
		<?php $this->renderPartial('_communitiesPanel', array(
			'communityModel' => $communityModel,
		)); ?>
	</div>
</div>

<script>
	$(document).ready(function () {
		$("#communities-page-size").on("change", function () {
			$.fn.yiiGridView.update("communities", {
				data: {communitiesGridPageSize: $(this).val()}
			});
		});
	});
</script>
