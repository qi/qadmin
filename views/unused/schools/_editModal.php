<?php
/**
 * @var CController $this
 * @var CActiveForm $form
 * @var School $model
 */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'edit-form',
)); ?>

<?php echo $form->hiddenField($model, 'id'); ?>

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">
			<?php echo Yii::t('School', 'Edit school'); ?>
			<small><?php echo $model->name; ?></small>
		</h4>
	</div>

	<div class="modal-body">
		<div class="row">
			<div class="col-md-5 inputContainer">
				<h5><?php echo $form->labelEx($model, 'type'); ?></h5>
				<hr/>
				<div class="switch-toggle btn btn-sm btn-default col-xs-12">
					<?php echo $form->radioButtonList($model, 'type', [
						School::TYPE_CONNECTED => Yii::t('School', 'Connected'),
						School::TYPE_PUBLIC => Yii::t('School', 'Public'),
					], ['separator' => false, 'container' => false])?>
					<a class="bg-warning"></a>
				</div>
				<?php echo CHtml::label('&nbsp;', get_class($model) . '_type', [
					'class' => 'help-block',
				]); ?>
				<?php echo $form->error($model, 'type'); ?>
			</div>
			<div class="col-md-7 inputContainer">
				<h5><?php echo $form->labelEx($model, 'shk_id'); ?></h5>
				<hr/>
				<?php echo $form->textField($model, 'shk_id', [
					'class' => 'form-control input-sm',
				]); ?>
				<?php echo CHtml::label('shkolnaya-karta.ru system ID', get_class($model) . '_shk_id', [
					'class' => 'help-block',
				]); ?>
				<?php echo $form->error($model, 'shk_id'); ?>
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<?php
		$this->widget('qSubmit', array(
			'type' => 'cancel',
			'confirm' => false,
			'htmlOptions' => array(
				'class' => 'pull-left',
				'data-dismiss' => 'modal',
			),
		));
		$this->widget('qSubmit', array(
			'type' => $model->isNewRecord ? 'create' : 'save',
		));
		?>
	</div>

<?php $this->endWidget('edit-form'); ?>