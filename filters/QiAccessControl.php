<?php

/**
 * Class QiAccessControl filters access based on @see CAuthManager.
 * TODO: create access control extension
 * @author chervand <chervand@gmail.com>
 */
class QiAccessControl extends CFilter
{
	public $filterAjax = false;

	/**
	 * Controller instance.
	 * @author andcherv
	 * @var
	 */
	private $_controller;

	/**
	 * Authorization item bizrule params.
	 * @author andcherv
	 * @var array
	 */
	private $_accessParams = array();


	/**
	 * @author andcherv
	 */
	public function init()
	{
		Yii::app()->setComponent('authManager', array(
			'class' => 'CDbAuthManager',
			'itemTable' => 'qi_auth_item',
			'itemChildTable' => 'qi_auth_item_child',
			'assignmentTable' => 'qi_auth_assignment',
			'defaultRoles' => array(
				'guest',
				'user',
				'moderator',
				'admin',
			),
		));
		$this->_controller = Yii::app()->controller;
	}

	/**
	 * Checks the access to the action.
	 * @author andcherv
	 * @param CFilterChain $filterChain
	 * @return bool
	 * @throws CHttpException
	 * @see IAuthManager::checkAccess()
	 */
	protected function preFilter($filterChain)
	{
		$itemName = $this->getAuthItemName();
		if ($this->filterAjax === false && strpos($itemName, 'ajax'))
			return true;
		elseif (!Yii::app()->user->checkAccess($itemName, $this->getAccessParams()))
			throw new CHttpException(403, 'You are not authorized to perform this action.');
		return true;
	}

	/**
	 * Returns authorization item bizrule params array.
	 * @author andcherv
	 * @return mixed
	 * @see IAuthManager::checkAccess()
	 */
	protected function getAccessParams()
	{
		if (method_exists($this->_controller, 'accessParams'))
			$this->_accessParams = $this->_controller->accessParams();
		return $this->_accessParams;
	}

	/**
	 * Returns authorization item name.
	 * @author andcherv
	 * @return string
	 * @see CAuthManager
	 */
	private function getAuthItemName()
	{
		$authItemName = array();
		if (isset($this->_controller->module))
			$authItemName['module'] = $this->_controller->module->id;
		if (isset($this->_controller))
			$authItemName['controller'] = $this->_controller->id;
		if (isset($this->_controller->action))
			$authItemName['action'] = $this->_controller->action->id;
		return implode('/', $authItemName);
	}
}