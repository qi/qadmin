<?php
/**
 * @var qApiLog $logsModel
 */
?>

<?php $this->beginWidget('qPanel', array(
	'id' => 'logs-panel',
	'title' => 'API Logs',
	'collapsible' => true,
	'buttons' => array(
		CHtml::dropDownList('logs-page-size', Yii::app()->user->getState('apiLogPageSize'), array(
			10 => 10, 30 => 30, 50 => 50
		), array(
			'class' => 'form-control input-sm pull-right',
			'style' => 'width: 70px; float: right;',
		))
	),
)); ?>
	<div class="panel-body table-responsive" style="overflow: auto;">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'api-logs',
			'dataProvider' => $logsModel->today()->search(),
			'filter' => $logsModel,
			'itemsCssClass' => 'table table-condensed table-hover text-center',
			'pager' => array(
				'header' => false,
				//'cssFile' => $this->assetsPath . '/css/shk.admin.pager.css'
			),
			'rowCssClassExpression' => function ($row, $data) {
					switch ($data->response_status) {
						case "success":
							$rowClass = "success";
							break;
						case "fail":
							$rowClass = "warning";
							break;
						case "error":
							$rowClass = "danger";
							break;
						default:
							$rowClass = "default";
					}
					return $rowClass;
				},
			'columns' => array(
				array(
					'type' => 'raw',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-1',
					),
					'name' => 'app',
				),
				array(
					'headerHtmlOptions' => array(
						'class' => 'col-xs-1',
					),
					'name' => 'client_addr',
				),
				array(
					'headerHtmlOptions' => array(
						'class' => 'col-xs-1',
					),
					'name' => 'request_time',
					'value' => 'gmdate("H:i:s", $data->request_time)',
				),
				array(
					'headerHtmlOptions' => array(
						'class' => 'col-xs-1',
					),
					'name' => 'response_status',
					'filter' => array(
						'success' => 'success',
						'fail' => 'fail',
						'error' => 'error',
					),
				),
				array(
					'headerHtmlOptions' => array(
						'class' => 'col-xs-1',
					),
					'name' => 'response_code',
				),
				array(
					'headerHtmlOptions' => array(
						'class' => 'col-xs-1',
					),
					'name' => 'request_type',
					'filter' => array(
						'HEAD' => 'HEAD',
						'GET' => 'GET',
						'POST' => 'POST',
						'PUT' => 'PUT',
						'PATCH' => 'PATCH',
						'DELETE' => 'DELETE',
					),
				),
				array(
					'headerHtmlOptions' => array(
						'class' => 'col-xs-6'
					),
					'name' => 'request_uri',
					'value' => 'urldecode($data->request_uri)'
				),
				array(
					'type' => 'raw',
					'value' => 'CHtml::link("<span class=\'glyphicon glyphicon-list-alt\'></span>", null, array(
						"data-toggle" => "modal",
						"data-target" => "#logDetailsModal",
						"data-source" => "/admin/api/ajaxGetLogDetails?id=$data->id",
						"style" => "cursor:pointer",
						"title" => "' . Yii::t('qPanel', 'View Details') . '"
					))',
					'headerHtmlOptions' => array(
						'style' => 'width: 25px; min-width: 25px; max-width: 25px;',
					),
				),
			),
		)); ?>
	</div>
<?php $this->endWidget(); ?>