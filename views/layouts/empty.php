<?php
/**
 * @var $content
 */
?>

<html>
<head>
	<meta charset="UTF-8">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
	<title><?php echo $this->pageTitle; ?></title>
</head>
<body>

<div class="container">
	<?php echo $content; ?>
</div>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>