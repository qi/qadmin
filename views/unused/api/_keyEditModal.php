<?php
/**
 * @author andcherv
 * @var qApiKey $model
 */
?>
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'key-form',
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
		'validateOnType' => true,
		'inputContainer' => '.inputContainer',
		'afterValidate' => 'js:function(form, data, hasErrors){
			if (hasErrors){
				$(form).find(".popover").qP().loading().off();
				$(form).find("input[name=create], input[name=save]").popover("toggle");
				return false;
			} else {
				return true;
			}
		}'
	),
	'focus' => array($model, 'id'),
)); ?>
<?php echo CHtml::activeHiddenField($model, 'id'); ?>
<?php echo CHtml::activeHiddenField($model, 'user_id', array('value' => 1)); ?>

	<!-- Modal header -->
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">
			<?php echo $model->isNewRecord ? Yii::t('qApi', 'New API Key') : Yii::t('qApi', 'Edit API Key'); ?>
			<small><?php echo isset($model->key) ? $model->key : '&nbsp;'; ?></small>
		</h4>
	</div>

	<!-- Modal body -->
	<div class="modal-body">
		<div class="row">
			<div class="col-xs-8 inputContainer">
				<?php
				echo CHtml::tag('h4', array(), $form->labelEx($model, 'key'));
				echo CHtml::tag('hr');
				echo CHtml::activeTextField($model, 'key', array(
					'class' => 'form-control',
					'readonly' => true,
				));
				echo CHtml::label('', 'qApiKey_key', array(
					'class' => 'help-block',
				));
				echo $form->error($model, 'key'); ?>
			</div>
			<div class="col-xs-4 inputContainer">
				<?php
				echo CHtml::tag('h4', array(), $form->labelEx($model, 'secret'));
				echo CHtml::tag('hr');
				echo CHtml::activeTextField($model, 'secret', array(
					'class' => 'form-control',
					'readonly' => true,
				));
				echo CHtml::label('', 'qApiKey_secret', array(
					'class' => 'help-block',
				));
				echo $form->error($model, 'secret'); ?>
			</div>
			<div class="col-xs-8 inputContainer">
				<?php
				echo CHtml::tag('h4', array(), $form->labelEx($model, 'description'));
				echo CHtml::tag('hr');
				echo CHtml::activeTextArea($model, 'description', array(
					'class' => 'form-control',
					'rows' => 1,
				));
				echo CHtml::label('', 'qApiKey_description', array(
					'class' => 'help-block',
				));
				echo $form->error($model, 'content'); ?>
			</div>
			<div class="col-xs-4 inputContainer">
				<?php
				echo CHtml::tag('h4', array(), $form->labelEx($model, 'state'));
				echo CHtml::tag('hr'); ?>
				<div class="btn-group" data-toggle="buttons">
					<?php isset($model->state) && $model->state == qApiKey::STATE_ACTIVE ? $checked = true : $checked = false; ?>
					<label class="btn btn-sm btn-margin <?php echo $checked ? 'active btn-success' : 'btn-default'; ?>">
						<?php echo CHtml::radioButton('qApiKey[state]', $checked, array(
							'value' => qApiKey::STATE_ACTIVE,
						));
						echo Yii::t('qPanel', 'Active'); ?>
					</label>
					<?php isset($model->state) && $model->state == qApiKey::STATE_DISABLED ? $checked = true : $checked = false; ?>
					<label class="btn btn-sm btn-margin <?php echo $checked ? 'active btn-warning' : 'btn-default'; ?>">
						<?php echo CHtml::radioButton('qApiKey[state]', $checked, array(
							'value' => qApiKey::STATE_DISABLED,
						));
						echo Yii::t('qPanel', 'Disabled'); ?>
					</label>
				</div>
				<?php echo CHtml::label('', 'qApiKey_state', array(
					'class' => 'help-block',
				));
				echo $form->error($model, 'state'); ?>
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<?php
		$this->widget('qSubmit', array(
			'type' => 'cancel',
			'confirm' => false,
			'htmlOptions' => array(
				'class' => 'pull-left',
				'data-dismiss' => 'modal',
			),
		));
		if (!$model->isNewRecord) {
			$this->widget('qSubmit', array(
				'type' => 'delete',
			));
		}
		$this->widget('qSubmit', array(
			'type' => $model->isNewRecord ? 'create' : 'save',
		));
		?>
	</div>

<?php $this->endWidget(); ?>