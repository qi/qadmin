<?php

class CliController extends DefaultController
{
	public $layout = 'default';

	public function actionIndex()
	{
		$this->pageTitle = Yii::t('qAdmin', 'Commands');
		$this->addBreadcrumbs(array(
			Yii::t('qAdmin', 'Commands') => Yii::app()->createUrl('admin/cli'),
		));

		$commandModel = new CliCommand('search');
		$commandModel->unsetAttributes();
		if (isset($_GET['CliCommand']))
			$commandModel->attributes = $_GET['CliCommand'];

		$launchModel = new CliLaunch('search');
		$launchModel->unsetAttributes();
		if (isset($_GET['CliLaunch']))
			$launchModel->attributes = $_GET['CliLaunch'];

		$this->render('index', array(
			'commandModel' => $commandModel,
			'launchModel' => $launchModel,
		));
	}

	public function actionAjaxTest()
	{
		$command = CliCommand::model()->findByPk(1);
		$command->addPaths(array(
			Yii::app()->basePath . '/modules/sync/commands'
		));
		echo $command->run();
	}

	public function actionAjaxEditModal($id)
	{
		if (Yii::app()->request->isAjaxRequest) {
			$model = School::model()->with(['metadata', 'city'])->findByPk($id);
			$this->renderPartial('_editModal', [
				'model' => isset($model) ? $model : null,
			], false, true);
			Yii::app()->end();
		} elseif (isset($_POST['School']) && isset($_POST['School']['id'])) {
			$model = School::model()->with(['metadata', 'city'])->findByPk($_POST['School']['id']);
			if (isset($_POST['save'])) {
				$model->setAttributes($_POST['School']);
				if ($model->save()) {
					Yii::app()->panel->setFlash('qPanel-save-success');
				}
			}
			$this->redirect('.');
		}
	}
}