<?php
/**
 * @var UserLocation $location
 */
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php echo Yii::t('User', 'User\'s Location'); ?>
		<small><?php echo isset($location->user) ? $location->user->fullName : '&nbsp;' ?></small>
	</h4>
</div>
<div class="modal-body">
	<?php if (isset($location)) { ?>
	<iframe width="725" height="500" frameborder="0" style="border:0"
	        src="https://www.google.com/maps/embed/v1/place?q=<?php echo urlencode("$location->latitude,$location->longitude"); ?>&key=<?php echo Yii::app()->params['googleAPIKey']; ?>">
	</iframe>
	<?php } else { ?>
		<div class="text-center text-muted">
			No data.
		</div>
	<?php } ?>
</div>