<?php
/**
 * Yii widget for XDSoft DateTimePicker jQUery plugin.
 * Class DateTimePicker
 * @author chervand <chervand@gmail.com>
 * @version 1.0
 * @link http://xdsoft.net/jqplugins/datetimepicker/
 * @license MIT
 * @todo CInputWidget
 */
class DateTimePicker extends CWidget
{
	/**
	 * HTML options for input.
	 * @var array
	 */
	public $htmlOptions = array();
	/**
	 * If set to true, default empty label will be rendered before the input.
	 * If set to string, it will be rendered before the input.
	 * @var bool|string
	 */
	public $label = true;
	public $model = null;
	public $attribute = null;
	/**
	 * XDSoft DateTimePicker jQUery plugin options array.
	 * @link http://xdsoft.net/jqplugins/datetimepicker/
	 * @var array
	 */
	public $options = array();
	/**
	 * Default HTML options.
	 * @var array
	 */
	protected $defaultHtmlOptions = array(
		'id' => 'dateTimePicker',
		'value' => null,
	);
	/**
	 * Default XDSoft DateTimePicker jQuery plugin options.
	 * @var array
	 */
	protected $defaultOptions = array(
		'timepicker' => true,
		'format' => 'd.m.Y H:i:s',
	);

	/**
	 *    Merges options and registers scripts.
	 */
	public function init()
	{
		$this->defaultHtmlOptions['id'] .= '_' . rand();
		$this->defaultHtmlOptions['name'] = $this->defaultHtmlOptions['id'];
		$this->defaultOptions['lang'] = Yii::app()->language;
		$this->htmlOptions = CMap::mergeArray($this->defaultHtmlOptions, $this->htmlOptions);
		$this->options = CMap::mergeArray($this->defaultOptions, $this->options);

		$assetsUrl = CHtml::asset(dirname(__FILE__) . '/assets');

		$cs = Yii::app()->clientScript;
		$cs->registerCssFile($assetsUrl . '/jquery.datetimepicker.css');
		$cs->registerScriptFile($assetsUrl . '/jquery.datetimepicker.js', CClientScript::POS_END);

		$cs->registerScript($this->id, $this->script, CClientScript::POS_LOAD);
	}

	/**
	 * Renders label and input.
	 */
	public function run()
	{
		if (isset($this->label) && is_bool($this->label) && $this->label == true)
			echo CHtml::label('', $this->htmlOptions['id'], array('class' => 'sr-only'));
		elseif (isset($this->label) && is_string($this->label))
			echo $this->label;

		if (isset($this->model) && isset($this->attribute))
			echo CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions);
		else echo CHtml::textField($this->htmlOptions['name'], $this->htmlOptions['value'], $this->htmlOptions);
	}

	/**
	 * Returns jQuery script for the datepicker.
	 * @return string
	 */
	protected function getScript()
	{
		return "$('#" . $this->htmlOptions['id'] . "').datetimepicker(" . CJavaScript::encode($this->options) . ");";
	}
}