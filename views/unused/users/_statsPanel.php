<style>
	.q-dash-panel-title {
		display: inline-block;
		overflow: hidden;
	}
	.progress {
		margin-bottom: 10px;
	}
</style>

<div id="users-stats-panel" class="panel panel-default">
	<div class="panel-body">
		<h1>
			<i class="fa fa-user"></i>
			<small>
				<span class="q-dash-panel-value">&nbsp;</span>
				<span class="q-dash-panel-title">
					<?php echo Yii::t('User', 'Users'); ?>
				</span>
			</small>
		</h1>
		<div class="progress">
			<div class="progress-bar progress-bar-success"></div>
			<div class="progress-bar progress-bar-warning"></div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		var usersStatsPanel = $("#users-stats-panel");
		updateUsersStats(usersStatsPanel);
		setInterval(function () {
			updateUsersStats(usersStatsPanel);
		}, 1000 * 10);
	});
	function updateUsersStats(panel) {
		var value = panel.find(".q-dash-panel-value");
		var progressBar = panel.find(".progress");
		$.get('/admin/users/ajaxUpdateStats', function (response) {
			if (value.html() != response["data"]["total"]) {
				value.slideToggle("fast", function () {
					$(this).html(response["data"]["total"]);
					$(this).slideToggle("fast");
				});
				progressBar.find(".progress-bar-success").prop("title", "Online: " + response["data"]["online"]);
				progressBar.find(".progress-bar-warning").prop("title", "Offline: " + response["data"]["offline"]);
				progressBar.find(".progress-bar-success").width(response["data"]["pctOnline"] + "%");
				progressBar.find(".progress-bar-warning").width(response["data"]["pctOffline"] + "%");
			}
		}, 'json');
	}
</script>