<?php

class m000000_000004_qi_cli extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('qi_cli_command', array(
			'id' => 'pk',
			'script' => 'string not null default \'yiic\'',
			'command' => 'string not null',
			'params' => 'string',
		));
		$this->createTable('qi_cli_launch', array(
			'id' => 'pk',
			'command_id' => 'integer not null',
			'status' => 'string not null',
			'exit_code' => 'integer',
			'output' => 'text',
			'start_time' => 'integer not null',
			'end_time' => 'integer',
			'foreign key (`command_id`) references `qi_cli_command` (`id`) on delete cascade on update cascade',
		));
	}

	public function safeDown()
	{
		$this->dropTable('qi_cli_launch');
		$this->dropTable('qi_cli_command');
	}
}