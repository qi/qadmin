<?php

/**
 * Class EasyPieChart
 *
 * @author andcherv <andcherv@gmail.com>
 *
 * @link https://github.com/rendro/easy-pie-chart
 * @link http://rendro.github.io/easy-pie-chart/
 */
class EasyPieChart extends CWidget
{
	/**
	 * @var string jquery.easypiechart.min.js custom url
	 */
	public $baseScriptUrl;
	/**
	 * @var array HTML options
	 */
	public $htmlOptions = array();
	/**
	 * @var string custom script
	 */
	public $js;
	/**
	 * @var string chart label
	 */
	public $label = '';
	/**
	 * @var array {@link http://rendro.github.io/easy-pie-chart/ options}.
	 */
	public $options = array();
	/**
	 * @var string appended to the percentage value
	 */
	public $valueSign = '%';

	/**
	 * Init widget.
	 */
	public function init()
	{
		$cs = Yii::app()->clientScript;

		if ($this->baseScriptUrl === null)
			$this->baseScriptUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('qi.qadmin.vendors') . '/rendro/easy-pie-chart');
		$cs->registerScriptFile($this->baseScriptUrl . '/dist/jquery.easypiechart.min.js', CClientScript::POS_END);

		if (!isset($this->htmlOptions['id']))
			$this->htmlOptions['id'] = $this->id;
		if (!isset($this->htmlOptions['data-percent']))
			$this->htmlOptions['data-percent'] = 0;

		if (!isset($this->options['size']))
			$this->options['size'] = 110;

		if (!isset($this->options['onStep']))
			$this->options['onStep'] = 'js:function(from,to,value){jQuery(this.el).parent().find("span").text(value.toFixed(1));}';
		if (!isset($this->options['onStep']))
			$this->options['onStop'] = 'js:function(from,to,value){jQuery(this.el).parent().find("span").text(to.toFixed(1));}';
		$cs->registerScript($this->id, $this->getScript(), CClientScript::POS_READY);

		parent::init();
	}

	/**
	 * Run widget.
	 */
	public function run()
	{
		echo CHtml::openTag('div', CMap::mergeArray(array('class' => 'easy-pie-chart'), $this->htmlOptions));
		echo CHtml::tag('div', array('class' => 'value', 'style' => $this->getValueStyle()), '<span></span>' . $this->valueSign);
		echo CHtml::tag('div', array('class' => 'canvas', 'data-percent' => $this->htmlOptions['data-percent']), '');
		echo CHtml::tag('div', array('class' => 'label'), $this->label);
		echo CHtml::closeTag('div');
	}

	protected function getValueStyle()
	{
		return 'line-height: ' . $this->options['size'] . 'px;' . 'font-size: ' . $this->options['size'] / 60 . 'em;';
	}

	/**
	 * Returns complete js string.
	 * @return string
	 */
	protected function getScript()
	{
		$script = 'jQuery("#' . $this->id . '").find(".canvas").easyPieChart(' . CJavaScript::encode($this->options) . ');';
		if (isset($this->js))
			$script .= PHP_EOL . CJavaScript::encode($this->js);
		return $script;
	}
}