<?php

class ApiController extends DefaultController
{
	public $layout = 'default';

	public $defaultAction = 'index';

	public function actionIndex()
	{
		$this->pageTitle = Yii::t('qApi', 'API Dashboard');
		$this->addBreadcrumbs(array(
			Yii::t('qApi', 'RESTful API') => Yii::app()->createUrl('admin/api'),
			Yii::t('qApi', 'Dashboard'),
		));

		$settingsModels = qApiSettings::model()->findAll();

		$keysModel = new qApiKey('search');
		$keysModel->unsetAttributes();
		if (isset($_GET['qApiKey']))
			$keysModel->attributes = $_GET['qApiKey'];

		$logsModel = new qApiLog('search');
		$logsModel->unsetAttributes();
		if (isset($_GET['qApiLog']))
			$logsModel->attributes = $_GET['qApiLog'];

		if (isset($_GET['apiLogPageSize']))
			Yii::app()->user->setState('apiLogPageSize', $_GET['apiLogPageSize']);

		$this->render('dashboard', array(
			'logsModel' => $logsModel,
			'keysModel' => $keysModel,
			'settingsModels' => $settingsModels,
		));
	}

	public function actionDoc()
	{
		$this->pageTitle = Yii::t('qApi', 'RESTful API Documentation');
		$this->addBreadcrumbs(array(
			Yii::t('qApi', 'RESTful API') => Yii::app()->createUrl('admin/api'),
			Yii::t('qApi', 'Documentation'),
		));
		Yii::app()->highlightCode->addHighlighter();
		$this->render('doc');
	}

	public function actionLogs()
	{
		$this->pageTitle = Yii::t('qApi', 'API Logs');
		$this->addBreadcrumbs(array(
			Yii::t('qApi', 'API') => Yii::app()->createUrl('admin/api'),
			Yii::t('qApi', 'API Logs'),
		));

		$logsModel = new qApiLog('search');
		$logsModel->unsetAttributes();
		if (isset($_GET['qApiLog']))
			$logsModel->attributes = $_GET['qApiLog'];

		if (isset($_GET['apiLogPageSize']))
			Yii::app()->user->setState('apiLogPageSize', $_GET['apiLogPageSize']);

		$this->render('logs', array(
			'logsModel' => $logsModel,
		));
	}

	public function actionAjaxUpdateStats()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$criteria = new CDbCriteria();
			$criteria->condition = 't.client_key=:key';

			$criteria->params = array(':key' => '4c8fefe3c8f62a7d4cdb3263299a3a8f8efb2b9a');
			$iosRequestsTotal = qApiLog::model()->today()->count($criteria);

			$iosRequestsSuccess = qApiLog::model()->today()->success()->count($criteria);
			$iosRequestsFail = qApiLog::model()->today()->fail()->count($criteria);
			$iosRequestsError = qApiLog::model()->today()->error()->count($criteria);

			$criteria->params = array(':key' => 'a156ccab52b840482f86158ac60a71ba8746b3df');
			$androidRequestsTotal = qApiLog::model()->today()->count($criteria);
			$androidRequestsSuccess = qApiLog::model()->today()->success()->count($criteria);
			$androidRequestsFail = qApiLog::model()->today()->fail()->count($criteria);
			$androidRequestsError = qApiLog::model()->today()->error()->count($criteria);

			echo CJSON::encode([
				'stutus' => 'success',
				'data' => [
					'ios' => [
						'requestsTotal' => $iosRequestsTotal,
						'pctSuccess' => $iosRequestsTotal > 0 ? round($iosRequestsSuccess * 100 / $iosRequestsTotal, 1) : 0,
						'pctFail' => $iosRequestsTotal > 0 ? round($iosRequestsFail * 100 / $iosRequestsTotal, 1) : 0,
						'pctError' => $iosRequestsTotal > 0 ? round($iosRequestsError * 100 / $iosRequestsTotal, 1) : 0,
					],
					'android' => [
						'requestsTotal' => $androidRequestsTotal,
						'pctSuccess' => $androidRequestsTotal > 0 ? round($androidRequestsSuccess * 100 / $androidRequestsTotal, 1) : 0,
						'pctFail' => $androidRequestsTotal > 0 ? round($androidRequestsFail * 100 / $androidRequestsTotal, 1) : 0,
						'pctError' => $androidRequestsTotal > 0 ? round($androidRequestsError * 100 / $androidRequestsTotal, 1) : 0,
					],
				],
			]);
			
		}
		Yii::app()->end();
	}

	public function actionAjaxKeyEditModal($id = null)
	{
		$model = $this->getModel('qApiKey', $id);
		if (Yii::app()->request->isAjaxRequest) {
			$this->performAjaxValidation(array($model), 'key-form');
			$this->renderPartial('_keyEditModal', array(
				'model' => $model,
			), false, true);
			Yii::app()->end();
		} else {
			Yii::app()->panel->setFlash('shk-system-assets-clean-fail');
			$connection = Yii::app()->db;
			$currentTransaction = $connection->currentTransaction;
			if ($currentTransaction == null)
				$transaction = $connection->beginTransaction();
			try {
				if (isset($_POST['create']) || isset($_POST['save'])) {
					if (isset($_POST[get_class($model)])) {
						$model->attributes = $_POST[get_class($model)];
						if ($model->save()) {
							if (isset($transaction) && $currentTransaction == null) {
								$transaction->commit();
								Yii::app()->panel->setFlash('qPanel-save-success');
							}
						} else throw new CDbException('Can\'t save ' . get_class($model) . ' model.');
					}
				} elseif (isset($_POST['delete'])) {
					if ($model->delete()) {
						if (isset($transaction) && $currentTransaction == null) {
							$transaction->commit();
							Yii::app()->panel->setFlash('qPanel-delete-success');
						}
					} else throw new CDbException('Can\'t delete ' . get_class($model) . ' model.');
				}
			} catch (Exception $exception) {
				if (isset($transaction) && $currentTransaction == null)
					$transaction->rollback();
				Yii::app()->panel->setFlash('qPanel-transaction-fail', $exception->getMessage());
				if (isset($helpModel) && $helpModel->hasErrors()) {
					foreach ($helpModel->errors as $key => $value) {
						Yii::app()->user->setFlash($key, CJSON::encode(array(
							'title' => Yii::t('qPanel', 'Error'),
							'text' => $value,
							'type' => 'error',
						)));
					}
				}
			}
			$this->redirect('index');
		}
	}

	public function actionAuthenticity()
	{
		$panelSchoolId = Yii::app()->shkPanel->school;
		Yii::app()->shkPanel->setFilters(array('school'));

		if (Yii::app()->request->getQuery('api_key') && Yii::app()->request->getQuery('api_key') != 'new') {
			Yii::app()->clientScript->registerScript('enableSave', 'shk.enableSave();');
			$key = Yii::app()->request->getQuery('api_key');
			$this->addBreadcrumbs(array(
				Yii::t('admin', 'API') => Yii::app()->homeUrl . 'admin/api',
				Yii::t('admin', 'Authenticity') => Yii::app()->homeUrl . 'admin/api/authenticity',
				Yii::t('admin', 'API key'),
			));
			$criteria = new CDbCriteria();
			$criteria->addCondition("api_key='$key'");
			$model = ShkApiAuthenticity::model()->find($criteria); //handle 403
			if (Yii::app()->request->getPost('delete')) {
				$model->delete();
				$this->redirect(Yii::app()->createUrl('/admin/api/authenticity/'));
			} elseif (Yii::app()->request->getPost('ShkApiAuthenticity')) {
				$model->attributes = Yii::app()->request->getPost('ShkApiAuthenticity');
				$model->save();
				$this->redirect(Yii::app()->createUrl('/admin/api/authenticity/'));
			}
			$this->render('key', array(
				'model' => $model,
			));
		} elseif (Yii::app()->request->getQuery('api_key') && Yii::app()->request->getQuery('api_key') == 'new') {
			$this->addBreadcrumbs(array(
				Yii::t('admin', 'API') => Yii::app()->homeUrl . 'admin/api',
				Yii::t('admin', 'Authenticity') => Yii::app()->homeUrl . 'admin/api/authenticity',
				Yii::t('admin', 'New API key'),
			));
			$key = new ShkApiAuthenticity();
			if ($panelSchoolId) {
				Yii::app()->clientScript->registerScript('enableSave', 'shk.enableSave();');
				$criteria = new CDbCriteria();
				$criteria->addCondition("id_school=$panelSchoolId");
				$users = Member::model()->findAll($criteria);
			} else {
				Yii::app()->clientScript->registerScript('disableSave', 'shk.disableSave();');
				Yii::app()->user->setFlash('shk-admin-api-settings-saved', CJSON::encode(array(
					'title' => Yii::t('admin', 'Выберите школу'),
					'text' => Yii::t('admin', 'Для продолжения необходимо выбрать школу в выпадающем списке главной панели.'),
					'hide' => false,
				)));
			}
			if (Yii::app()->request->getPost('ShkApiAuthenticity')) {
				$key->attributes = Yii::app()->request->getPost('ShkApiAuthenticity');
				if ($key->save()) {
					$this->redirect($this->createUrl('/admin/api/authenticity', array(
						'api_key' => $key->api_key,
					)));
				}
			}
			$this->render('key', array(
				'model' => $key,
				'users' => isset($users) ? $users : null,
			));
		} else {
			$this->addBreadcrumbs(array(
				Yii::t('admin', 'API') => Yii::app()->homeUrl . 'admin/api',
				Yii::t('admin', 'Authenticity'),
			));
			$keys = new ShkApiAuthenticity('search');
			$keys->unsetAttributes();
			if (isset($_GET['ShkApiAuthenticity'])) {
				$keys->attributes = $_GET['ShkApiAuthenticity'];
			}
			if ($this->module->panelSchoolId) {
				$keys->getDbCriteria()->mergeWith(array(
					'with' => array(
						'user' => array(
							'condition' => 'id_school=:schoolId',
							'params' => array(
								':schoolId' => $this->module->panelSchoolId
							),
							'joinType' => 'INNER JOIN'
						),
					),
				));
			} else {
				$keys->getDbCriteria()->mergeWith(array(
					'with' => array(
						'user' => array(
							'joinType' => 'INNER JOIN'
						),
					),
				));
			}
			$this->render('authenticity', array(
				'keys' => $keys,
			));
		}
	}

	public function actionAjaxActivateKey()
	{
		$this->changeKeyStatus('activate');
	}

	public function actionAjaxDeactivateKey()
	{
		$this->changeKeyStatus('deactivate');
	}

	/**
	 * Renders API settings form. Saves settings if $_POST data set, and sets notifications.
	 * @author andcherv
	 */
	public function actionSettings()
	{
		$settings = ShkApiSettings::model()->findAll();
		if (Yii::app()->request->getPost('ShkApiSettings') && is_array(Yii::app()->request->getPost('ShkApiSettings'))) {
			$postData = Yii::app()->request->getPost('ShkApiSettings');
			foreach ($postData as $index => $value) {
				$index = explode(':', $index);
				foreach ($settings as $setting) {
					if ($setting->type == $index[0] && $setting->attribute == $index[1]) {
						$setting->value = $value;
						if ($setting->save()) {
							Yii::app()->user->setFlash('shk-admin-api-settings-saved', CJSON::encode(array(
								'title' => Yii::t('admin', 'Settings saved'),
								'text' => Yii::t('admin', 'API settings saved'),
								'type' => 'success',
							)));
						} else {
							Yii::app()->user->setFlash('shk-admin-api-settings-failed', CJSON::encode(array(
								'title' => Yii::t('admin', 'Error'),
								'text' => Yii::t('admin', 'API settings save failed'),
								'type' => 'error',
							)));
						}
					}
				}

			}
		}
		$this->render('settings', array(
			'settings' => $settings,
		));
	}

	public function actionAjaxGetLogDetails($id)
	{
		if (Yii::app()->request->isAjaxRequest) {
			Yii::app()->highlightCode->addHighlighter();
			$log = qApiLog::model()->findByPk($id);
			$this->renderPartial('_logDetailsModal', array(
				'log' => $log,
			), false, false);
		}
		Yii::app()->end();
	}

	public function actionAjaxSettingsEditModal()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$settings = qApiSettings::model()->findAll();
			$settingsGrouped = array();
			foreach ($settings as $settingsModel) {
				if (!isset($settingsGrouped[$settingsModel->type]))
					$settingsGrouped[$settingsModel->type] = array();
				array_push($settingsGrouped[$settingsModel->type], $settingsModel);
			}
			$this->renderPartial('_settingsEditModal', array(
				'settings' => $settingsGrouped,
			), false, true);
		} else {
			if (isset($_POST['save']) && isset($_POST['qApiSettings'])) {
				foreach ($_POST['qApiSettings'] as $attribute => $attributes) {
					// @todo notifications/transaction
					$model = qApiSettings::model()->find(array(
						'condition' => 't.attribute=:attribute',
						'params' => array(':attribute' => $attribute),
					));
					if (isset($model)) {
						$model->attributes = $attributes;
						$model->save();
					}
				}
			}
			$this->redirect('index');
		}
	}

	private function changeKeyStatus($status)
	{
		if (isset($_POST['api_key']) && !empty($_POST['api_key'])) {
			$key = ShkApiAuthenticity::model()->find(array(
				'condition' => 'api_key=:apiKey',
				'params' => array(
					':apiKey' => $_POST['api_key'],
				),
			));
			if ($key != null) {
				if ($status == 'activate') {
					$key->activate();
				} elseif ($status == 'deactivate') {
					$key->deactivate();
				} else {
					throw new CHttpException(400, Yii::t('admin', 'Wrong status value.'));
				}
				if (!$key->save()) {
					throw new CHttpException(500, Yii::t('admin', 'Can not save API key.'));
				}
			} else {
				throw new CHttpException(404, Yii::t('admin', 'Can not find API key.'));
			}
		} else {
			throw new CHttpException(400, Yii::t('admin', 'API key value not set.'));
		}
		Yii::app()->end();
	}

	public function actionAjaxGetActivityData($length)
	{
		if (Yii::app()->request->isAjaxRequest) {
			$data = array();
			$time = time();
			$criteria = new CDbCriteria();
			$criteria->addBetweenCondition('t.request_time', $time - $length, $time);
			$logs = qApiLog::model()->findAll($criteria);

			$counts = array();
			foreach ($logs as $log) {
				$counts[(int)$log->request_time][] = $log->client_addr;
			}
			for ($i = 0; $i < $length; $i++) {
				$data[$time - $length + $i] = isset($counts[$time - $length + $i]) ? count($counts[$time - $length + $i]) : 0;
			}

			echo CJSON::encode(array(
				'status' => 'success',
				'data' => $data,
			));
			Yii::app()->end();
		}
	}
}