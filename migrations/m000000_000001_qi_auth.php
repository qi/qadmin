<?php

class m000000_000001_qi_auth extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('qi_auth_item', array(
			'name' => 'varchar(64) not null',
			'type' => 'integer not null',
			'description' => 'text',
			'bizrule' => 'text',
			'data' => 'text',
			'primary key (`name`)',
		));
		$this->createTable('qi_auth_item_child', array(
			'parent' => 'varchar(64) not null',
			'child' => 'varchar(64) not null',
			'primary key (`parent`,`child`)',
			'foreign key (`parent`) references `qi_auth_item` (`name`) on delete cascade on update cascade',
			'foreign key (`child`) references `qi_auth_item` (`name`) on delete cascade on update cascade',
		));
		$this->createTable('qi_auth_assignment', array(
			'itemname' => 'varchar(64) not null',
			'userid' => 'varchar(64) not null',
			'bizrule' => 'text',
			'data' => 'text',
			'primary key (`itemname`,`userid`)',
			'foreign key (`itemname`) references `qi_auth_item` (`name`) on delete cascade on update cascade',
		));

		$this->insert('qi_auth_item', array(
			'name' => 'admin',
			'type' => CAuthItem::TYPE_ROLE,
			'description' => 'System administrator',
			'bizrule' => 'return Yii::app()->user->isAdmin;',
			'data' => 'N;',
		));
		$this->insert('qi_auth_item', array(
			'name' => 'moderator',
			'type' => CAuthItem::TYPE_ROLE,
			'description' => 'Backend moderator',
			'bizrule' => 'return Yii::app()->user->isModerator;',
			'data' => 'N;',
		));
		$this->insert('qi_auth_item', array(
			'name' => 'user',
			'type' => CAuthItem::TYPE_ROLE,
			'description' => 'Authenticated user',
			'bizrule' => 'return !Yii::app()->user->isGuest;',
			'data' => 'N;',
		));
		$this->insert('qi_auth_item', array(
			'name' => 'guest',
			'type' => CAuthItem::TYPE_ROLE,
			'description' => 'Guest user',
			'bizrule' => 'return Yii::app()->user->isGuest;',
			'data' => 'N;',
		));

		$this->insert('qi_auth_item', array('type' => CAuthItem::TYPE_TASK, 'name' => ''));
		$this->insert('qi_auth_item', array('type' => CAuthItem::TYPE_TASK, 'name' => 'dashboard'));
		$this->insert('qi_auth_item', array('type' => CAuthItem::TYPE_TASK, 'name' => 'dashboard/default'));
		$this->insert('qi_auth_item', array('type' => CAuthItem::TYPE_TASK, 'name' => 'dashboard/default/index'));
		$this->insert('qi_auth_item', array('type' => CAuthItem::TYPE_TASK, 'name' => 'dashboard/default/login'));
		$this->insert('qi_auth_item', array('type' => CAuthItem::TYPE_TASK, 'name' => 'dashboard/default/logout'));
		$this->insert('qi_auth_item', array('type' => CAuthItem::TYPE_TASK, 'name' => 'dashboard/default/error'));

		$this->insert('qi_auth_item_child', array('parent' => 'guest', 'child' => 'dashboard/default/login'));
		$this->insert('qi_auth_item_child', array('parent' => 'guest', 'child' => 'dashboard/default/logout'));
		$this->insert('qi_auth_item_child', array('parent' => 'guest', 'child' => 'dashboard/default/error'));

		$this->insert('qi_auth_item_child', array('parent' => 'user', 'child' => ''));
		$this->insert('qi_auth_item_child', array('parent' => 'user', 'child' => 'dashboard'));
		$this->insert('qi_auth_item_child', array('parent' => 'user', 'child' => 'dashboard/default'));
		$this->insert('qi_auth_item_child', array('parent' => 'user', 'child' => 'dashboard/default/index'));
		$this->insert('qi_auth_item_child', array('parent' => 'user', 'child' => 'dashboard/default/login'));
		$this->insert('qi_auth_item_child', array('parent' => 'user', 'child' => 'dashboard/default/logout'));
		$this->insert('qi_auth_item_child', array('parent' => 'user', 'child' => 'dashboard/default/error'));

		$this->insert('qi_auth_item_child', array('parent' => 'moderator', 'child' => ''));
		$this->insert('qi_auth_item_child', array('parent' => 'moderator', 'child' => 'dashboard'));
		$this->insert('qi_auth_item_child', array('parent' => 'moderator', 'child' => 'dashboard/default'));
		$this->insert('qi_auth_item_child', array('parent' => 'moderator', 'child' => 'dashboard/default/index'));
		$this->insert('qi_auth_item_child', array('parent' => 'moderator', 'child' => 'dashboard/default/login'));
		$this->insert('qi_auth_item_child', array('parent' => 'moderator', 'child' => 'dashboard/default/logout'));
		$this->insert('qi_auth_item_child', array('parent' => 'moderator', 'child' => 'dashboard/default/error'));
		
		$this->insert('qi_auth_item_child', array('parent' => 'admin', 'child' => ''));
		$this->insert('qi_auth_item_child', array('parent' => 'admin', 'child' => 'dashboard'));
		$this->insert('qi_auth_item_child', array('parent' => 'admin', 'child' => 'dashboard/default'));
		$this->insert('qi_auth_item_child', array('parent' => 'admin', 'child' => 'dashboard/default/index'));
		$this->insert('qi_auth_item_child', array('parent' => 'admin', 'child' => 'dashboard/default/login'));
		$this->insert('qi_auth_item_child', array('parent' => 'admin', 'child' => 'dashboard/default/logout'));
		$this->insert('qi_auth_item_child', array('parent' => 'admin', 'child' => 'dashboard/default/error'));

		$this->insert('qi_auth_item', array('type' => CAuthItem::TYPE_TASK, 'name' => 'dashboard/settings/help'));
		$this->insert('qi_auth_item', array('type' => CAuthItem::TYPE_TASK, 'name' => 'dashboard/settings/maintenance'));

		$this->insert('qi_auth_item_child', array('parent' => 'admin', 'child' => 'dashboard/settings/help'));
		$this->insert('qi_auth_item_child', array('parent' => 'admin', 'child' => 'dashboard/settings/maintenance'));
	}

	public function safeDown()
	{
		$this->delete('qi_auth_item', 'name=\'\'');
		$this->delete('qi_auth_item', 'name=\'dashboard/default/index\'');
		$this->delete('qi_auth_item', 'name=\'dashboard/default/login\'');
		$this->delete('qi_auth_item', 'name=\'dashboard/default/logout\'');
		$this->delete('qi_auth_item', 'name=\'dashboard/default/error\'');
		$this->delete('qi_auth_item', 'name=\'guest\'');
		$this->delete('qi_auth_item', 'name=\'user\'');
		$this->delete('qi_auth_item', 'name=\'moderator\'');
		$this->delete('qi_auth_item', 'name=\'admin\'');
		$this->dropTable('qi_auth_assignment');
		$this->dropTable('qi_auth_item_child');
		$this->dropTable('qi_auth_item');
	}
}