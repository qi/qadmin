<?php
/**
 * Discounts managment page.
 * @author andcherv
 * @var ShkServiceDiscount $discountModel
 */
?>

<!-- Page Header -->
<?php $this->renderPartial('//layouts/_adminPageHeader', array(
	'header' => Yii::t('ShkService', 'Discounts'),
	'small' => Yii::app()->shkPanel->filterString,
)); ?>

<!-- Buttons Bar -->
<div id="btnBar" class="row">
	<?php
	$school = Yii::app()->shkPanel->getSchool(true);
	$period = Yii::app()->shkPanel->getPeriod();
	echo CHtml::link('<span class="glyphicon glyphicon-plus"></span>' . Yii::t('qPanel', 'Add'), null, array(
		'class' => 'btn btn-sm btn-default',
		'disabled' => !isset($school) || !isset ($period) || !$school->serviceSettings->isBillingActive,
		'data-toggle' => 'modal',
		'data-target' => '#discountEditModal',
		'data-source' => '/admin/service/discountEditModal',
		'title' => Yii::t('ShkService', 'Add a discount'),
	));
	?>
</div>

<!-- Benefit Edit Modal -->
<?php $this->widget('qPanel.widgets.qModal', array(
	'htmlOptions' => array(
		'id' => 'discountEditModal',
		'width' => '768px',
	),
)); ?>

<!-- Discounts Grid View -->
<div id="table" class="row table-responsive">
	<?php if (isset($discountModel) && is_object($discountModel)) {
		$this->renderPartial('_discountsGridView', array(
			'discountModel' => $discountModel,
		));
	} ?>
</div>