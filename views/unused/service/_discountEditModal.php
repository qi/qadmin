<?php
/**
 * @author andcherv
 * @var ShkServiceDiscount $discountModel
 */
if (!isset($discountModel) || !is_object($discountModel)) {
	$this->renderPartial('//layouts/_adminModalError');
} else {
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'discountEditForm',
		'focus' => array($discountModel, 'title'),
		'enableAjaxValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'validateOnType' => true,
			'inputContainer' => '.inputContainer',
			'afterValidate' => 'js:function(form, data, hasErrors){
				if (hasErrors){
					$(form).find(".popover").qP().loading().off();
					$(form).find("input[name=create], input[name=save]").popover("toggle");
					return false;
				} else {
					return true;
				}
			}'
		),
	));
	echo $form->hiddenField($discountModel, 'id');
	echo $form->hiddenField($discountModel, 'period_id');
	?>
	<!-- Modal Header -->
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">
			<?php echo isset($discountModel->title) ? $discountModel->title : Yii::t('ShkService', 'New Discount'); ?>
			<small><?php echo isset($discountModel->period) ? $discountModel->period->getRange() : '&nbsp;'; ?></small>
		</h4>
	</div>
	<!-- Modal body -->
	<div class="modal-body">
		<div class="row">
			<div class="col-xs-12 inputContainer">
				<h4><?php echo $form->labelEx($discountModel, 'title'); ?></h4>
				<hr/>
				<?php echo $form->textField($discountModel, 'title', array(
					'class' => 'form-control input-sm',
					'placeholder' => isset($discountModel->title) ? $discountModel->title : '',
				)); ?>
				<?php echo $form->label($discountModel, 'title', array(
					'class' => 'help-block',
				)); ?>
				<?php echo $form->error($discountModel, 'title'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4 inputContainer">
				<h4><?php echo $form->labelEx($discountModel, 'amount_default'); ?></h4>
				<hr/>
				<?php echo $form->textField($discountModel, 'amount_default', array(
					'class' => 'form-control input-sm',
					'placeholder' => isset($discountModel->amount_default) ? $discountModel->amount_default : '',
					'disabled' => $discountModel->amount_type == ShkServiceDiscount::AMOUNT_TYPE_DAILY,
				)); ?>
				<?php echo $form->label($discountModel, 'amount_default', array(
					'class' => 'help-block',
				)); ?>
				<?php echo $form->error($discountModel, 'amount_default'); ?>
			</div>
			<div class="col-xs-4 inputContainer">
				<h4><?php echo $form->labelEx($discountModel, 'amount_type'); ?></h4>
				<hr/>
				<div id="discountAmountTypeButtons" class="btn-group" data-toggle="buttons">
					<?php $discountModel->amount_type == ShkServiceDiscount::AMOUNT_TYPE_VALUE ? $checked = true : $checked = false; ?>
					<label class="btn btn-sm btn-margin <?php echo $checked ? 'active btn-warning' : 'btn-default'; ?>">
						<?php echo CHtml::radioButton('ShkServiceDiscount[amount_type]',
							$discountModel->amount_type == ShkServiceDiscount::AMOUNT_TYPE_VALUE, array(
								'value' => ShkServiceDiscount::AMOUNT_TYPE_VALUE,
							));
						echo Yii::t('ShkService', '$'); ?>
					</label>
					<?php $discountModel->amount_type == ShkServiceDiscount::AMOUNT_TYPE_PERCENT ? $checked = true : $checked = false; ?>
					<label class="btn btn-sm btn-margin <?php echo $checked ? 'active btn-warning' : 'btn-default'; ?>">
						<?php echo CHtml::radioButton('ShkServiceDiscount[amount_type]', $checked, array(
							'value' => ShkServiceDiscount::AMOUNT_TYPE_PERCENT,
						));
						echo Yii::t('ShkService', '%'); ?>
					</label>
					<?php $discountModel->amount_type == ShkServiceDiscount::AMOUNT_TYPE_DAILY ? $checked = true : $checked = false; ?>
					<label class="btn btn-sm btn-margin <?php echo $checked ? 'active btn-warning' : 'btn-default'; ?>">
						<?php echo CHtml::radioButton('ShkServiceDiscount[amount_type]', $checked, array(
							'value' => ShkServiceDiscount::AMOUNT_TYPE_DAILY,
						));
						echo Yii::t('ShkService', 'Daily'); ?>
					</label>
				</div>
				<?php echo CHtml::label(Yii::t('ShkService', 'Daily: percentile discount for past days of a payment period.'),
					get_class($discountModel) . '_amount_type', array(
						'class' => 'help-block'
					)); ?>
				<?php echo $form->error($discountModel, 'amount_type'); ?>
			</div>
			<div class="col-xs-4 inputContainer">
				<h4><?php echo $form->labelEx($discountModel, 'status'); ?></h4>
				<hr/>
				<div class="btn-group" data-toggle="buttons">
					<?php isset($discountModel->status) && $discountModel->status == 1 ? $checked = true : $checked = false; ?>
					<label class="btn btn-sm btn-margin <?php echo $checked ? 'active btn-success' : 'btn-default'; ?>">
						<?php echo CHtml::radioButton('ShkServiceDiscount[status]', $checked, array(
							'value' => ShkServiceDiscount::STATUS_ACTIVE,
						));
						echo Yii::t('ShkService', 'Active'); ?>
					</label>
					<?php isset($discountModel->status) && $discountModel->status == 0 ? $checked = true : $checked = false; ?>
					<label class="btn btn-sm btn-margin <?php echo $checked ? 'active btn-warning' : 'btn-default'; ?>">
						<?php echo CHtml::radioButton('ShkServiceDiscount[status]', $checked, array(
							'value' => ShkServiceDiscount::STATUS_DISABLED,
						));
						echo Yii::t('ShkService', 'Disabled'); ?>
					</label>
				</div>
				<?php echo $form->label($discountModel, 'status', array(
					'class' => 'help-block',
				)); ?>
				<?php echo $form->error($discountModel, 'status'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4 inputContainer">
				<h4><?php echo $form->labelEx($discountModel, 'start_time'); ?></h4>
				<hr/>
				<?php
				$this->widget('qPanel.extensions.datetimepicker.DateTimePicker', array(
					'label' => false,
					'model' => $discountModel,
					'attribute' => 'start_time',
					'options' => array(
						'timepicker' => false,
						'format' => 'd.m.Y',
						'formatDate' => 'd.m.Y',
						'minDate' => isset($discountModel->create_time) ? date('d.m.Y', $discountModel->create_time) : date('d.m.Y', time()),
						'maxDate' => isset($discountModel->period) ? date('d.m.Y', $discountModel->period->acceptanceOfBill) : false,
					),
					'htmlOptions' => array(
						'id' => 'ShkServiceDiscount_start_time',
						'name' => 'ShkServiceDiscount[start_time]',
						'class' => 'form-control input-sm',
						'value' => isset($discountModel) ? date('d.m.Y', $discountModel->start_time) : null,
						'placeholder' => isset($discountModel) ? date('d.m.Y', $discountModel->start_time) : null
					),
				));
				?>
				<?php echo CHtml::label(Yii::t('ShkService', 'A discount can be applied for a bill created starting this date.'),
					get_class($discountModel) . '_start_time', array(
						'class' => 'help-block'
					)); ?>
				<?php echo $form->error($discountModel, 'start_time'); ?>
			</div>
			<div class="col-xs-4 inputContainer">
				<h4><?php echo $form->labelEx($discountModel, 'end_time'); ?></h4>
				<hr/>
				<?php
				$this->widget('qPanel.extensions.datetimepicker.DateTimePicker', array(
					'label' => false,
					'model' => $discountModel,
					'attribute' => 'end_time',
					'options' => array(
						'timepicker' => false,
						'format' => 'd.m.Y',
						'formatDate' => 'd.m.Y',
						'minDate' => isset($discountModel->create_time) ? date('d.m.Y', $discountModel->create_time) : date('d.m.Y', time()),
						'maxDate' => isset($discountModel->period) ? date('d.m.Y', $discountModel->period->acceptanceOfBill) : false,
					),
					'htmlOptions' => array(
						'id' => 'ShkServiceDiscount_end_time',
						'name' => 'ShkServiceDiscount[end_time]',
						'class' => 'form-control input-sm',
						'value' => isset($discountModel) ? date('d.m.Y', $discountModel->end_time) : null,
						'placeholder' => isset($discountModel) ? date('d.m.Y', $discountModel->end_time) : null
					),
				));
				?>
				<?php echo CHtml::label(Yii::t('ShkService', 'A discount can be applied for a bill created before this date.'),
					get_class($discountModel) . '_end_time', array(
						'class' => 'help-block'
					)); ?>
				<?php echo $form->error($discountModel, 'end_time'); ?>
			</div>
			<div class="col-xs-4 inputContainer">
				<h4><?php echo $form->labelEx($discountModel, 'apply_type'); ?></h4>
				<hr/>
				<div class="btn-group" data-toggle="buttons">
					<?php $discountModel->apply_type == ShkServiceDiscount::APPLY_AUTO ? $checked = true : $checked = false; ?>
					<label class="btn btn-sm btn-margin <?php echo $checked ? 'active btn-info' : 'btn-default'; ?>">
						<?php echo CHtml::radioButton('ShkServiceDiscount[apply_type]', $checked, array(
							'value' => ShkServiceDiscount::APPLY_AUTO,
						));
						echo Yii::t('ShkService', 'Automatically'); ?>
					</label>
					<?php $discountModel->apply_type == ShkServiceDiscount::APPLY_USER ? $checked = true : $checked = false; ?>
					<label class="btn btn-sm btn-margin disabled <?php echo $checked ? 'active btn-info' : 'btn-default'; ?>">
						<?php echo CHtml::radioButton('ShkServiceDiscount[apply_type]', $checked, array(
							'value' => ShkServiceDiscount::APPLY_USER,
						));
						echo Yii::t('ShkService', 'Manually') . ' **'; ?>
					</label>
				</div>
				<?php echo CHtml::label(Yii::t('ShkService', 'A discount will be applied automatically for an every bill or manually by a user.'),
					get_class($discountModel) . '_apply_type', array(
						'class' => 'help-block'
					)); ?>
				<?php echo $form->error($discountModel, 'apply_type'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 inputContainer">
				<h4><?php echo $form->labelEx($discountModel, 'description'); ?></h4>
				<hr/>
				<?php echo $form->textArea($discountModel, 'description', array(
					'class' => 'form-control input-sm',
					'placeholder' => isset($discountModel->description) ? $discountModel->description : '',
					'rows' => 3,
				)); ?>
				<?php echo $form->label($discountModel, 'description', array(
					'class' => 'help-block',
				)); ?>
				<?php echo $form->error($discountModel, 'description'); ?>
			</div>
		</div>
		<span class="help-block">* Поля, обязательные для заполнения</span>
		<span class="help-block">** <?php echo Yii::t('qPanel', 'Currently unavailable')?></span>
	</div>
	<!-- Modal Footer -->
	<div class="modal-footer">
		<?php
		$this->widget('admin.widgets.qSubmit', array(
			'type' => 'cancel',
			'confirm' => false,
			'htmlOptions' => array(
				'class' => 'pull-left',
				'data-dismiss' => 'modal',
			),
		));
		$this->widget('admin.widgets.qSubmit', array(
			'type' => $discountModel->isNewRecord ? 'create' : 'save',
			'htmlOptions' => array(
				'class' => 'pull-right',
			),
		));
		if ($discountModel->isNewRecord == false) {
			$this->widget('admin.widgets.qSubmit', array(
				'type' => 'delete',
				'htmlOptions' => array(
					'class' => 'pull-right',
				),
			));
		}
		?>
	</div>
	<?php $this->endWidget();
} ?>
<script>
	$("#discountAmountTypeButtons").on("click", ".btn", function () {
		var val = $(this).find("input[type=radio]").val();
		if (val && val == 3) {
			$("#ShkServiceDiscount_amount_default").prop("disabled", true);
		} else {
			$("#ShkServiceDiscount_amount_default").prop("disabled", false);
		}
	});
</script>