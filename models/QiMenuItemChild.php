<?php

/**
 * This is the model class for table "qi_menu_item_child".
 *
 * The followings are the available columns in table 'qi_menu_item_child':
 * @property integer $menu_id
 * @property string $parent_name
 * @property string $child_name
 *
 * The followings are the available model relations:
 * @property QiMenu $menu
 * @property QiMenuItem $parentItem
 * @property QiMenuItem $childItem
 */
class QiMenuItemChild extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'qi_menu_item_child';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('menu_id, parent_name, child_name', 'required'),
			array('menu_id', 'numerical', 'integerOnly'=>true),
			array('parent_name, child_name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('menu_id, parent_name, child_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'menu' => array(self::BELONGS_TO, 'QiMenu', 'menu_id'),
			'parentItem' => array(self::BELONGS_TO, 'QiMenuItem', 'parent_name'),
			'childItem' => array(self::BELONGS_TO, 'QiMenuItem', 'child_name'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'menu_id' => 'Menu',
			'parent_name' => 'Parent Name',
			'child_name' => 'Child Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('menu_id',$this->menu_id);
		$criteria->compare('parent_name',$this->parent_name,true);
		$criteria->compare('child_name',$this->child_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QiMenuItemChild the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
