<?php

class SchoolsController extends DefaultController
{
	public $layout = 'default';

	public function actionIndex()
	{
		$this->pageTitle = Yii::t('School', 'Schools');
		$this->addBreadcrumbs(array(
			Yii::t('School', 'Schools') => Yii::app()->createUrl('admin/schools'),
		));

		$schoolModel = new School('search');
		$schoolModel->unsetAttributes();
		if (isset($_GET['School']))
			$schoolModel->attributes = $_GET['School'];

		$this->render('index', array(
			'schoolModel' => $schoolModel,
		));
	}

	public function actionAjaxEditModal($id)
	{
		if (Yii::app()->request->isAjaxRequest) {
			$model = School::model()->with(['metadata', 'city'])->findByPk($id);
			$this->renderPartial('_editModal', [
				'model' => isset($model) ? $model : null,
			], false, true);
			Yii::app()->end();
		} elseif (isset($_POST['School']) && isset($_POST['School']['id'])) {
			$model = School::model()->with(['metadata', 'city'])->findByPk($_POST['School']['id']);
			if (isset($_POST['save'])) {
				$model->setAttributes($_POST['School']);
				if ($model->save()) {
					Yii::app()->panel->setFlash('qPanel-save-success');
				}
			}
			$this->redirect('.');
		}
	}

	public function actionAjaxUpdateStats()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$totalCount = School::model()->with('metadata:active')->count();
			$publicCount = School::model()->with('metadata:active')->count(array(
				'condition' => 't.type=:public',
				'params' => array(':public' => School::TYPE_PUBLIC),
			));
			$connectedCount = School::model()->with('metadata:active')->count(array(
				'condition' => 't.type=:public',
				'params' => array(':public' => School::TYPE_CONNECTED),
			));
			echo CJSON::encode([
				'status' => 'success',
				'data' => [
					'total' => $totalCount,
					'public' => $publicCount,
					'connected' => $connectedCount,
					'pctPublic' => $totalCount > 0 ? round($publicCount * 100 / $totalCount, 1) : 0,
					'pctConnected' => $totalCount > 0 ? round($connectedCount * 100 / $totalCount, 1) : 0,
				],
			]);
		}
		Yii::app()->end();
	}
}