<?php

/**
 * Bootstrap modal with AJAX loading content.
 * The trigger link\button should have data-toggle='modal', data-target='#{modalId}', data-source='{actionUrl}' attributes,
 * @author andcherv
 */
class QiModal extends CWidget
{
	public $htmlOptions = array();
	public $reloadOnClose = false;

	public function init()
	{
		if (!isset($this->htmlOptions['id']) || empty($this->htmlOptions['id']))
			$this->htmlOptions['id'] = $this->id;
		if (!isset($this->htmlOptions['title']) || empty($this->htmlOptions['title']))
			$this->htmlOptions['title'] = 'Modal';
		if (!isset($this->htmlOptions['width']) || empty($this->htmlOptions['width']))
			$this->htmlOptions['width'] = '600px';
		if (!isset($this->htmlOptions['data-backdrop']) || empty($this->htmlOptions['data-backdrop']))
			$this->htmlOptions['data-backdrop'] = 'static';

		$js = '$(document).on("click", "#container [data-toggle=modal]",function(){';
		$js .= '$(document).qP().modal().load($(this).data("target"),$(this).data("source"));';
		if ($this->reloadOnClose === true)
			$js .= '$($(this).data("target")).on("hidden.bs.modal",function(){location.reload();});';
		$js .= '});';

		Yii::app()->clientScript->registerScript('qModal', $js, CClientScript::POS_READY);
	}

	public function run()
	{
		echo CHtml::openTag('div', array(
			'id' => $this->htmlOptions['id'],
			'class' => 'modal fade',
			'data-backdrop' => $this->htmlOptions['data-backdrop'],
			'tabindex' => '-1',
		));
		echo CHtml::openTag('div', array(
			'style' => 'width: ' . $this->htmlOptions['width'],
			'class' => 'modal-dialog'
		));
		echo CHtml::tag('div', array(
			'class' => 'modal-content',
		), '&nbsp');
		echo CHtml::closeTag('div');
		echo CHtml::closeTag('div');
	}

}