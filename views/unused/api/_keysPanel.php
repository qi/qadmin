<?php
/**
 * @var qApiKey $keysModel
 */
?>

<?php $this->beginWidget('qPanel', array(
	'id' => 'keys-panel',
	'title' => 'API Keys',
	'collapsible' => true,
	'collapsed' => true,
	'buttons' => array(
		CHtml::link('<i class="fa fa-plus"></i>', null, array(
			'class' => 'btn btn-default btn-sm pull-right',
			'title' => Yii::t('qPanel', 'Add'),
			'disabled' => false,
			'data-toggle' => 'modal',
			'data-target' => '#keyEditModal',
			'data-source' => "/{$this->module->id}/api/ajaxKeyEditModal",
		))
	),
)); ?>
	<div class="panel-body table-responsive">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'api-keys',
			'dataProvider' => $keysModel->search(),
			'filter' => $keysModel,
			'itemsCssClass' => 'table table-condensed table-hover text-center',
			'template' => '{items}',
			'rowCssClassExpression' => function ($row, $data) {
					switch ($data->state) {
						case "active":
							$rowClass = "success";
							break;
						case "disabled":
							$rowClass = "warning";
							break;
						default:
							$rowClass = "default";
					}
					return $rowClass;
				},
			'columns' => array(
				array(
					'name' => 'description',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-4',
					),
				),
				array(
					'name' => 'key',
					'headerHtmlOptions' => array(
						'class' => 'col-xs-8',
					),
				),
				array(
					'type' => 'raw',
					'value' => 'CHtml::link("<span class=\'glyphicon glyphicon-cog\'></span>", null, array(
						"data-toggle" => "modal",
						"data-target" => "#keyEditModal",
						"data-source" => "/admin/api/ajaxKeyEditModal?id=$data->id",
						"style" => "cursor:pointer",
						"title" => "' . Yii::t('qPanel', 'View Details') . '"
					))',
					'headerHtmlOptions' => array(
						'style' => 'width: 25px; min-width: 25px; max-width: 25px;',
					),
				),
			),
		)); ?>
	</div>
<?php $this->endWidget(); ?>