<?php

/**
 * This is the model class for table "qi_menu_item".
 *
 * The followings are the available columns in table 'qi_menu_item':
 * @property string $name
 * @property string $title
 * @property string $route
 * @property string $description
 *
 * The followings are the available model relations:
 * @property QiMenuItemChild[] $parentItem
 * @property QiMenuItemChild[] $childItem
 *
 * @property bool isActive
 */
class QiMenuItem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'qi_menu_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, title, route', 'required'),
			array('name, title, route', 'length', 'max'=>255),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('name, title, route, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'childItems' => array(self::HAS_MANY, 'QiMenuItemChild', 'parent_name'),
			'children' => array(self::HAS_MANY, 'QiMenuItem', array('child_name' => 'name'), 'through' => 'childItems'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'name' => 'Name',
			'title' => 'Title',
			'route' => 'Route',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('name',$this->name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('route',$this->route,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QiMenuItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getIsActive(array $routes = array())
	{
		if (empty($routes)) {
			array_push($routes, Yii::app()->controller->route);
			array_push($routes, Yii::app()->controller->uniqueId);
		}
		return in_array($this->route, $routes);
	}
}
