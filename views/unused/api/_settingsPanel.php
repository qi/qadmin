<?php
/**
 * @var qApiSettings[] $settingsModels
 */
?>
<?php $this->beginWidget('qPanel', array(
	'id' => 'settings-panel',
	'title' => 'API Settings',
	'collapsible' => true,
	'collapsed' => true,
	'buttons' => array(
		CHtml::link('<span class="glyphicon glyphicon-cog"></span>', null, array(
			'class' => 'btn btn-default btn-sm pull-right',
			'title' => Yii::t('qPanel', 'Edit'),
			'disabled' => false,
			'data-toggle' => 'modal',
			'data-target' => '#keyEditModal',
			'data-source' => "/{$this->module->id}/api/ajaxSettingsEditModal",
		))
	),
)); ?>
	<div class="panel-body">
		<?php
		$settingsArray = array();
		foreach ($settingsModels as $settingsModel) {
			if (!isset($settingsArray[$settingsModel->type]))
				$settingsArray[$settingsModel->type] = array();
			array_push($settingsArray[$settingsModel->type], $settingsModel);
		}
		foreach ($settingsArray as $type => $settings) {
			foreach ($settings as $setting) {
				echo CHtml::tag('span', array(
						'class' => $setting->isActive ? 'label label-q-success' : 'label label-warning',
					), $setting->label) . PHP_EOL;
			}
		}
		?>
	</div>
<?php $this->endWidget(); ?>