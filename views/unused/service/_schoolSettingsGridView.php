<?php
/**
 * @author andcherv
 * @var School $schoolModel
 */

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'schoolBillingGridView',
	'dataProvider' => $schoolModel->search(),
	'filter' => $schoolModel,
	'itemsCssClass' => 'table table-condensed table-hover text-center',
	'rowCssClassExpression' => function ($row, $data) {
			if (isset($data->serviceSettings) && $data->serviceSettings->isBillingActive) {
				$rowClass = 'success';
			}
			return isset($rowClass) ? $rowClass : null;
		},
	'pager' => array(
		'header' => false,
		'cssFile' => false,
		'selectedPageCssClass' => 'active',
		'htmlOptions' => array(
			'class' => 'pagination'
		)
	),
	'columns' => array(
		array(
			'name' => 'id',
			'headerHtmlOptions' => array(
				'class' => 'col-xs-1',
			),
		),
		array(
			'name' => 'name',
			'headerHtmlOptions' => array(
				'class' => 'col-xs-8',
			),
		),
		array(
			'type' => 'raw',
			'value' => 'billingStatusButtons($data)',
			'headerHtmlOptions' => array(
				'class' => 'col-xs-3',
			),
		),
	)
));

function billingStatusButtons($schoolModel)
{
	$active = false;
	if (isset($schoolModel->serviceSettings))
		$active = $schoolModel->serviceSettings->isBillingActive;
	return CHtml::tag('div', array(
			'class' => 'btn-group',
			'data-toggle' => 'buttons',
		), CHtml::label(CHtml::radioButton('billingStatusOff_' . $schoolModel->id, $active, array(
			'data-id' => $schoolModel->id,
			'value' => ShkServiceSchoolSettings::BILLING_STATUS_DISABLED,
		)) . Yii::t('ShkService', 'Off'), 'billingStatusOff_' . $schoolModel->id, array(
			'class' => !$active ? 'btn btn-success btn-xs active' : 'btn btn-success btn-xs',
		)) . CHtml::label(CHtml::radioButton('billingStatusOff_' . $schoolModel->id, !$active, array(
				'data-id' => $schoolModel->id,
				'value' => ShkServiceSchoolSettings::BILLING_STATUS_ACTIVE,
			)) . Yii::t('ShkService', 'On'), 'billingStatusOn_' . $schoolModel->id, array(
			'class' => $active ? 'btn btn-success btn-xs active' : 'btn btn-success btn-xs',
		)));
}

?>

<script>
	$(document).ready(function () {
		var settingsSchoolBilling = $("#settingsSchoolBilling");
		settingsSchoolBilling.on("change", "input[type=radio]", function () {
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "/admin/service/settingsSchoolBillingStatus",
				data: {
					ShkServiceSchoolSettings: {
						school_id: $(this).data("id"),
						billing_status: $(this).val()
					}
				}
			}).always(function (response) {
				ajaxCallback(response);
			});
		});
	});

	function ajaxCallback(response) {
		$(document).qP().notifications().update();
		if (response && response.status) {
			switch (response.status) {
				case "success":
					$.fn.yiiGridView.update("schoolBillingGridView");
					break;
				case "fail":
					console.log(response.data);
					break;
				case "error":
					console.log(response.message);
					break;
				default:
					console.log(response);
			}
		}
	}
</script>