<?php

/**
 * This is the model class for table "qi_cli_launch".
 *
 * The followings are the available columns in table 'qi_cli_launch':
 * @property integer $id
 * @property integer $command_id
 * @property string $status
 * @property integer $exit_code
 * @property string $output
 * @property integer $start_time
 * @property integer $end_time
 *
 * The followings are the available model relations:
 * @property QiCliCommand $command
 */
class QiCliLaunch extends CActiveRecord
{
	const STATUS_PROCESSING = 'processing';
	const STATUS_SUCCESS = 'success';
	const STATUS_FAILURE = 'failure';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'qi_cli_launch';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('command_id, status, start_time', 'required'),
			array('command_id, exit_code, start_time, end_time', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>255),
			array('output', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, command_id, status, exit_code, output, start_time, end_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'command' => array(self::BELONGS_TO, 'QiCliCommand', 'command_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'command_id' => 'Command',
			'status' => 'Status',
			'exit_code' => 'Exit Code',
			'output' => 'Output',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('command_id',$this->command_id);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('exit_code',$this->exit_code);
		$criteria->compare('output',$this->output,true);
		$criteria->compare('start_time',$this->start_time);
		$criteria->compare('end_time',$this->end_time);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QiCliLaunch the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
