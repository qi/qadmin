<?php
/**
 * @var $school School
 * @var $period ShkServicePeriod
 * @var $activeServices ShkServicePeriodActive
 */

$billsNewCount = isset($period->bills) ? count($period->bills(array('scopes' => 'new'))) : 0;
$billsApprovedCount = isset($period->bills) ? count($period->bills(array('scopes' => 'approved'))) : 0;
$billsInvoiceCount = isset($period->bills) ? count($period->bills(array('scopes' => 'invoice'))) : 0;
$billsPaidCount = isset($period->bills) ? count($period->bills(array('scopes' => 'paid'))) : 0;
$billsOverdueCount = isset($period->bills) ? count($period->bills(array('scopes' => 'overdue'))) : 0;

?>

<!-- Modal header -->
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"><?php echo Yii::t('ShkService', 'Period Statistics'); ?>
		<small><?php echo $period->range; ?></small>
	</h4>
</div>

<!-- Modal body -->
<div class="modal-body">
	<div class="row">

		<!-- Period summary info -->
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
			<h4><?php echo(Yii::t('ShkService', 'Information')) ?></h4>
			<hr><?php echo $period->summary; ?>
		</div>

		<!-- Bills stat -->
		<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
			<h4><?php echo(Yii::t('ShkService', 'Bills')) ?></h4>
			<hr>
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div class="list-group">
						<a href="<?php echo Yii::app()->createUrl('/admin/service/bills/', array(
							'period_id' => $period->id,
							'status' => 'new',
						))?>" class="list-group-item">
						<span class="badge" style="background-color: rgba(144, 202, 119, 0.9)">
							<?php echo $billsNewCount ?>
						</span>
							<?php echo Yii::t('ShkService', 'New') ?>
						</a>
						<a href="<?php echo Yii::app()->createUrl('/admin/service/bills/', array(
							'period_id' => $period->id,
							'status' => 'approved',
						))?>" class="list-group-item">
						<span class="badge" style="background-color: rgba(228, 135, 67, 0.9)">
							<?php echo $billsApprovedCount ?>
						</span>
							<?php echo Yii::t('ShkService', 'Approved') ?>
						</a>
						<a href="<?php echo Yii::app()->createUrl('/admin/service/bills/', array(
							'period_id' => $period->id,
							'status' => 'invoice',
						))?>" class="list-group-item">
						<span class="badge" style="background-color: rgba(233, 182, 77, 0.9)">
							<?php echo $billsInvoiceCount ?>
						</span>
							<?php echo Yii::t('ShkService', 'Invoice') ?>
						</a>
						<a href="<?php echo Yii::app()->createUrl('/admin/service/bills/', array(
							'period_id' => $period->id,
							'status' => 'paid',
						))?>" class="list-group-item">
						<span class="badge" style="background-color: rgba(129, 198, 221, 0.9)">
							<?php echo $billsPaidCount ?>
						</span>
							<?php echo Yii::t('ShkService', 'Paid') ?>
						</a>
						<a href="<?php echo Yii::app()->createUrl('/admin/service/bills/', array(
							'period_id' => $period->id,
							'status' => 'overdue',
						))?>" class="list-group-item">
						<span class="badge" style="background-color: rgba(158, 59, 51, 0.9)">
							<?php echo $billsOverdueCount ?>
						</span>
							<?php echo Yii::t('ShkService', 'Overdue') ?>
						</a>
						<a href="<?php echo Yii::app()->createUrl('/admin/service/bills/', array(
							'period_id' => $period->id,
						))?>" class="list-group-item">
						<span class="badge">
						<?php echo count($period->bills); ?>
						</span>
							<?php echo Yii::t('ShkService', 'Total') ?>
						</a>
					</div>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="text-align: center">
					<?php $this->widget('chartjs.widgets.ChDoughnut',
						array(
							'width' => 200,
							'height' => 200,
							'htmlOptions' => array(),
							'drawLabels' => false,
							'datasets' => array(
								array(
									"value" => $billsNewCount,
									"color" => "rgba(144, 202, 119, 0.9)"
								),
								array(
									"value" => $billsApprovedCount,
									"color" => "rgba(228, 135, 67, 0.9)"
								),
								array(
									"value" => $billsInvoiceCount,
									"color" => "rgba(233, 182, 77, 0.9)"
								),
								array(
									"value" => $billsPaidCount,
									"color" => "rgba(129, 198, 221, 0.9)"
								),
								array(
									"value" => $billsOverdueCount,
									"color" => "rgba(158, 59, 51, 0.9)"
								)
							),
							'options' => array()
						)
					); ?>
				</div>

			</div>
		</div>

		<!-- Period users stat -->
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
			<h4><?php echo(Yii::t('ShkService', 'Users')) ?></h4>
			<hr>
			<p>
				<strong><?php echo Yii::t('ShkService', 'Parent-student relations') . ': '; ?></strong>
				<?php echo count($period->relations); // @todo: make STAT relation ?>
			</p>

			<p>
				<strong><?php echo Yii::t('ShkService', 'Parents') . ': '; ?></strong>
				<?php echo count($period->parents); // @todo: make STAT relation ?>
			</p>

			<p>
				<strong><?php echo Yii::t('ShkService', 'Students') . ': '; ?></strong>
				<?php echo count($period->students); // @todo: make STAT relation ?>
			</p>
		</div>

		<!-- Services usage -->
		<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
			<h4><?php echo(Yii::t('ShkService', 'Active Services')) ?></h4>
			<hr>
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<ul class="list-group">
						<?php
						$chartData = array();
						$chartLabels = array();
						$max = 0;
						foreach ($activeServices as $activeService) {
							$servicesCount = $period->getServicesCount($activeService->service_id);
							$servicesCount > $max ? $max = $servicesCount : null;
							array_push($chartData, $servicesCount);
							array_push($chartLabels, $activeService->service->title_short);
							?>
							<li class="list-group-item">
								<span class="badge"><?php echo $servicesCount ?></span>
								<?php echo $activeService->service->title; ?>
							</li>
						<?php } ?>
					</ul>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="text-align: center">
					<?php
					$this->widget(
						'chartjs.widgets.ChRadar',
						array(
							'width' => 200,
							'height' => 200,
							'htmlOptions' => array(),
							'labels' => $chartLabels,
							'datasets' => array(
								array(
									"fillColor" => "rgba(129, 198, 221, 0.5)",
									"strokeColor" => "rgba(129, 198, 221, 1)",
									"data" => $chartData
								),
							),
							'options' => array(
								'scaleOverride' => 'true',
								'scaleSteps' => round(log10($max)) > 0 ? round(log10($max)) : $max,
								'scaleStepWidth' => round(log10($max)) > 0 ? $max / round(log10($max)) : $max,
								'scaleStartValue' => 0,
							)
						)
					);
					?>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- Modal footer -->
<div class="modal-footer">
	<?php $this->widget('admin.widgets.qSubmit', array(
		'type' => 'cancel',
		'confirm' => false,
		'htmlOptions' => array(
			'class' => 'pull-left',
			'data-dismiss' => 'modal',
		),
	)); ?>
</div>