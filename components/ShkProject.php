<?php

/**
 * @author andcherv
 */
class ShkProject extends CApplicationComponent
{
	/**
	 * Returns project's modules names.
	 * @return array
	 */
	public function getModulesNames()
	{
		$modulesPath = Yii::app()->basePath . '/modules';
		$foldersNames = array();
		foreach (scandir($modulesPath) as $folderName) {
			if ($folderName !== '.' && $folderName !== '..') {
				array_push($foldersNames, $folderName);
			}
		}
		return $foldersNames;
	}
}