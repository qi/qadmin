<?php
/**
 * @author andcherv
 * @var ShkSchoolBenefit $benefitModel
 */
if (isset($benefitModel)) {
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'benefitForm',
		'enableAjaxValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'validateOnType' => true,
			'inputContainer' => '.inputContainer',
			'afterValidate' => 'js:function(form, data, hasErrors){
			if (hasErrors){
				$(form).find(".popover").qP().loading().off();
				$(form).find("input[name=create], input[name=save]").popover("toggle");
				return false;
			} else {
				return true;
			}
		}'
		),
		'focus' => array($benefitModel, 'title'),
	));
	echo $form->hiddenField($benefitModel, 'id');
	echo $form->hiddenField($benefitModel, 'school_id');
	?>

	<!-- Modal header -->
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title"><?php echo isset($benefitModel->title) ? $benefitModel->title : Yii::t('ShkSchool', 'New Benefit'); ?>
			<small><?php echo isset($benefitModel->school) ? $benefitModel->school->name : '&nbsp;'; ?></small>
		</h4>
	</div>

	<!-- Modal body -->
	<div class="modal-body">
		<div class="row">
			<div class="col-xs-8 inputContainer">
				<?php echo CHtml::tag('h4', array(), $form->labelEx($benefitModel, 'title'));
				echo CHtml::tag('hr');
				echo $form->textField($benefitModel, 'title', array(
					'class' => 'form-control input-sm',
					'placeholder' => isset($benefitModel->title) ? $benefitModel->title : '',
				));
				echo CHtml::label(Yii::t('ShkSchool', 'Title'), 'ShkSchoolBenefit_title', array(
					'class' => 'help-block',
				));
				echo $form->error($benefitModel, 'title');
				?>
			</div>
			<div class="col-xs-4 inputContainer">
				<?php echo CHtml::tag('h4', array(), $form->labelEx($benefitModel, 'status'));
				echo CHtml::tag('hr'); ?>
				<div class="btn-group" data-toggle="buttons">
					<?php isset($benefitModel->status) && $benefitModel->status == 1 ? $checked = true : $checked = false; ?>
					<label class="btn btn-sm btn-sm btn-margin <?php echo $checked ? 'active btn-success' : 'btn-default'; ?>">
						<?php echo CHtml::radioButton('ShkSchoolBenefit[status]', $checked, array(
							'value' => ShkSchoolBenefit::STATUS_ACTIVE,
						));
						echo Yii::t('ShkSchool', 'Active'); ?>
					</label>
					<?php isset($benefitModel->status) && $benefitModel->status == 0 ? $checked = true : $checked = false; ?>
					<label class="btn btn-sm btn-sm btn-margin <?php echo $checked ? 'active btn-warning' : 'btn-default'; ?>">
						<?php echo CHtml::radioButton('ShkSchoolBenefit[status]', $checked, array(
							'value' => ShkSchoolBenefit::STATUS_DISABLED,
						));
						echo Yii::t('ShkSchool', 'Disabled'); ?>
					</label>
				</div>
				<?php echo $form->error($benefitModel, 'status');
				?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 inputContainer">
				<?php echo CHtml::tag('h4', array(), $form->labelEx($benefitModel, 'description'));
				echo CHtml::tag('hr');
				echo $form->textArea($benefitModel, 'description', array(
					'class' => 'form-control input-sm',
					'placeholder' => isset($benefitModel->description) ? $benefitModel->description : '',
					'rows' => 3,
				));
				echo CHtml::label(Yii::t('ShkSchool', 'Description'), 'ShkSchoolBenefit_description', array(
					'class' => 'help-block',
				));
				echo $form->error($benefitModel, 'description');
				?>
			</div>
		</div>
		<span class="help-block">* поля, обязательные для заполнения</span>
	</div>

	<!-- Modal footer -->
	<div class="modal-footer">
		<?php
		$this->widget('admin.widgets.qSubmit', array(
			'type' => 'cancel',
			'confirm' => false,
			'htmlOptions' => array(
				'class' => 'pull-left',
				'data-dismiss' => 'modal',
			),
		));
		if ($benefitModel->isNewRecord == false) {
			$this->widget('admin.widgets.qSubmit', array(
				'type' => 'delete',
			));
		}
		$this->widget('admin.widgets.qSubmit', array(
			'type' => $benefitModel->isNewRecord ? 'create' : 'save',
		));
		?>
	</div>

	<?php $this->endWidget();
} else {
	echo CHtml::openTag('div', array('class' => 'text-center'));
	echo CHtml::tag('span', array('class' => 'error'), Yii::t('qPanel', 'Error'));
	echo CHtml::closeTag('div');
} ?>