<?php

class m000000_000003_qi_page_help extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('qi_page_help', array(
			'id' => 'pk',
			'language' => 'varchar(5) not null',
			'title' => 'string not null',
			'module' => 'varchar(100)',
			'controller' => 'varchar(100) not null',
			'action' => 'varchar(100) not null',
			'content' => 'text not null',
			'unique key `route` (`language`, `module`, `controller`, `action`)',
		));
		$this->insert('qi_page_help', array(
			'id' => '1',
			'language' => 'en_us',
			'title' => 'Help Management',
			'module' => 'qadmin',
			'controller' => 'settings',
			'action' => 'help',
			'content' => '',
		));
		$this->insert('qi_page_help', array(
			'id' => '2',
			'language' => 'ru',
			'title' => 'Управление помощью',
			'module' => 'qadmin',
			'controller' => 'settings',
			'action' => 'help',
			'content' => '<p><em style="text-align: inherit;">На данной странице осуществляется управление быстрой помощью по разделам системы.</em></p>
<h4>Просмотр помощи</h4>
<p>Для просмотра помощи нужно нажать на иконку <i class="fa fa-question-circle"></i>, после чего отобразится модальное окно с текстом помощи по разделу. Иконка отображается только если существует помощь по разделу в текущем языке.</p>
<h4>Добавление, редактирование и удаление помощи</h4>
<p>Что бы создать помощь для раздела, нужно кликнуть по кнопке "+Добавить", после чего необходимо заполнить отобразившуюся форму:</p>
<ul>
<li>Название - заголовок модального окна, помощи. Как правило, соответсвует названию раздела.</li>
<li>Язык - латинское сокращение языка помощи (например <em>en</em> или <em>ru</em>).</li>
<li>Модуль, контроллер, действие - связка этих трех элементов, как правило, формирует ссылку, по которой доступен тот или иной раздел системы (например <em>admin/system/help</em>)</li>
<li>Содержание - текст помощи. Может содержать HTML разметку.</li>
</ul>
<p>Для редактировани или удаления нужно кликнуть по иконке <span class="fa fa-cog"></span> справа от строки помощи в таблице.</p>',
		));
	}

	public function safeDown()
	{
		$this->dropTable('qi_page_help');
	}
}