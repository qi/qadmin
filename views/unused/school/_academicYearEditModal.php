<?php
/**
 * @var ShkSchoolAcademicYear $year
 * @var int $panelSchoolId
 */
?>
<?php echo CHtml::beginForm('year', 'post'); ?>

	<!-- Modal header -->
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title"><?php echo Yii::t('ShkSchool', 'Academic Year'); ?>
			<small><?php echo isset($year->school) ? $year->school->name : ''; ?></small>
		</h4>
	</div>

	<!-- Modal body -->
	<div class="modal-body">

		<!-- Range picker buttons -->
		<div class="row">
			<div id="study-year" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php
				$nowYear = date('Y');
				if (isset($year->school_id) && isset($year->start_time) && isset($year->end_time)) {
					$schoolId = $year->school_id;
					$startYear = date('Y', $year->start_time);;
					$endYear = date('Y', $year->end_time);;
					$startMonth = date('m', $year->start_time);
					$endMonth = date('m', $year->end_time);
				} else {
					$schoolId = $panelSchoolId;
					$startYear = $nowYear;
					$startMonth = 9;
					$endYear = $nowYear + 1;
					$endMonth = 5;
				}
				echo CHtml::hiddenField('ShkSchoolAcademicYear[school_id]', $schoolId);
				echo CHtml::hiddenField('ShkSchoolAcademicYear[id]', $year->id);
				?>

				<!-- Start year -->
				<div id="first-year" class="btn-group" data-toggle="buttons">
					<?php for ($i = -1; $i < 3; $i++) {
						$startYear == $startYear + $i ? $checked = true : $checked = false;
						?>
						<label class="btn btn-sm btn-margin-full <?php echo $checked === true ? 'btn-warning active' : 'btn-default'; ?>">
							<?php echo CHtml::radioButton('ShkSchoolAcademicYear[start_year]', $checked, array(
								'value' => $startYear + $i,
							));
							echo $startYear + $i ?>
						</label>
					<?php } ?>
				</div>

				<!-- Start month -->
				<div id="first-month" class="btn-group" data-toggle="buttons">
					<?php foreach (Yii::app()->params['_months_short'] as $monthNumber => $monthName) {
						$monthNumber == $startMonth ? $checked = true : $checked = false;
						if ($monthNumber >= $startMonth) {
							$color = 'btn-success';
						} elseif ($monthNumber <= $endMonth) {
							$color = 'btn-default';
						} else {
							$color = 'btn-default';
						}
						?>
						<label class="btn btn-sm btn-margin-full <?php echo $color;
						echo $checked ? ' active' : null; ?>">
							<?php echo CHtml::radioButton('ShkSchoolAcademicYear[start_month]', $checked, array(
								'value' => $monthNumber
							));
							echo $monthName; ?>
						</label>
					<?php } ?>
					<span class="help-block">Начало учебного года.</span>
				</div>

				<!-- End year -->
				<div id="last-year" class="btn-group" data-toggle="buttons">
					<?php for ($i = -1; $i < 3; $i++) {
						$endYear == $startYear + $i ? $checked = true : $checked = false;
						?>
						<label class="btn btn-sm btn-margin-full <?php echo $checked === true ? 'btn-warning active' : 'btn-default'; ?>">
							<?php echo CHtml::radioButton('ShkSchoolAcademicYear[end_year]', $checked, array(
								'value' => $startYear + $i,
							));
							echo $startYear + $i ?>
						</label>
					<?php } ?>
				</div>

				<!-- End month -->
				<div id="last-month" class="btn-group" data-toggle="buttons">
					<?php foreach (Yii::app()->params['_months_short'] as $monthNumber => $monthName) {
						$monthNumber == $endMonth ? $checked = true : $checked = false;
						if ($monthNumber <= $endMonth) {
							$color = 'btn-success';
						} elseif ($monthNumber >= $startMonth) {
							$color = 'btn-default';
						} else {
							$color = 'btn-default';
						}
						?>
						<label class="btn btn-sm btn-margin-full <?php echo $color;
						echo $checked ? ' active' : null; ?>">
							<?php echo CHtml::radioButton('ShkSchoolAcademicYear[end_month]', $checked, array(
								'value' => $monthNumber
							));
							echo $monthName; ?>
						</label>
					<?php } ?>
					<span class="help-block">Конец учебного года.</span>
				</div>

			</div>
		</div>
	</div>

	<!-- Modal footer -->
	<div class="modal-footer">
		<?php if (!$year->isNewRecord) {
			$this->widget('admin.widgets.qSubmit', array(
				'type' => 'delete',
			));
		} ?>
		<?php $this->widget('admin.widgets.qSubmit', array(
			'type' => $year->isNewRecord ? 'create' : 'save',
		)); ?>
		<?php $this->widget('admin.widgets.qSubmit', array(
			'type' => 'cancel',
			'confirm' => false,
			'htmlOptions' => array(
				'class' => 'pull-left',
				'data-dismiss' => 'modal',
			),
		)); ?>
	</div>

<?php echo CHtml::endForm(); ?>