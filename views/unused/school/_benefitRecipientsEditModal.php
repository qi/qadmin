<?php
/**
 * @author andcherv
 * @var ShkSchoolBenefit $benefitModel
 * @var ShkSchoolStudentBenefitRel $benefitRecipientsModel
 */
?>

<!-- Modal header -->
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title"><?php echo Yii::t('ShkSchool', 'Benefit Recipients'); ?>
		<small>
			<?php if (isset($benefitModel->school->name) && isset($benefitModel->title)) {
				echo $benefitModel->school->name . ', ' . $benefitModel->title;
			} ?>
		</small>
	</h4>
</div>

<!-- Modal body -->
<div class="modal-body">
	<div class="row">
		<form id="addRecipientForm" class="col-xs-12 form-inline" style="margin-bottom: 8px;">
			<?php echo CHtml::htmlButton(CHtml::tag('span', array(
					'class' => 'glyphicon glyphicon-plus'
				), '') . ' ' . Yii::t('qPanel', 'Add'), array(
					'id' => 'addBenefitRecipientButton',
					'class' => 'btn btn-default btn-sm',
				));
			?>
			<label class="sr-only" for="<?php echo get_class($benefitRecipientsModel) . '_student_id'; ?>"></label>
			<?php echo CHtml::activeHiddenField($benefitModel, 'id') ?>
			<?php $this->widget('qPanel.extensions.yii-selectize.YiiSelectize', array(
				'model' => $benefitRecipientsModel,
				'attribute' => 'student_id',
				'placeholder' => Yii::t('ShkSchool', 'Search a student...'),
				'useWithBootstrap' => true,
				'options' => array(
					'create' => false,
					'hideSelected' => true,
					'valueField' => 'id',
					'labelField' => 'name',
					'searchField' => 'name',
					'load' => "js:function (query, callback) {
						if (!query.length) return callback();
						$.ajax({
							type: \"GET\",
							dataType: \"json\",
							url: \"/admin/school/ajaxGetAvailableBenefitRecipients\",
							data: {
								school_id: {$benefitModel->school_id},
								query: query
							},
							success: function(response) {callback(response.data);}
						});
					}",
				),
				'htmlOptions' => array(
					'class' => 'form-control text-left',
					'style' => 'width: 60%',
				),
			)); ?>
		</form>
		<div class="col-xs-12">
			<?php
			if (isset($benefitRecipientsModel)) {
				$this->widget('zii.widgets.grid.CGridView', array(
					'id' => 'benefitRecipientsGrid',
					'dataProvider' => $benefitRecipientsModel->search($benefitModel->id),
					'filter' => $benefitRecipientsModel,
					'itemsCssClass' => 'table table-condensed table-hover text-center',
					'pager' => array(
						'header' => false,
						'cssFile' => false,
						'selectedPageCssClass' => 'active',
						'htmlOptions' => array(
							'class' => 'pagination'
						)
					),
					'columns' => array(
						array(
							'name' => 'studentName',
							'headerHtmlOptions' => array(
								'class' => 'col-xs-12',
							),
						),
						array(
							'type' => 'raw',
							'value' => 'CHtml::link("<span class=\'glyphicon glyphicon-remove\'></span>", null, array(
								"class" => "benefit-recipient-delete-link",
								"data-id" => $data->student_id,
								"title" => "' . Yii::t('qPanel', 'Delete') . '"
							))',
						),
					),
				));
			}
			?>
		</div>
	</div>
</div>

<!-- Modal footer -->
<div class="modal-footer">
	<?php
	$this->widget('admin.widgets.qSubmit', array(
		'type' => 'cancel',
		'confirm' => false,
		'htmlOptions' => array(
			'value' => Yii::t('qPanel', 'Close'),
			'class' => 'pull-left',
			'data-dismiss' => 'modal',
		),
	));
	?>
</div>

<script>
	$(document).ready(function () {
		var modal = $("#benefitRecipientsEditModal");
		modal.on("click", "#addBenefitRecipientButton", function () {
			var studentId = $("#ShkSchoolStudentBenefitRel_student_id").val();
			var benefitId = $("#ShkSchoolBenefit_id").val();
			if (studentId && benefitId) {
				$.ajax({
					type: "POST",
					dataType: "json",
					url: "/admin/school/ajaxAddBenefitRecipient",
					data: {
						student_id: studentId,
						benefit_id: benefitId
					}
				}).always(function (response) {
					var selectize = $("#ShkSchoolStudentBenefitRel_student_id").selectize();
					var select = selectize[0].selectize;
					select.clear();
					ajaxCallback(response);
				});
			}
		});
		modal.on("click", ".benefit-recipient-delete-link", function () {
			var studentId = $(this).data("id");
			if (studentId) {
				$.ajax({
					type: "GET",
					dataType: "json",
					url: "/admin/school/ajaxDeleteBenefitRecipient",
					data: {
						student_id: studentId
					}
				}).always(function (response) {
					ajaxCallback(response);
				});
			}
		});
	});

	function ajaxCallback(response) {
		if (response.status === "success") {
			$.fn.yiiGridView.update("benefitRecipientsGrid");
		} else if (response['status'] === "fail") {
			console.log(response["data"]);
		} else if (response['status'] === "error") {
			console.log(response["message"]);
		}
	}
</script>