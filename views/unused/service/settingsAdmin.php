<?php
/**
 * Service settings managment page.
 * @author andcherv
 * @var School $schoolModel
 */
?>

<!-- Page Header -->
<?php $this->renderPartial('//layouts/_adminPageHeader', array(
	'header' => Yii::t('ShkService', 'Settings'),
)); ?>

<div id="table" class="row table-responsive">
	<!-- School Service Settings GridView -->
	<div id="settingsSchoolBilling" class="col-xs-12 col-lg-6">
		<h4><?php echo Yii::t('ShkService', 'Billing 2.0'); ?></h4>
		<hr/>
		<?php if (isset($schoolModel) && is_object($schoolModel)) {
			$this->renderPartial('_schoolSettingsGridView', array(
				'schoolModel' => $schoolModel,
			));
		} ?>
		<span class="help-block"><?php echo Yii::t('ShkService', 'Status of the Billing 2.0 mode for a school.'); ?></span>
	</div>
	<div id="settingsSchoolBilling" class="col-xs-12 col-lg-6">
		<h4>Settings block 2</h4>
		<hr/>
		<span class="help-block">Settings block 2</span>
	</div>
</div>