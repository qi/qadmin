SET FOREIGN_KEY_CHECKS = 0;

-- --------------------------------------------------------

INSERT INTO `qp_page_help` (`id`, `language`, `title`, `module`,`controller`, `action`, `content`) VALUES
(1, 'ru', 'Управление помощью', 'admin', 'system', 'help', '<p><img src="/themes/shk/assets/img/logo.png" alt="" style="float: left; margin: 0px 10px 10px 0px;"><em style="text-align: inherit;">На данной странице осуществляется управление быстрой помощью по разделам системы.</em></p>
<h4>Просмотр помощи</h4>
<p>Для просмотра помощи нужно нажать на иконку <span class="glyphicon glyphicon-question-sign"></span>, после чего отобразится модальное окно с текстом помощи по разделу. Иконка отображается только если существует помощь по разделу в текущем языке.</p>
<h4>Добавление, редактирование и удаление помощи</h4>
<p>Что бы создать помощь для раздела, нужно кликнуть по кнопке "+Добавить", после чего необходимо заполнить отобразившуюся форму:</p>
<ul>
<li>Название - заголовок модального окна, помощи. Как правило, соответсвует названию раздела.</li>
<li>Язык - латинское сокращение языка помощи (например <em>en</em> или <em>ru</em>).</li>
<li>Модуль, контроллер, действие - связка этих трех элементов, как правило, формирует ссылку, по которой доступен тот или иной раздел системы (например <em>admin/system/help</em>)</li>
<li>Содержание - текст помощи. Может содержать HTML разметку.</li>
</ul>
<p>Для редактировани или удаления нужно кликнуть по иконке <span class="glyphicon glyphicon-cog"></span> справа от строки помощи в таблице.</p>');

-- --------------------------------------------------------

INSERT INTO `shk_system_status_enum` (`id`, `label`, `title`, `description`) VALUES
(1, 'shk-sys-default', 'SYSTEM: Default', 'Default status'),
(2, 'shk-sys-unknown', 'SYSTEM: Unknown', 'Unknown status'),
(10, 'shk-sys-ok', 'SYSTEM: OK', 'OK'),
(11, 'shk-sys-error', 'SYSTEM: Error', 'Error'),
(99, 'shk-srv-unavailable', 'SERVICE: Unavailable', 'Service is not available'),
(100, 'shk-srv-active', 'SERVICE: Active', 'Service is active'),
(101, 'shk-srv-suspended', 'SERVICE: Suspended', 'Service is suspended'),
(201, 'shk-role-general', 'ROLE: General', 'General user role'),
(202, 'shk-role-additional', 'ROLE: Additional', 'Additional user role'),
(221, 'shk-group-general', 'GROUP: General', 'General study group'),
(222, 'shk-group-subgroup', 'GROUP: Sub-group', 'Study sub-group'),

(500, 'shk-cafe-created', 'CAFETERIA: Created item', 'Cafeteria menu item was created'),
(501, 'shk-cafe-deleted', 'CAFETERIA: Deleted item', 'Cafeteria menu item was deleted'),
(520, 'shk-cafe-peronal', 'CAFETERIA: Personal menu', 'Personal cafeteria menu'),
(521, 'shk-cafe-school', 'CAFETERIA: School menu', 'School cafeteria menu'),

(720, 'shk-bill-item-added', 'BILL ITEM: Added', 'Item is added to the bill'),
(721, 'shk-bill-item-deleted', 'BILL ITEM: Deleted', 'Item is deleted form the bill'),

(1000, 'shk-api-key-active', 'API: Active key', 'API Key is active'),
(1001, 'shk-api-key-inactive', 'API: Inactive key', 'API Key is inactive');

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;