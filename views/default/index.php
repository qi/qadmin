<?php $this->widget('qi.qadmin.widgets.QiPageHeader', array(
	'header' => Yii::t('admin', 'Dashboard'),
)); ?>

	<div id="page-content">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-md-4">
						<?php $this->beginWidget('qi.qadmin.widgets.QiPanel', array('id' => 'panel1')); ?>
						<div class="panel-body">
							<h3>Credits</h3>
							<hr/>
							<ul>
								<li><a href="#">Yii Framework</a> Module</li>
								<li>Powered by <a href="#">jQuery</a> and <a href="#">Bootstrap 3</a></li>
								<li><a href="#">Font Awesome</a> Icons</li>
								<li>User Icons by <a href="http://icons8.com/">Visual Pharm</a></li>
								<li>Charts by <a href="#">Flot</a> and <a href="#">Easy Pie Chart</a></li>
								<li><a href="#">Animate.css</a></li>
								<li><a href="#">PYOE</a></li>
							</ul>
<!--							<img class="pull-right" src="http://i.creativecommons.org/l/by-nd/3.0/88x31.png" alt="" width="88" height="31">-->
						</div>

						<?php $this->endWidget('panel1'); ?>
					</div>
					<div class="col-xs-8">
						<?php
						$this->beginWidget('qi.qadmin.widgets.QiPanel', array(
							'id' => 'panel2',
							'title' => 'Panel',
							'collapsible' => true,
							'buttons' => array(
								CHtml::link('<i class="fa fa-bars"></i>', null, array(
									'class' => 'btn btn-default btn-sm pull-right',
								))
							),
						));
						$this->widget('vendor.eirikhm.morris.MorrisChartWidget', array(
							'id'      => 'myChartElement',
							'options' => array(
								'chartType' => 'Area',
								'data'      => array(
									array('y' => 2006, 'a' => 100, 'b' => 90),
									array('y' => 2007, 'a' => 40, 'b' => 60),
									array('y' => 2008, 'a' => 50, 'b' => 10),
									array('y' => 2009, 'a' => 60, 'b' => 50),
									array('y' => 2010, 'a' => 60, 'b' => 40),
								),
								'xkey'      => 'y',
								'ykeys'     => array('a', 'b'),
								'labels'    => array('Series A', 'Series B'),
							),
						));
						$this->endWidget('panel2');
						?></div>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-4"></div>
					<div class="col-xs-4"></div>
					<div class="col-xs-4"></div>
				</div>
			</div>
		</div>
	</div>