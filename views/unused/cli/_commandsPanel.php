<?php $this->beginWidget('qPanel', array(
	'id' => 'commands-panel',
	'title' => 'Commands'
)); ?>
	<div class="panel-body">
		<?php
		echo CHtml::ajaxButton('Run', '/admin/cli/ajaxTest', array(
			'allways' => 'js:function(response){js(response);}'
		));
		?>
	</div>
<?php $this->endWidget(); ?>

<script>
	function js(response)
	{
		console.log(response);
	}
</script>