<?php
/**
 * @author andcherv
 */
?>
<?php $this->renderPartial('/layouts/_defaultPageHeader', array(
	'header' => Yii::t('qPanel', 'Access Control'),
	'small' => '',
)); ?>
<div id="page-content" class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-body table-responsive">


				<?php
				$result = Yii::app()->db->createCommand('SELECT * FROM auth_item WHERE type=2')->queryAll();

				$dataProvider = new CArrayDataProvider($result, array(
					'keyField' => 'name',
					'sort' => array(
						'attributes' => array(
							'name',
						),
					),
				));


				$this->widget('zii.widgets.grid.CGridView', array(
					'dataProvider' => $dataProvider,
					'itemsCssClass' => 'table table-condensed table-hover text-center',

				));

				?>
			</div>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-body table-responsive">


				<?php
				$result = Yii::app()->db->createCommand('SELECT * FROM auth_item_child')->queryAll();

				$dataProvider = new CArrayDataProvider($result, array(
					'keyField' => 'parent',
					'sort' => array(
						'attributes' => array(
							'parent', 'child'
						),
					),
				));


				$this->widget('zii.widgets.grid.CGridView', array(
					'dataProvider' => $dataProvider,
					'itemsCssClass' => 'table table-condensed table-hover text-center',

				));

				?>
			</div>
		</div>
	</div>
</div>

