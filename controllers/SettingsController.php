<?php

/**
 * Class SystemController
 * @author andcherv
 */
class SettingsController extends QiController
{

	public function getViewPath()
	{
		return $this->module->basePath . '/views/settings';
	}

	public function actionHelp()
	{
		$helpModel = new QiPageHelp('search');
		$helpModel->unsetAttributes();
		$helpModel->setAttributes(Yii::app()->request->getQuery('PageHelp'));
		$this->addBreadcrumbs(array(
			Yii::t('settings', 'Settings') => Yii::app()->createUrl('..'),
			Yii::t('settings', 'Page Help Management'),
		));
		$this->render('help/index', array(
			'helpModel' => $helpModel,
		));
	}

	/**
	 * Returns quick page help modal's content.
	 * @param $module
	 * @param $controller
	 * @param $action
	 */
	public function actionAxViewHelp($module, $controller, $action)
	{
		$this->renderPartial('help/_modalView', array(
			'pageHelp' => QiPageHelp::model()->find(array(
					'condition' => 'module=:module AND controller=:controller AND action=:action AND language=:language',
					'params' => array(
						':module' => $module, ':controller' => $controller, ':action' => $action,
						':language' => Yii::app()->language ? Yii::app()->language : 'en',
					),
				))
		));
		Yii::app()->end();
	}

	public function actionAjaxEditPageHelp($id = null)
	{
		$helpModel = $this->getModel('QiPageHelp', $id);
		if (Yii::app()->request->isAjaxRequest) {
			$this->performAjaxValidation(array($helpModel), 'pageHelpForm');
			$this->renderPartial('help/_modalEdit', array(
				'helpModel' => $helpModel,
			), false, true);
			Yii::app()->end();
		} else {
			$connection = Yii::app()->db;
			$currentTransaction = $connection->currentTransaction;
			if ($currentTransaction == null)
				$transaction = $connection->beginTransaction();
			try {
				if (isset($_POST['create']) || isset($_POST['save'])) {
					if (isset($_POST[get_class($helpModel)])) {
						$helpModel->attributes = $_POST[get_class($helpModel)];
						if ($helpModel->save()) {
							if (isset($transaction) && $currentTransaction == null) {
								$transaction->commit();
								Yii::app()->panel->setFlash('qPanel-save-success');
							}
						} else throw new CDbException('Can\'t save ' . get_class($helpModel) . ' model.');
					}
				} elseif (isset($_POST['delete'])) {
					if ($helpModel->delete()) {
						if (isset($transaction) && $currentTransaction == null) {
							$transaction->commit();
							Yii::app()->panel->setFlash('qPanel-delete-success');
						}
					} else throw new CDbException('Can\'t delete ' . get_class($helpModel) . ' model.');
				}
			} catch (Exception $exception) {
				if (isset($transaction) && $currentTransaction == null)
					$transaction->rollback();
				Yii::app()->panel->setFlash('qPanel-transaction-fail', $exception->getMessage());
				if (isset($helpModel) && $helpModel->hasErrors()) {
					foreach ($helpModel->errors as $key => $value) {
						Yii::app()->user->setFlash($key, CJSON::encode(array(
							'title' => Yii::t('qPanel', 'Error'),
							'text' => $value,
							'type' => 'error',
						)));
					}
				}
			}
			$this->redirect('help');
		}
	}

	public function actionAjaxAssetsClean()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$diff = Yii::app()->qFiles->emptyDir(Yii::app()->assetManager->basePath);
			if ($diff !== false)
				Yii::app()->panel->setFlash('shk-system-assets-clean-success', implode(', ', $diff));
			else
				Yii::app()->panel->setFlash('shk-system-assets-clean-fail');
			Yii::app()->end();
		}
	}

	public function actionAjaxAssetsGetCount()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$count = Yii::app()->qFiles->count(Yii::app()->assetManager->basePath);
			echo CJSON::encode(array(
				'status' => 'success',
				'data' => array(
					'count' => $count,
				),
			));
			Yii::app()->end();
		}
	}

	public function actionMaintenance()
	{
		$this->pageTitle = Yii::t('qPanel', 'Maintenance');
		$this->addBreadcrumbs(array(
			Yii::t('qPanel', 'System') => Yii::app()->createUrl('system'),
			Yii::t('qPanel', 'Maintenance'),
		));
		$this->render('settings', array());
	}

	public function actionAccess()
	{
		$this->pageTitle = Yii::t('qPanel', 'Access Control');
		$this->addBreadcrumbs(array(
			Yii::t('qPanel', 'System') => Yii::app()->createUrl('system'),
			Yii::t('qPanel', 'Access Control'),
		));
		$this->render('access', array(
			'roles' => null,
		));
	}
}