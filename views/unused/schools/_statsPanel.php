<style>
	.q-dash-panel-title {
		display: inline-block;
		overflow: hidden;
	}

	.progress {
		margin-bottom: 10px;
	}

	.progress-bar-connected {
		background-color: #c9c554;
	}

	.progress-bar-public {
		background-color: #568ca3;
	}
</style>

<div id="schools-stats-panel" class="panel panel-default">
	<div class="panel-body">
		<h1>
			<i class="fa fa-graduation-cap"></i>
			<small>
				<span class="q-dash-panel-value">&nbsp;</span>
				<span class="q-dash-panel-title">
					<?php echo Yii::t('School', 'Schools'); ?>
				</span>
			</small>
		</h1>
		<div class="progress">
			<div class="progress-bar progress-bar-public"></div>
			<div class="progress-bar progress-bar-connected"></div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		var schoolsStatsPanel = $("#schools-stats-panel");
		updateSchoolsStats(schoolsStatsPanel);
		setInterval(function () {
			updateSchoolsStats(schoolsStatsPanel);
		}, 1000 * 10);
	});
	function updateSchoolsStats(panel) {
		var requestsTotal = panel.find(".q-dash-panel-value");
		var progressBar = panel.find(".progress");
		$.get('/admin/schools/ajaxUpdateStats', function (response) {
			if (requestsTotal.html() != response["data"]["total"]) {
				requestsTotal.slideToggle("fast", function () {
					$(this).html(response["data"]["total"]);
					$(this).slideToggle("fast");
				});
				progressBar.find(".progress-bar-connected").prop("title", "Public: " + response["data"]["public"]);
				progressBar.find(".progress-bar-public").prop("title", "Connected: " + response["data"]["connected"]);
				progressBar.find(".progress-bar-connected").width(response["data"]["pctPublic"] + "%");
				progressBar.find(".progress-bar-public").width(response["data"]["pctConnected"] + "%");
			}
		}, 'json');
	}
</script>