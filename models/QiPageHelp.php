<?php

/**
 * This is the model class for table "qi_page_help".
 *
 * The followings are the available columns in table 'qi_page_help':
 * @property integer $id
 * @property string $language
 * @property string $title
 * @property string $module
 * @property string $controller
 * @property string $action
 * @property string $content
 */
class QiPageHelp extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'qi_page_help';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('language, title, controller, action, content', 'required'),
			array('language', 'length', 'max'=>5),
			array('title', 'length', 'max'=>255),
			array('module, controller, action', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, language, title, module, controller, action, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'language' => 'Language',
			'title' => 'Title',
			'module' => 'Module',
			'controller' => 'Controller',
			'action' => 'Action',
			'content' => 'Content',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('module',$this->module,true);
		$criteria->compare('controller',$this->controller,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('content',$this->content,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QiPageHelp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function helpExists()
	{
		return self::model()->exists(array(
			'condition' => '`t`.`language`=:language AND `t`.`module`=:module AND `t`.`controller`=:controller AND `t`.`action`=:action',
			'params' => array(
				':language' => isset(Yii::app()->language) ? Yii::app()->language : 'en',
				':module' => isset(Yii::app()->controller->module) ? Yii::app()->controller->module->id : null,
				':controller' => isset(Yii::app()->controller) ? Yii::app()->controller->id : '',
				':action' => isset(Yii::app()->controller->action) ? Yii::app()->controller->action->id : '',
			),
		));
	}
}
