<?php
/**
 * @var $periods ShkServicePeriod[]
 * @param $data ShkServicePeriod
 * @param $row
 * @return string
 */

$this->widget('datepicker.BootstrapDatepicker');

$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'service-periods',
	'afterAjaxUpdate' => 'function() {
							shk.admin.installPopover();
							shk.reloadDatePicker();
						}',
	'dataProvider' => $periods->search(),
	'filter' => $periods,
	'itemsCssClass' => 'table table-condensed table-hover text-center',
	'rowCssClassExpression' => function ($row, $data) {
			$now = time();
			if ($now >= $data->start_time && $now <= $data->end_time)
				return 'success';
			return null;
		},
	'pager' => array(
		'header' => false,
		'cssFile' => $this->assetsPath . '/css/shk.admin.pager.css'
	),
	'columns' => array(
		array(
			'type' => 'raw',
			'value' => 'CHtml::tag("span", array(
				"class" => "glyphicon glyphicon-info-sign",
				"data-content" => "$data->summary",
				"data-toggle" => "popover"
			))',
		),
		array(
			'name' => 'school_name',
			'filter' => false,
			'headerHtmlOptions' => array(
				'class' => 'col-xs-2',
			),
		),
		array(
			'name' => 'start_time',
			'filter' => dateFilter($periods, 'start_time'),
			'headerHtmlOptions' => array(
				'class' => 'col-xs-2',
			),
			'value' => 'date(\'d.m.Y\', $data->start_time)',
		),
		array(
			'name' => 'end_time',
			'filter' => dateFilter($periods, 'end_time'),
			'headerHtmlOptions' => array(
				'class' => 'col-xs-2',
			),
			'value' => 'date(\'d.m.Y\', $data->end_time)',
		),
		/*array(
			'name' => 'totalBillsCount',
			'type' => 'raw',
			'filter' => false,
			'value' => 'CHtml::link(CHtml::tag(\'span\', array(\'class\' => \'badge\'), $data->totalBillsCount), array(\'bills\', \'period_id\' => $data->id)) .
						CHtml::link(CHtml::tag(\'span\', array(\'class\' => \'badge bg-new\'), $data->newBillsCount), array(\'bills\', \'period_id\' => $data->id, \'status\' => \'new\')) .
						CHtml::link(CHtml::tag(\'span\', array(\'class\' => \'badge bg-approved\'), $data->approvedBillsCount), array(\'bills\', \'period_id\' => $data->id, \'status\' => \'approved\')) .
						CHtml::link(CHtml::tag(\'span\', array(\'class\' => \'badge bg-invoice\'), $data->invoiceBillsCount),	array(\'bills\', \'period_id\' => $data->id, \'status\' => \'invoice\')) .
						CHtml::link(CHtml::tag(\'span\', array(\'class\' => \'badge bg-overdue\'), $data->overdueBillsCount), array(\'bills\', \'period_id\' => $data->id, \'status\' => \'overdue\')) .
						CHtml::link(CHtml::tag(\'span\', array(\'class\' => \'badge bg-paid\'), $data->paidBillsCount), array(\'bills\', \'period_id\' => $data->id, \'status\' => \'paid\'))',
			'headerHtmlOptions' => array(
				'class' => 'col-xs-2',
			),
		),*/
		array(
			'name' => 'totalBillsCount',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::tag(\'span\', array(\'class\' => \'badge\'), $data->totalBillsCount),
									array(\'bills\', \'period_id\' => $data->id))',
			'headerHtmlOptions' => array(
				'class' => 'col-xs-1',
			),
		),
		array(
			'name' => 'newBillsCount',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::tag(\'span\', array(\'class\' => \'badge bg-new\'), $data->newBillsCount),
									array(\'bills\', \'period_id\' => $data->id, \'status\' => \'new\'))',
			'htmlOptions' => array(
				'class' => 'col-xs-1',
			),
		),
		array(
			'name' => 'approvedBillsCount',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::tag(\'span\', array(\'class\' => \'badge bg-approved\'), $data->approvedBillsCount),
									array(\'bills\', \'period_id\' => $data->id, \'status\' => \'approved\'))',
			'htmlOptions' => array(
				'class' => 'col-xs-1',
			),
		),
		array(
			'name' => 'invoiceBillsCount',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::tag(\'span\', array(\'class\' => \'badge bg-invoice\'), $data->invoiceBillsCount),
									array(\'bills\', \'period_id\' => $data->id, \'status\' => \'invoice\'))',
			'htmlOptions' => array(
				'class' => 'col-xs-1',
			),
		),
		array(
			'name' => 'overdueBillsCount',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::tag(\'span\', array(\'class\' => \'badge bg-overdue\'), $data->overdueBillsCount),
									array(\'bills\', \'period_id\' => $data->id, \'status\' => \'overdue\'))',
			'headerHtmlOptions' => array(
				'class' => 'col-xs-1',
			)
		),
		array(
			'name' => 'paidBillsCount',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::tag(\'span\', array(\'class\' => \'badge bg-paid\'), $data->paidBillsCount),
									array(\'bills\', \'period_id\' => $data->id, \'status\' => \'paid\'))',
			'headerHtmlOptions' => array(
				'class' => 'col-xs-1 col-sm-1 col-md-1 col-lg-1',
			),
		),
		array(
			'type' => 'raw',
			'value' => 'CHtml::link("<span class=\'glyphicon glyphicon-stats\'></span>", null, array(
				"data-toggle" => "modal",
				"data-target" => "#paymentPeriodStatistics",
				"data-source" => "/admin/service/periodStatistics?period_id=$data->id",
				"title" => "' . Yii::t('ShkService', 'Payment Period Statistics') . '"
			))',
		),
		array(
			'type' => 'raw',
			'value' => 'CHtml::link("<span class=\'glyphicon glyphicon-usd\'></span>", null, array(
				"data-toggle" => "modal",
				"data-target" => "#periodPricesDefault",
				"data-source" => "/admin/service/pricesDefault?period_id=$data->id",
				"title" => "' . Yii::t('ShkService', 'Payment Period Prices') . '"
			))',
		),
		array(
			'type' => 'raw',
			'value' => 'CHtml::link("<span class=\'glyphicon glyphicon-cog\'></span>", null, array(
				"data-toggle" => "modal",
				"data-target" => "#paymentPeriodParams",
				"data-source" => "/admin/service/periodParameters?period_id=$data->id",
				"title" => "' . Yii::t('ShkService', 'Payment Period Parameters') . '"
			))',
		),
	),
));

function dateFilter($data, $attribute)
{
	$filter = '<div class="input-group">';
	$attribute == 'start_time' ? $filter .= '<span class="input-group-addon">></span>' : null;
	$filter .= '<input class="date form-control input-sm"
	id="ShkServicePeriod_' . $attribute . '"
	name="ShkServicePeriod[' . $attribute . ']"
	value="' . $data->$attribute . '"
	type="text">';
	$attribute == 'end_time' ? $filter .= '<span class="input-group-addon"><</span>' : null;
	$filter .= '</div>';
	return $filter;
}