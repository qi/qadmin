<?php ?>

<?php $this->renderPartial('/layouts/_defaultPageHeader', array(
	'header' => Yii::t('qApi', 'RESTful API Documentation'),
	'small' => '<a href="http://master.s-kid.net-pupil.com/api">http://master.s-kid.net-pupil.com/api</a>',
)); ?>

<div id="page-content" class="row">
	<div class="col-xs-12">
		<?php $this->beginWidget('qPanel', array(
			'id' => 'api-doc-panel',
			'collapsible' => false,
		)); ?>
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
					<?php $this->renderPartial('doc/_docIndex'); ?>
				</div>
				<div class="col-xs-5 col-sm-4 col-md-3 col-lg-2">
					<?php $this->renderPartial('doc/_docContents'); ?>
				</div>
			</div>
		</div>
		<div class="panel-footer"></div>
		<?php $this->endWidget(); ?>
	</div>
</div>