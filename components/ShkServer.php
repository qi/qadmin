<?php

/**
 * ShkServer component.
 * @author andcherv
 */
class ShkServer extends CApplicationComponent
{
	/** Returns mobile code for the specified location.
	 * @author andcherv
	 * @param null|string $location
	 * @return null|string
	 */
	public static function getMobileCode($location = null)
	{
		$mobileCode = null;
		if ($location === null) {
			$location = self::getLocation();
		}
		switch ($location) {
			case 'ru':
				$mobileCode = '7';
				break;
			case 'ua':
				$mobileCode = '38';
				break;
			case 'by':
				$mobileCode = '37';
				break;
			case 'kz':
				$mobileCode = '7';
				break;
		}
		return $mobileCode;
	}

	/** Returns 1-level domain of the request.
	 * @author andcherv
	 * @return mixed|null|string
	 */
	public static function getLocation()
	{
		$domains = explode('.', Yii::app()->request->serverName);
		return end($domains);
	}
}