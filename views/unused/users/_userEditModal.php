<?php
/**
 * @var UserAuth $model
 * @var CActiveForm $form
 */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'user-form',
	'enableAjaxValidation' => true,
	'errorMessageCssClass' => 'help-block error',
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'errorCssClass' => 'has-error',
		'afterValidate' => 'js:function(form, data, hasErrors){
			return form.qP().modal().afterValidate(form, data, hasErrors);
		}',
	),
)); ?>

<?php echo CHtml::activeHiddenField($model, 'id'); ?>

<?php /* Modal header */ ?>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">
			<?php echo isset($model) ? $model->fullName : '&nbsp;' ?>
			<small>
				<?php if (isset($model->activity)) {
					echo Yii::t('User', 'LastOnline') . ': ';
					echo Yii::app()->dateFormatter->formatDateTime($model->activity->timestamp, 'full');
				} else echo '&nbsp;'; ?>
			</small>
		</h4>
	</div>

<?php /* Modal body */ ?>
	<div class="modal-body">
		<div class="row">
			<?php /* Photo */ ?>
			<div class="col-xs-3">
				<a href="#" class="thumbnail">
					<img width="122px" height="122px" src="<?php echo $model->profile->photo_url; ?>" alt=""/>
				</a>
			</div>
			<?php /* Name */ ?>
			<div class="col-xs-9 form-horizontal">
				<div class="form-group input-container" style="margin-bottom: 13px;">
					<div class="col-sm-12">
						<?php echo $form->labelEx($model->profile, 'last_name', array('class' => 'sr-only')); ?>
						<?php echo $form->textField($model->profile, 'last_name', array(
							'class' => 'form-control input-sm',
							'placeholder' => $model->profile->getAttributeLabel('last_name'),
						)); ?>
					</div>
				</div>
				<div class="form-group input-container" style="margin-bottom: 13px;">
					<div class="col-sm-12">
						<?php echo $form->labelEx($model->profile, 'first_name', array('class' => 'sr-only')); ?>
						<?php echo $form->textField($model->profile, 'first_name', array(
							'class' => 'form-control input-sm',
							'placeholder' => $model->profile->getAttributeLabel('first_name'),
						)); ?>
					</div>
				</div>
				<div class="form-group input-container" style="margin-bottom: 13px;">
					<div class="col-sm-12">
						<?php echo $form->labelEx($model->profile, 'father_name', array('class' => 'sr-only')); ?>
						<?php echo $form->textField($model->profile, 'father_name', array(
							'class' => 'form-control input-sm',
							'placeholder' => $model->profile->getAttributeLabel('father_name'),
						)); ?>
					</div>
				</div>
				<?php echo $form->error($model->profile, 'last_name'); ?>
				<?php echo $form->error($model->profile, 'first_name'); ?>
				<?php echo $form->error($model->profile, 'father_name'); ?>
				<span class="help-block">&nbsp;</span>
			</div>
		</div>
		<div class="row">
			<?php /* E-mail */ ?>
			<div class="col-xs-6">
				<h4><?php echo $form->labelEx($model, 'email'); ?></h4>
				<hr/>
				<?php echo $form->textField($model, 'email', array(
					'class' => 'form-control input-sm',
					'placeholder' => $model->getAttributeLabel('email'),
				)); ?>
				<?php echo $form->error($model, 'email'); ?>
				<span class="help-block">&nbsp;</span>
			</div>
			<?php /* Mobile */ ?>
			<div class="col-xs-6">
				<h4><?php echo $form->labelEx($model, 'mobile'); ?></h4>
				<hr/>
				<?php echo $form->textField($model, 'mobile', array(
					'class' => 'form-control input-sm',
					'placeholder' => $model->getAttributeLabel('mobile'),
				)); ?>
				<?php echo $form->error($model, 'mobile'); ?>
				<span class="help-block">&nbsp;</span>
			</div>
		</div>
		<div class="row">
			<?php /* Gender */ ?>
			<div class="col-xs-3">
				<h4><?php echo Yii::t('User', 'Gender'); ?></h4>
				<hr/>
				<div class="switch-toggle well col-xs-11">
					<input id="week" name="view" type="radio" checked>
					<label for="week" onclick=""><?php echo Yii::t('User', 'GenderMaleShort'); ?></label>

					<input id="month" name="view" type="radio">
					<label for="month" onclick=""><?php echo Yii::t('User', 'GenderFemaleShort'); ?></label>

					<a class="btn btn-success"></a>
				</div>
				<span class="help-block">&nbsp;</span>
			</div>
			<?php /* Role */ ?>
			<div class="col-xs-3">
				<h4>Role</h4>
				<hr/>
				<select class="form-control input-sm">
					<option>admin</option>
					<option>moderator</option>
					<option selected>student</option>
				</select>
				<span class="help-block">&nbsp;</span>
			</div>
			<?php /* School */ ?>
			<div class="col-xs-6">
				<h4>School</h4>
				<hr/>
				<select class="form-control input-sm">
					<option selected>СШ №1</option>
					<option>СШ №2</option>
					<option>СШ №3</option>
				</select>
				<span class="help-block">&nbsp;</span>
			</div>
		</div>
		<div class="row">
			<?php /* Address */ ?>
			<div class="col-xs-6">
				<h4><?php echo $form->labelEx($model->profile, 'address'); ?></h4>
				<hr/>
				<?php echo $form->textField($model->profile, 'address', array(
					'class' => 'form-control input-sm',
					'placeholder' => $model->profile->getAttributeLabel('address'),
				)); ?>
				<?php echo $form->error($model->profile, 'address'); ?>
				<span class="help-block">&nbsp;</span>
			</div>
			<?php /* Grade */ ?>
			<div class="col-xs-6">
				<h4><?php echo $form->labelEx($model->profile, 'grade'); ?></h4>
				<hr/>
				<?php echo $form->textField($model->profile, 'grade', array(
					'class' => 'form-control input-sm',
					'placeholder' => $model->profile->getAttributeLabel('grade'),
				)); ?>
				<?php echo $form->error($model->profile, 'grade'); ?>
				<span class="help-block">&nbsp;</span>
			</div>
		</div>
		<div class="row">
			<?php /* Info */ ?>
			<div class="col-xs-7">
				<h4><?php echo Yii::t('User', 'Info'); ?></h4>
				<hr/>
				<p>ID: <?php echo $model->id; ?></p>

				<p>ID (public): <?php echo $model->metadata->public_id; ?></p>

				<p>ID (sh-karta.ru): <?php echo $model->metadata->shk_id; ?></p>

				<p>ID (vk.com): <?php echo $model->metadata->vk_id; ?></p>
			</div>
			<?php /* State */ ?>
			<div class="col-xs-5">
				<h4><?php echo Yii::t('User', 'Gender'); ?></h4>
				<hr/>
				<div class="switch-toggle switch-3 well">
					<input id="1" name="view" type="radio" checked>
					<label for="1" onclick=""><?php echo Yii::t('ObjectMetadata', 'Active'); ?></label>

					<input id="2" name="view" type="radio">
					<label for="2" onclick=""><?php echo Yii::t('ObjectMetadata', 'Disabled'); ?></label>

					<input id="3" name="view" type="radio">
					<label for="3" onclick=""><?php echo Yii::t('ObjectMetadata', 'Deleted'); ?></label>

					<a class="btn btn-success"></a>
				</div>
				<span class="help-block">&nbsp;</span>
			</div>
		</div>
	</div>

<?php /* Modal footer */ ?>
	<div class="modal-footer">
		<?php
		$this->widget('qSubmit', array(
			'type' => 'cancel',
			'confirm' => false,
			'htmlOptions' => array(
				'class' => 'pull-left',
				'data-dismiss' => 'modal',
			),
		));
		if (!$model->isNewRecord) {
			$this->widget('qSubmit', array(
				'type' => 'delete',
			));
		}
		$this->widget('qSubmit', array(
			'type' => $model->isNewRecord ? 'create' : 'save',
		));
		?>
	</div>

<?php $this->endWidget(); ?>