<?php
$this->pageTitle = Yii::app()->name . ' - Error';
?>
<style>
	#page-header img {
		width: 148px;
		height: 148px;
	}
	#page-header small {
		font-weight: lighter;
	}
</style>
<div id="page-header">
	<div class="page-header">
		<h1 class="text-danger">
			<img src="<?php echo $this->module->logoUrl; ?>" alt="<?php echo Yii::app()->name; ?>"/>
			Error <?php echo $code; ?>
			<small><?php echo CHtml::encode($message); ?></small>
		</h1>
	</div>
</div>